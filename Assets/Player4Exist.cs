﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player4Exist : MonoBehaviour
{
    public GameObject playerSetupPlayer4;

    public string player4Name;
    public bool player4joined = false;

    void OnTriggerEnter(Collider other)
    {
        if (player4joined == false)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                playerSetupPlayer4 = other.gameObject;
                player4Name = playerSetupPlayer4.GetPhotonView().owner.NickName;
            }
            if (playerSetupPlayer4 != null)
            {
                player4joined = true;
            }
        }
    }

    private void Update()
    {
        if (playerSetupPlayer4 == null)
        {
            player4Name = "";
            player4joined = false;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(player4Name);

        }
        else if (stream.isReading)
        {
            player4Name = (string)stream.ReceiveNext();

        }

    }

}
