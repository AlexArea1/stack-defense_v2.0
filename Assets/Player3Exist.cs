﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player3Exist : MonoBehaviour
{
    public GameObject playerSetupPlayer3;

    public GameManager gameManager;
    public string player3Name;
    public bool player3joined = false;

    void OnTriggerEnter(Collider other)
    {
        if (player3joined == false)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                playerSetupPlayer3 = other.gameObject;
                player3Name = playerSetupPlayer3.GetPhotonView().owner.NickName;
            }
            if (playerSetupPlayer3 != null)
            {
                player3joined = true;
            }
        }
    }

    private void Update()
    {
        if (playerSetupPlayer3 == null)
        {
            player3Name = "";
            player3joined = false;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(player3Name);

        }
        else if (stream.isReading)
        {
            player3Name = (string)stream.ReceiveNext();

        }

    }

}
