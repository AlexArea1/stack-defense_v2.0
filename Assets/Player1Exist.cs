﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Exist : MonoBehaviour
{
    public GameObject playerSetupPlayer1;

    public string player1Name;
    public bool isPlayer1Joined = false;

    void OnTriggerEnter(Collider other)
    {
        if (isPlayer1Joined == false)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                playerSetupPlayer1 = other.gameObject;
                player1Name = playerSetupPlayer1.GetPhotonView().owner.NickName;
            }
            if (playerSetupPlayer1 != null)
            {
                isPlayer1Joined = true;
            }
        }
    }

    private void Update()
    {
        if (playerSetupPlayer1 == null)
        {
            player1Name = "";
            isPlayer1Joined = false;
        }

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(player1Name);

        }
        else if (stream.isReading)
        {
            player1Name = (string)stream.ReceiveNext();

        }

    }

}