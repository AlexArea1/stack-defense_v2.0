﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Com.VisionMe.StackDefense
{
    public class PlayerStatsPhoton : MonoBehaviour
    {
        private Hashtable playerHashtable;
        private void Awake()
        {
            playerHashtable = new Hashtable();
        }

        public void SetStat(string hashTag, object value)
        {
            playerHashtable[hashTag] = value;
            PhotonNetwork.player.SetCustomProperties(playerHashtable);
        }
    }
}
