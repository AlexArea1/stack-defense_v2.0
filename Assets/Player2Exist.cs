﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Exist : MonoBehaviour
{
    public GameObject playerSetupPlayer2;
    public string player2Name;

    public GameManager gameManager;
    public bool player2joined = false;

    void OnTriggerEnter(Collider other)
    {
        if (player2joined == false)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                playerSetupPlayer2 = other.gameObject;
                player2Name = playerSetupPlayer2.GetPhotonView()?.owner.NickName;
            }
            if (playerSetupPlayer2 != null)
            {
                player2joined = true;
            }
        }
    }

    private void Update()
    {
        if (playerSetupPlayer2 == null)
        {
            player2Name = "";
            player2joined = false;
        }
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(player2Name);

        }
        else if (stream.isReading)
        {
            player2Name = (string)stream.ReceiveNext();

        }

    }

}
