﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace VRKeyboard.Utils
{
    public class WriteIPAddress : MonoBehaviour
    {
        public KeyboardManager keyboard;

        public TMP_InputField iPAddress;
        public TMP_InputField portNumber;



        private void Awake()
        {
            keyboard.gameObject.SetActive(false);
        }
        public void AssignInputField(TMP_InputField input)
        {
            keyboard.inputText = input;
            keyboard.gameObject.SetActive(true);
        }


    }
}