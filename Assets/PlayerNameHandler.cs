﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerNameHandler : MonoBehaviour
{

    public TextMeshProUGUI nameText;
    public GameObject keyBoard;
    public void changePosKeyboard()
    {
        if (keyBoard.activeInHierarchy == true)
        {
        keyBoard.transform.position = new Vector3(-0.7f,2.5f, 3.32f);
        keyBoard.transform.eulerAngles = new Vector3(0,0,0);

        }

    }

}
