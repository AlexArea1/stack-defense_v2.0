﻿using Com.VisionMe.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsData
{
    public List<string> keys;
    public List<OwnInputUsages> usages;
    public List<bool> leftHands;
    public List<GetType> getTypes;

    public ControlsData()
    {
        keys = new List<string>();
        usages = new List<OwnInputUsages>();
        leftHands = new List<bool>();
        getTypes = new List<GetType>();
    }
}
