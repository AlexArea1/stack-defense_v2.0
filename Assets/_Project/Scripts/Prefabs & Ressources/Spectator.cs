﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    public class Spectator : Photon.PunBehaviour
    {
        #region Public Variables

        [Header("General settings")]
        [Space(10)]

        [Tooltip("Speed the camera is accelerating with")]
        public float accelerationSpeed;

        [Tooltip("Speed the camera is rotating with")]
        public float rotationSpeed;

        [Tooltip("Maximal speed the camera is moving with in focused mode")]
        public float movementSpeedForwardMax;

        [Tooltip("Maximal speed the camera is rotating with in focused mode")]
        public float movementSpeedSidewardMax;

        [Header("Focused mode settings")]
        [Space(10)]

        public Vector3 focusedTarget;

        [Header("Free mode settings")]
        [Space(10)]

        [Tooltip("Maximal speed the camera is moving with in free mode")]
        public float freeMovementSpeedMax;

        [Header("Current state")]
        [Space(10)]

        [Tooltip("Current selected mode")]
        public SpectatorCameraMode mode;

        [Tooltip("Speed the camera is moving with forward and backward")]
        public float movementSpeedForward;

        [Tooltip("Speed the camera is moving with to the left and right")]
        public float movementSpeedSideward;

        [Tooltip("True if the movement is locked so the camera is moving constantly")]
        public bool isLocked = false;

        #endregion

        #region Private Variables

        #endregion

        #region MonoBehaviour Callbacks

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            DeactivateMidDisplays();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            //Change the camera mode
            if(Input.GetKeyDown("1"))
            {
                mode = SpectatorCameraMode.FOCUSED;
            }
            else if(Input.GetKeyDown("2"))
            {
                mode = SpectatorCameraMode.FREE;
            }

            if(Input.GetKeyDown(KeyCode.Escape))
            {
                PhotonNetwork.Disconnect();
            }

            //Move the camera accordingly to the current mode
            if(mode == SpectatorCameraMode.FOCUSED)
            {
                MoveFocused();
            }
            else if(mode == SpectatorCameraMode.FREE)
            {
                MoveFree();
            }
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Moves the camera focused on (around) a given point
        /// </summary>
        private void MoveFocused()
        {
            //Toggle isLocked
            if(Input.GetKeyDown(KeyCode.Space))
            {
                isLocked = !isLocked;
            }

            HandleForwardMovement();

            //Handle input
            if (isLocked)
            {
                //Move the camera constantly
                if(movementSpeedSideward < movementSpeedSidewardMax)
                {
                    movementSpeedSideward += accelerationSpeed * Time.deltaTime;
                }
            }
            else
            {
                //Handle sideware movement
                if (Input.GetKey("a"))
                {
                    //Move the camera to the right
                    if (movementSpeedSideward < movementSpeedSidewardMax)
                    {
                        movementSpeedSideward += accelerationSpeed * Time.deltaTime;
                    }
                }
                else if (Input.GetKey("d"))
                {
                    //Move the camera to the left
                    if (movementSpeedSideward > -movementSpeedSidewardMax)
                    {
                        movementSpeedSideward -= accelerationSpeed * Time.deltaTime;
                    }
                }
                else
                {
                    //Slow the camera down
                    if (movementSpeedSideward < -0.1)
                    {
                        movementSpeedSideward += accelerationSpeed * Time.deltaTime;
                    }
                    else if (movementSpeedSideward > 0.1)
                    {
                        movementSpeedSideward -= accelerationSpeed * Time.deltaTime;
                    }
                    else
                    {
                        movementSpeedSideward = 0;
                    }
                }
            }

            transform.LookAt(focusedTarget);
            transform.Translate(Vector3.forward * movementSpeedForward * Time.deltaTime);
            transform.RotateAround(focusedTarget, Vector3.up, movementSpeedSideward * Time.deltaTime);
        }

        /// <summary>
        /// Moves the camera freely
        /// </summary>
        private void MoveFree()
        {
            HandleForwardMovement();

            //Handle sideware movement
            if (Input.GetKey("a"))
            {
                //Move the camera to the right
                if (movementSpeedSideward < movementSpeedSidewardMax)
                {
                    movementSpeedSideward += accelerationSpeed * 10;
                }
            }
            else if (Input.GetKey("d"))
            {
                //Move the camera to the left
                if (movementSpeedSideward > -movementSpeedSidewardMax)
                {
                    movementSpeedSideward -= accelerationSpeed * 10;
                }
            }
            else
            {
                //Slow the camera down
                if (movementSpeedSideward < -0.1)
                {
                    movementSpeedSideward += accelerationSpeed * 10;
                }
                else if (movementSpeedSideward > 0.1)
                {
                    movementSpeedSideward -= accelerationSpeed * 10;
                }
                else
                {
                    movementSpeedSideward = 0;
                }
            }

        transform.eulerAngles += new Vector3(-Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime, Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime, 0);
        transform.Translate(Vector3.forward * movementSpeedForward * Time.deltaTime + -Vector3.right * movementSpeedSideward * 0.1f * Time.deltaTime);
        }

        /// <summary>
        /// Moves the camera forward according to the input
        /// </summary>
        private void HandleForwardMovement()
        {
            if (Input.GetKey("w"))
            {
                //Move the camera forward
                if (movementSpeedForward < movementSpeedForwardMax)
                {
                    movementSpeedForward += accelerationSpeed;
                }
            }
            else if (Input.GetKey("s"))
            {
                //Move the camera backward
                if (movementSpeedForward > -movementSpeedForwardMax)
                {
                    movementSpeedForward -= accelerationSpeed;
                }
            }
            else
            {
                //Slow the camera down
                if (movementSpeedForward < -0.1)
                {
                    movementSpeedForward += accelerationSpeed;
                }
                else if (movementSpeedForward > 0.1)
                {
                    movementSpeedForward -= accelerationSpeed;
                }
                else
                {
                    movementSpeedForward = 0;
                }
            }
        }

        private void DeactivateMidDisplays()
        {
            Transform[] transforms = GameObject.Find("Arenas").GetComponentsInChildren<Transform>(true);

            foreach (Transform transform in transforms)
            {
                if (transform.name == "Mid Display")
                {
                    transform.gameObject.SetActive(false);
                }
            }
        }

        #endregion

        #region RPCs

        #endregion
    }
}