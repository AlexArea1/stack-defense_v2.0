﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
public static class LayerHelpers {
    public static SingleUnityLayer GetPlayerModelLayer(this List<SingleUnityLayer> layers, int playerNumber) {
        if (layers == null || playerNumber < 0 || playerNumber >= layers.Count) {
            Debug.Log($"Did not find a layer for player={playerNumber}.");
            return null;
        }

        return layers[playerNumber];
    }

    public static void MoveToLayerIncludingChildren(Transform root, int layer) {
        root.gameObject.layer = layer;
        foreach (Transform child in root)
            MoveToLayerIncludingChildren(child, layer);
    }

    public static void RemoveLayerFromMask(this Camera cam, SingleUnityLayer singleUnityLayer) {
        cam.cullingMask = cam.cullingMask.RemoveLayer(singleUnityLayer.LayerIndex);
    }

    private static int RemoveLayer(this int mask, int layer) {
        if (layer < 0 || layer > 32)
            return mask;
        return mask & ~(1 << layer);
    }
}
}