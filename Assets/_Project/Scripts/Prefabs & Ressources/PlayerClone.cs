﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using VRTK;

namespace Com.VisionMe.StackDefense {
/// <summary>
/// This class creates and controls the player clone. A player clone is a virtual representation of the actual human playing the game. It doublicates the human
/// by interpolating a simple rig using the position of the VR headset and controllers. The clone is then synchronized on the network.
/// </summary>
public class PlayerClone : Photon.PunBehaviour, IPlayerNumberProvider {
    #region Public Variables

    [Tooltip("The player number of the player this clone belongs to")]
    public int playerNumber;
    public int PlayerNumber => playerNumber;

    [Space(10)] [Header("Body parts")] [Space(10)] [Tooltip("The gameobject representing the head")]
    public GameObject head;

    [Tooltip("The gameobject representing the body")]
    public GameObject body;

    [Tooltip("The gameobject representing the left hand")]
    public GameObject handLeft;

    [Tooltip("The gameobject representing the right hand")]
    public GameObject handRight;

    [Tooltip("The colors for each player from 1 to 4")]
    public Color[] colors;

    #endregion

    #region Private Variables

    //The VR headset
    public GameObject headset;

    //The left controller
    public GameObject controllerLeft;

    //The right controller
    public GameObject controllerRight;

    private GameManager gameManager;

    #endregion

    #region MonoBehaviour Callbacks

    // Use this for initialization


    private void Awake() {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();

        if (photonView.isMine || !PhotonNetwork.connected) //   if (photonView.isMine || !PhotonNetwork.connected)
        {
            //headset = GameObject.Find("CenterEyeAnchor");
            //controllerLeft = GameObject.Find("LeftHandAnchor");
            //controllerRight = GameObject.Find("RightHandAnchor");

            headset = GameObject.Find("HeadCam");
            controllerLeft = GameObject.Find("LeftHandC");
            controllerRight = GameObject.Find("RightHandC");

            photonView.RPC("SetPlayerNumber", PhotonTargets.AllBuffered, gameManager.playerNumber);
            photonView.RPC("SetColor", PhotonTargets.AllBuffered, gameManager.playerNumber);

            SetVisibility(true);
        }
    }

    void Start() {
        //if (!gameManager)
        //    gameManager = FindObjectOfType<GameManager>();

        // if (photonView.isMine || !PhotonNetwork.connected) //   if (photonView.isMine || !PhotonNetwork.connected)
        // {
        //headset = GameObject.Find("CenterEyeAnchor");
        //controllerLeft = GameObject.Find("LeftHandAnchor");
        //controllerRight = GameObject.Find("RightHandAnchor");

        //  headset = GameObject.Find("Main Camera");
        //  controllerLeft = GameObject.Find("LeftHand Controller");
        // controllerRight = GameObject.Find("RightHand Controller");

        //  photonView.RPC("SetPlayerNumber", PhotonTargets.AllBuffered, gameManager.playerNumber);
        //  photonView.RPC("SetColor", PhotonTargets.AllBuffered, gameManager.playerNumber - 1);

        //  SetVisibility(false);  
        //}
    }

    // Update is called once per frame
    void Update() {
        if (photonView.isMine || !PhotonNetwork.connected) {
            SyncPosition(head, headset);
            SyncRotation(head, headset);
            SyncPosition(handRight, controllerRight);
            SyncRotation(handRight, controllerRight);
            SyncPosition(handLeft, controllerLeft);
            SyncRotation(handLeft, controllerLeft);

            SyncBody();
        }

        head.GetComponent<MeshRenderer>().enabled = false;
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    /// <summary>
    /// Sets the clone's position to the origin's position
    /// </summary>
    /// <param name="clone">The gameobject that should be synchronized to origin</param>
    /// <param name="origin">The gameobject the clone should be synchronized to</param>
    private void SyncPosition(GameObject clone, GameObject origin) {
        clone.transform.position = origin.transform.position;
    }

    /// <summary>
    /// Sets the clone's rotation to the origin's rotation
    /// </summary>
    /// <param name="clone">The gameobject that should be synchronized to origin</param>
    /// <param name="origin">The gameobject the clone should be synchronized to</param>
    private void SyncRotation(GameObject clone, GameObject origin) {
        clone.transform.rotation = origin.transform.rotation;
    }


    /// <summary>
    /// Sync the bodys position to half of the head's position
    /// </summary>
    private void SyncBody() {
        body.transform.position = Vector3.Scale(head.transform.position, new Vector3(0, 0, 0));
        body.transform.localScale = new Vector3(body.transform.localScale.x, body.transform.position.y,
            body.transform.localScale.z);
    }

    /// <summary>
    /// Set's the player's visibility to the given value
    /// </summary>
    /// <param name="value">True if the player should be visible</param>
    private void SetVisibility(bool value) {
        SetVisibilityForTransform(head, value);
        SetVisibilityForTransform(body, value);
        SetVisibilityForTransform(handLeft, value);
        SetVisibilityForTransform(handRight, value);

        handRight.GetComponent<Renderer>().enabled = value;
    }

    /// <summary>
    /// Set's the visibility of a transform and all it's children
    /// </summary>
    /// <param name="value"></param>
    private void SetVisibilityForTransform(GameObject obj, bool value) {
        head.GetComponent<Renderer>().enabled = value;

        foreach (Transform child in obj.transform) {
            if (child.GetComponent<Renderer>()) {
                child.GetComponent<Renderer>().enabled = value;
            }
        }
    }

    #endregion

    #region RPCs

    /// <summary>
    /// Sets the player clone's color to the given color
    /// </summary>
    /// <param name="color">The index of the color the player clone should bet set to</param>
    [PunRPC]
    private void SetColor(int value) {
        Color color = colors[value];

        head.GetComponent<Renderer>().material.color = color;
        body.GetComponent<Renderer>().material.color = color;
        handLeft.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_EmissionColor", color);
        handRight.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_EmissionColor", color);
    }

    /// <summary>
    /// Sets the clone's player number to the given value
    /// </summary>
    /// <param name="value">The clone's player number</param>
    [PunRPC]
    private void SetPlayerNumber(int value) {
        playerNumber = value;
    }

    #endregion
}
}