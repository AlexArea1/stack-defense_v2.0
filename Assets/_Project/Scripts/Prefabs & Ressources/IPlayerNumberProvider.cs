﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
public interface IPlayerNumberProvider {
    int PlayerNumber { get; }
}
}