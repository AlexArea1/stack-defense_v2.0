﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    [CreateAssetMenu(menuName = "StackDefense/GameStats")]
    public class ScriptableGameStats : ScriptableObject
    {
        public List<PlayerInfo> playerInfos = new List<PlayerInfo>();

        public void SetPing(int playerNumber, int ping)
        {
            //pings[playerNumber] = ping;
            CheckList(playerNumber);
            playerInfos[playerNumber - 1].ping = ping;

        }
        public void SetName(int playerNumber, string name)
        {
            CheckList(playerNumber);
            playerInfos[playerNumber].name = name;
        }

        public void SetScore(int playerNumber, int score)
        {
            CheckList(playerNumber);
            playerInfos[playerNumber].score = score;
        }

        private void CheckList(int playerNumber)
        {
            while (playerInfos.Count < playerNumber)
            {
                playerInfos.Add(new PlayerInfo());
            }
        }
    }

    [System.Serializable]
    public class PlayerInfo
    {
        public int ping;
        public string name;
        public int score;
    }
}
