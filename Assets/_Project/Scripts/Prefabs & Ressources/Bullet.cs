﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.VisionMe.VR;

namespace Com.VisionMe.StackDefense
{
    public class Bullet : Photon.PunBehaviour, IPunCallbacks
    {
        #region Public Variables

        [Tooltip("Speed the bullet is moving with")]
        public float movementSpeed;

        [Tooltip("Amount of seconds the bullet lives. It's lifetime is reseted whenever it's changing it's direction")]
        public float lifetime;

        [Tooltip("This is the distance between the position where the bullet has changed it's direction last and the current position before the target" +
            "recalculated and the ownership transfered. This is used to avoid a lag right after a bullet has been blocked or shot.")]
        public float distanceOffsetForRecalculation = 1f;

        public Color[] colors;

        #endregion

        #region Private Variables

        // The position where the bullet's target has been recalculated the last time
        public Vector3 calculationPosition;

        // Indicates whether the target has been recalculated since the last direction change or not
        public bool targetHasBeenCalculated = false;

        private float ownershipTransferTime = 0;

        // True when the bullet's 
        private bool firstCollision = false;

        private GameManager gameManager;
        private VR_Input vr_Input;

        #endregion

        #region MonoBehaviour Callbacks

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            vr_Input = FindObjectOfType<VR_Input>();

            calculationPosition = transform.position;
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            if (photonView.isMine || !PhotonNetwork.connected)
            {
                if(PhotonNetwork.connected)
                {
                    //Recalculate the bullet whenever it has been instantiated or changed it's rection (reflected). The recalculation takes place
                    //with a small delay (= after a small position offset) to reduce lags due to ownership transfer time (~0.2 ms)
                    if (Vector3.Distance(transform.position, calculationPosition) > distanceOffsetForRecalculation && !targetHasBeenCalculated)
                    {
                        //Debug.Log("Recalculation");
                        photonView.RPC("SetCalculationVariables", PhotonTargets.All, true, calculationPosition);
                        CalculateTarget();
                    }
                }

                transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);

                //Destroy the bullet after 5 seconds or if the game has finished
                if (Vector3.Distance(transform.position, Vector3.zero) > 8 || gameManager.state != GameState.RUNNING)
                {
                    //Debug.Log("Destroyed cause bullet has left the area");
                    PhotonNetwork.Destroy(gameObject);
                }
            }
            else if(PhotonNetwork.connected)
            {
                if(gameManager.settingTransferOwnershipOldOwnerInterpolation && ownershipTransferTime + 0.2 > Time.time)
                {
                    transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            CheckCollisionWithStackingObject(collision);
        }

        private void OnTriggerEnter(Collider collider)
        {
            CheckCollisionWithShield(collider);
        }

        #endregion

        #region PUN Callbacks

        void IPunCallbacks.OnOwnershipTransfered(object[] viewAndPlayers)
        {
            PhotonPlayer newOwner = (PhotonPlayer) viewAndPlayers[1];
            PhotonPlayer oldOwner = (PhotonPlayer)viewAndPlayers[2];

            Debug.Log("New owner: " + newOwner.ID + " - old owner: " + oldOwner.ID);
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Calculates the target the player is shooting on and transfers the ownership to this player
        /// </summary>
        /// <returns>The playerNumber of the player this player is shooting at</returns>
        void CalculateTarget()
        {
            //Debug.Log("--- Calculating Target [" + PhotonNetwork.time + "] ---");

            int targetPlayer = 1;
            float angle = 1000;

            int[] playerNumbers = gameManager.GetPlayerNumbersOfConnectedPlayers();

            Debug.Log("Connected PlayersNumbers: " + PrintArray(playerNumbers));
            Debug.Log("Connected Players on network: " + PrintArray(PhotonNetwork.playerList));

            //Calculate the target playerNumber by casting a ray to each player and comparing the angles
            for (int i = 0; i < playerNumbers.Length; i++)
            {
                string tableName = "Table Player_" + playerNumbers[i];
                if (GameObject.Find(tableName))
                {
                    float angleToPlayer = Vector3.Angle(transform.forward, GameObject.Find(tableName).transform.position - transform.position);

                    if (angleToPlayer < angle && playerNumbers[i] != gameManager.playerNumber)
                    {
                        angle = angleToPlayer;
                        targetPlayer = playerNumbers[i];
                    }
                }
                else
                {
                    Debug.LogError("Couldn't find: " + tableName);
                }
                
            }

            Debug.Log("Player_" + targetPlayer + " should get the ownership");

            //Transfer the ownership if the target is another player
            if (targetPlayer != gameManager.playerNumber)
            {
                Debug.Log("Trying to transfer ownership to Player_" + targetPlayer);

                PhotonPlayer newOwner = PhotonPlayer.Find(gameManager.GetPlayerIdWithPlayerNumber(targetPlayer));

                Debug.Log("Player_" + targetPlayer + ": " + newOwner.ToStringFull());
                Debug.Log("Player_" + targetPlayer + " has got ID " + newOwner.ID);

                photonView.TransferOwnership(newOwner);

                if(gameManager.settingTransferOwnershipNewOwnerInterpolation)
                {
                    photonView.RPC("TransferOwnership", PhotonTargets.All, PhotonNetwork.time);
                }

                ownershipTransferTime = Time.time;
            }
            else
            {
                //Debug.Log("Bullet's ownership has not been transfered");
            }
        }

        /// <summary>
        /// Checks if the bullet has destroyed a stacking object and deletes it
        /// </summary>
        private void CheckCollisionWithStackingObject(Collision collision)
        {
            if (photonView.isMine || !PhotonNetwork.connected)
            {
                if (collision.transform.tag == "StackingObject" && !firstCollision)
                {
                    firstCollision = true;
                    Debug.Log("Destorying cause hit by bullet");
                    PhotonNetwork.Instantiate(collision.gameObject.GetComponent<StackingObject>().explosionPrefabHit.name, collision.transform.position, Quaternion.identity, 0);
                    collision.gameObject.GetComponent<StackingObject>().Destroy();
                    SoundManager.instance.PlaySound(SoundEffect.HIT, transform.position);
                    PhotonNetwork.Destroy(gameObject);
                }
            }
        }

        private void CheckCollisionWithShield(Collider collider)
        {
            if(photonView.isMine || !PhotonNetwork.connected)
            {
                if(collider.transform.name == "Shield")
                {
                    float angle = Vector3.Angle(transform.forward, collider.transform.forward);

                    //Allow only collision with front side of the shield!

                    if(angle > 90)
                    {
                        transform.forward = Vector3.Reflect(transform.forward, collider.transform.forward);
                        photonView.RPC("SetCalculationVariables", PhotonTargets.All, false, transform.position);
                        SoundManager.instance.PlaySound(SoundEffect.REFLECTION, transform.position);
                        vr_Input.Vibrate(1, 1, 0.05f/*, OVRInput.Controller.LTouch*/);
                    }                    
                }
                else
                {
                    Debug.Log("This is not a shield: " + collider.transform.name);
                }
            }
        }

        #endregion

        #region RPCs

        /// <summary>
        /// Sets the variables 'targetHasBeenCalculated', 'calculationTime' and 'calculationPosition'
        /// </summary>
        /// <param name="value">Value</param>
        [PunRPC]
        public void SetCalculationVariables(bool targetHasBeenCalculatedValue, Vector3 calculationPositionValue)
        {
            targetHasBeenCalculated = targetHasBeenCalculatedValue;
            calculationPosition = calculationPositionValue;

            //Debug.Log("SetCalculationVariables: " + targetHasBeenCalculatedValue + " | Position: " + calculationPositionValue.ToString());
        }

        [PunRPC]
        public void TransferOwnership(double time)
        {
            Debug.Log("Transfering ownership");

            if(photonView.isMine)
            {
                Debug.Log("Ownership has been transfered to this client");
                float deltaTime = (float)PhotonNetwork.time - (float)time;
                Vector3 way = Vector3.forward * movementSpeed * deltaTime * gameManager.settingTransOwnershipNewOwnerMultiplier;

                Debug.Log("Time: " + deltaTime);
                Debug.Log("Movement speed: " + movementSpeed);
                Debug.Log("Vector: " + way.ToString());

                transform.Translate(way);
            }
        }

        #endregion

        String PrintArray(Array array)
        {
            String text = "";

            foreach(System.Object element in array)
            {
                text += element.ToString() + ", ";
            }

            return text;
        }
    }
}