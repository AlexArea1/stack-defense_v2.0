﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeadingStatus : Photon.PunBehaviour
{
    private bool isLeading = false;

    private bool isLeadingPrev = false;

    private void Start()
    {
        SetLeading(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Sets isLeading to the given value
    /// </summary>
    /// <param name="value"></param>
    public void SetIsLeading(bool value)
    {
        if (PhotonNetwork.isMasterClient)
        {
            isLeading = value;

            if (isLeading != isLeadingPrev)
            {
                photonView.RPC("SetLeading", PhotonTargets.All, isLeading);
            }

            isLeadingPrev = isLeading;
        }
    }

    /// <summary>
    /// Set's whether the player is leading or not and controls the particale system equally
    /// </summary>
    /// <param name="value"></param>
    [PunRPC]
    public void SetLeading(bool value)
    {
        isLeading = value;
        GetComponent<ParticleSystem>().enableEmission = isLeading;

        foreach(Transform child in transform)
        {
            if(child.GetComponent<ParticleSystem>())
            {
                child.GetComponent<ParticleSystem>().enableEmission = isLeading;
            }
        }
    }
}
