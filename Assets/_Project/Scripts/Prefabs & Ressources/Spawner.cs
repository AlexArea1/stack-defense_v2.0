﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    /// <summary>
    /// The spawner spawns the stacking objects
    /// </summary>
    public class Spawner : Photon.PunBehaviour
    {
        #region Public Variables

        [Tooltip("All possible cubes that could spawn")]
        public GameObject[] cubes;

        #endregion

        #region Private Variables

        #endregion

        #region MonoBehaviour Callbacks

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            //MoveToSpawnerPosition();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            /*if(GameManager.instance.state == GameState.RUNNING)
            {
                isSpawning = true;
            } else
            {
                isSpawning = false;
            }

            if(photonView.isMine || !PhotonNetwork.connected)
            {
                if(isSpawning)
                {
                    if(lastSpawnTime + spawnRate < Time.time)
                    {
                        SpawnCube();
                        lastSpawnTime = Time.time;
                    }
                }
            }*/
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Moves this spawner to it's according position
        /// </summary>
        public void MoveToSpawnerPosition()
        {
            int playerNumber = transform.parent.GetComponent<Player>().playerNumber;
            GameObject spawnerObject = GameObject.Find("Spawner Player_" + playerNumber);

            Debug.Log("MoveToSpawnerPosition() Spawner Player_" + playerNumber);

            transform.position = spawnerObject.transform.position;
            transform.rotation = spawnerObject.transform.rotation;
        }

        #endregion

        #region Private Methods

        #endregion

        #region RPCs

        /// <summary>
        /// Spawns a cube
        /// </summary>
        /// <param name="randomCube">The index of the cube that should be spawned. Is is equal for all spawners</param>
        [PunRPC]
        public void SpawnCube(int randomCube)
        {
            if(photonView.isMine)
            {
                PhotonNetwork.Instantiate(cubes[randomCube].name, transform.position, transform.rotation, 0);
            }
        }

        #endregion
    }
}