﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
public class FixedPlayerNumber : MonoBehaviour, IPlayerNumberProvider {
    [SerializeField] private int _playerNumber;
    public int PlayerNumber => _playerNumber;
}
}