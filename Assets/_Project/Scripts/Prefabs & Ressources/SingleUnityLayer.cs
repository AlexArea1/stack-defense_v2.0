﻿using UnityEngine;

namespace Com.VisionMe.StackDefense {
[System.Serializable]
public class SingleUnityLayer {
    [SerializeField] private int _layerIndex = 0;

    public int LayerIndex => _layerIndex;

    public void Set(int layerIndex) {
        if (layerIndex > 0 && layerIndex < 32) {
            _layerIndex = layerIndex;
        }
    }

    public int Mask => 1 << _layerIndex;
}
}