﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Com.VisionMe.StackDefense {
[RequireComponent(typeof(TMP_Dropdown))]
public class FillDropdownSpeakers : MonoBehaviour {
    private TMP_Dropdown _dropdown;

    private SoundManager _soundManager;

    private void Awake() {
        _dropdown = GetComponent<TMP_Dropdown>();
    }

    private void Start() {
        _dropdown.options = new List<TMP_Dropdown.OptionData>();
        foreach (var speaker in Enum.GetNames(typeof(SoundEffectSpeaker))) {
            var option = new TMP_Dropdown.OptionData(speaker);
            _dropdown.options.Add(option);
        }

        _dropdown.value = (int) SoundManager.SoundEffectSpeaker;
        _dropdown.RefreshShownValue();

        _dropdown.onValueChanged.AddListener(OnValueChanged);
    }

    public void OnValueChanged(int value) {
        SoundManager.SoundEffectSpeaker = (SoundEffectSpeaker) value;
        if (SoundManager.instance != null)
            SoundManager.instance.PlaySound(SoundEffect.READY);
    }
}
}