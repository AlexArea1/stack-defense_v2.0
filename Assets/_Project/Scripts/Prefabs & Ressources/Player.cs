﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Com.VisionMe.VR;
using UnityEngine.UI;
using TMPro;
using Photon.Realtime;
using System.Configuration;
using UnityEngine.Profiling;

namespace Com.VisionMe.StackDefense
{
    /// <summary>
    /// A player is represented by a 'player setup' which consists of a player clone and a spawner.
    /// </summary>
    public class Player : Photon.PunBehaviour, IPunObservable
    {
        #region Public Variables

        [Tooltip("The number of the player this setup belongs to")]
        public int playerNumber;

        [Tooltip("The points of this player")]
        public int points;

        // give a List the number of points of this player
        public static int playerPoints;
        public static float playerVoiceVolume;

        [Tooltip("True if the player is ready to start the game")]
        public PlayerState state = PlayerState.IDLE;

        [Tooltip("This player's spawner")]
        public Spawner spawner;

        [Tooltip("Players Name")]
        // [SerializeField]
        public TextMeshProUGUI nameText;


        #endregion

        #region Private Variables
        private GameManager gameManager;
        private VR_Input vr_Input;

        private float playerID = 0;

        public GameObject Shield;
        int numberofShield;
        bool ShieldOnOff;

        public string PlayerName;

        // public GameObject ingameToggleMute;
        //public Toggle mute;

        // fix the sphereHand
        public GameObject fixedRightHand;
        public GameObject fixedLeftHand;

        // faceObject
        public GameObject robotFace;

        /// items
        public List<GameObject> headitems = new List<GameObject>();
        public List<GameObject> decalitems = new List<GameObject>();
        public List<GameObject> handItems = new List<GameObject>();

        public Texture whiteSkin;
        public Texture defaultSkin;

        public List<GameObject> defaultHandItems = new List<GameObject>();

        public List<Material> faceItems = new List<Material>();


        bool isSpeaking = false;

        // headitem 
        public int headItem;
        public bool headItemActivity;

        // decalitem
        public int decalItem;
        public bool decalItemActivity;

        //faceitem
        public int faceItem;

        public GameObject checkPlayerColormute;
        #endregion

        bool shieldOnOff;
        #region MonoBehaviour Callbacks
        string colored;
        string coloredDecal;

        //[SerializeField]private ScriptableGameStats gameStats;

        /// <summary>
        /// Use this for initialization
        /// </summary>

        private void Awake()
        {
            shieldOnOff = (bool)PhotonNetwork.room.CustomProperties["Shield"];
            gameManager = FindObjectOfType<GameManager>();
           // gameManager.enabled = true;


            if (LogicUI.isWhite == true)
            {
                foreach (GameObject item in handItems)
                {
                    item.SetActive(true);
                    item.GetComponent<Renderer>().material.SetTexture("_MainTex", whiteSkin);
                }
            }

            if (LogicUI.isBlue == true)
            {
                foreach (GameObject item in handItems)
                {
                    Color newColor;
                    // hexColor = hexacolor.text;
                    colored = PlayerPrefs.GetString("Colored");
                    if (ColorUtility.TryParseHtmlString(colored , out newColor))
                    {
                        item.SetActive(true);
                        item.GetComponent<Renderer>().material.color = newColor;
                    }
                }
            }

            if (LogicUI.isWhite == false && LogicUI.isBlue == false)
            {
                foreach (GameObject item in handItems)
                {
                    item.SetActive(true);
                    item.GetComponent<Renderer>().material.SetTexture("_MainTex", defaultSkin);
                }
            }

            foreach (GameObject decalItem in decalitems)
            {
                Color decalColor;
                coloredDecal = PlayerPrefs.GetString("ColoredDecal");
                if (ColorUtility.TryParseHtmlString(coloredDecal, out decalColor))
                {
                    decalItem.GetComponent<Renderer>().material.color = decalColor;
                }
            }


            headItem = PlayerPrefs.GetInt("headitem");
            decalItem = PlayerPrefs.GetInt("decalitem");
            faceItem = PlayerPrefs.GetInt("faceItem");
            headItemActivity = false;
            //   ingameToggleMute = GameObject.Find("ToggleMute");
            // mute = ingameToggleMute.GetComponent<Toggle>(); // mute? for tthe mute button 422 443
            //Shield = Gam
            PlayerName = PlayerPrefs.GetString("PlayerName");
            playerID = UnityEngine.Random.Range(100000000, 999999999);
            PlayerPrefs.SetFloat("playerID", playerID);
            playerVoiceVolume = this.GetComponent<AudioSource>().volume;

        }
        void Start()
        {
            //GetComponent<PhotonVoiceRecorder>().Transmit = true;


            fixedRightHand.GetComponent<MeshRenderer>().enabled = false;
            fixedLeftHand.GetComponent<MeshRenderer>().enabled = false;
            // ingameToggleMute.SetActive(false);

            //if (!gameManager)
            //    gameManager = FindObjectOfType<GameManager>();

            vr_Input = FindObjectOfType<VR_Input>();

            if (vr_Input)
            {
                //vr_Input.controllerLeftAPressed.AddListener(ToggleReady);
                //vr_Input.controllerRightAPressed.AddListener(ToggleReady);
            }
            else
                Debug.LogError("No VR_Input found");

            if (gameManager)
            {
                //if (photonView.isMine || !PhotonNetwork.connected)
                //{
                // Toggle ready status when the debug menu isn't opened
                //VR_Input.instance.controllerRightAPressed.AddListener(delegate { if (!GameObject.Find("UI").GetComponent<MenuIngame>().IsMenuActive()) { ToggleReady(); } });
                //VR_Input.instance.controllerRightBPressed.AddListener(delegate { if (!GameObject.Find("UI").GetComponent<MenuIngame>().IsMenuActive()) { ToggleReady(); } });
                //VR_Input.instance.controllerLeftAPressed.AddListener(delegate { if (!GameObject.Find("UI").GetComponent<MenuIngame>().IsMenuActive()) { ToggleReady(); } });
                //VR_Input.instance.controllerLeftBPressed.AddListener(delegate { if (!GameObject.Find("UI").GetComponent<MenuIngame>().IsMenuActive()) { ToggleReady(); } });

                Invoke("UpdatePlayers", 1);

                if (gameManager.state == GameState.IDLE)
                {
                    state = PlayerState.IDLE;
                }
                else if (gameManager.state == GameState.FINISHED)
                {
                    state = PlayerState.START;
                }
                //}

                gameManager.gameObjectsSynced = true;
            }
            else
                Debug.LogError("No GameManager found");

            LoadHash();
            Name();
        }


        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {

            VoiceInteraction();

            // HeadItems
            switch (headItem)
            {
                case 0:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    break;
                case 1:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    if (headItem == 1)
                    {
                        headItemActivity = true;
                        headitems[0].SetActive(headItemActivity);
                    }
                    break;
                case 2:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    if (headItem == 2)
                    {
                        headItemActivity = true;
                        headitems[1].SetActive(headItemActivity);
                    }
                    break;
                case 3:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    if (headItem == 2)
                    {
                        headItemActivity = true;
                        headitems[0].SetActive(headItemActivity);
                        headitems[1].SetActive(headItemActivity);
                    }
                    break;
                case 4:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    if (headItem == 4)
                    {
                        headItemActivity = true;
                        headitems[2].SetActive(headItemActivity);
                    }
                    break;
                case 5:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    if (headItem == 5)
                    {
                        headItemActivity = true;
                        headitems[3].SetActive(headItemActivity);
                    }
                    break;
                case 6:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headItemActivity = false;
                        headitems[i].SetActive(headItemActivity);
                    }
                    if (headItem == 6)
                    {
                        headItemActivity = true;
                        headitems[4].SetActive(headItemActivity);
                    }
                    break;
            }
            // decalItems
            switch (decalItem)
            {
                case 0:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalItemActivity = false;
                        decalitems[i].SetActive(decalItemActivity);
                    }
                    break;
                case 1:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalItemActivity = false;
                        decalitems[i].SetActive(decalItemActivity);
                    }
                    if (decalItem == 1)
                    {
                        decalItemActivity = true;
                        decalitems[0].SetActive(decalItemActivity);
                    }
                    break;
                case 2:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalItemActivity = false;
                        decalitems[i].SetActive(decalItemActivity);
                    }
                    if (decalItem == 2)
                    {
                        decalItemActivity = true;
                        decalitems[1].SetActive(decalItemActivity);
                    }
                    break;
            }
                //faceItems
                switch (faceItem)
                {
                    case 0:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        break;
                    case 1:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        if (faceItem == 1)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[1];
                        }
                        break;
                    case 2:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        if (faceItem == 2)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[2];
                        }
                        break;
                    case 3:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        if (faceItem == 3)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[3];
                        }
                        break;
                    case 4:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        if (faceItem == 4)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[4];
                        }
                        break;
                    case 5:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        if (faceItem == 5)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[5];
                        }
                        break;
                    case 6:
                        for (int i = 0; i < faceItems.Count; i++)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[0];
                        }
                        if (faceItem == 6)
                        {
                            robotFace.GetComponent<MeshRenderer>().material = faceItems[6];
                        }
                        break;
                
            }


            playerPoints = points;
            //if (!ingameToggleMute) ingameToggleMute = GameObject.Find("ToggleMute");

            //ingameToggleMute.SetActive(IngameMenu.isMenuOpen);

            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            if (gameManager)
            {
                if (gameManager.isConnected)
                {
                    gameManager.SetPointsForPlayer(playerNumber, points);
                }
            }

            // Mute Self

            //    if (mute.isOn == true)
            //    {
            //        GetComponent<PhotonVoiceRecorder>().Transmit = false;

            //    }
            //    else if (mute.isOn == false)
            //    {
            //        // GetComponent<PhotonVoiceRecorder>().enabled = true;
            //        //  isSpeaking = true;
            //        GetComponent<PhotonVoiceRecorder>().Transmit = true;

            //    }
            //}
        }

        public void VoiceInteraction()
        {

            if (photonView.isMine)
            {
                GetComponent<PhotonVoiceRecorder>().enabled = true;
            }
        }

        public void Name()
        {
            string playerNames = photonView.isMine ? PhotonNetwork.player.NickName : photonView.owner.NickName;
            nameText.text = playerNames;
            //gameStats.SetName(playerNumber, playerNames);
        }

        public void LoadHash()
        {
            Shield.SetActive(shieldOnOff);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Increases the player's points by one
        /// </summary>
        public void AddPoint()
        {
            // points += 1;
        }

        /// <summary>
        /// Decreases the player's points by one
        /// </summary>
        public void RemovePoint()
        {
            //points -= 1;
            //if (points <= 0)
            //{
            //    points = 0;
            //}
        }

        /// <summary>
        /// Sets the player's points to zero
        /// </summary>
        public void ResetPoints()
        {
            points = 0;
        }

        /// <summary>
        /// Sets the state variable on the given value
        /// </summary>
        /// <param name="value">Value</param>
        public void SetState(PlayerState value)
        {
            state = value;
        }

        /// <summary>
        /// Sets the player number of this player and changes it color accordingly
        /// </summary>
        /// <param name="playerNumber">The new player number</param>
        public void SetPlayerNumber(int playerNumber)
        {
            Debug.Log("[Player] Setting player number of player " + this.playerNumber + " to " + playerNumber);
            this.playerNumber = playerNumber;

            if (photonView.isMine)
            {
                gameManager.playerNumber = playerNumber;
            }
            transform.GetChild(0).GetComponent<PlayerClone>().photonView.RPC("SetColor", PhotonTargets.AllBuffered, playerNumber - 1);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calls the UpdatePlayers RPC
        /// This function is needed to create a delay between updating the player's numbers and synchronizing the player numbers (must be done before!)
        /// </summary>
        private void UpdatePlayers()
        {
            gameManager.photonView.RPC("UpdatePlayers", PhotonTargets.All);
        }

        /// <summary>
        /// Toggles the player's state whether he is ready to start a game or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ToggleReady()
        {
            Debug.Log("Change State");

            GameState gameState = gameManager.state;

            // Toggle to start game
            if (gameState == GameState.IDLE)
            {
                if (state == PlayerState.IDLE)
                {
                    state = PlayerState.START;
                }
                else
                {
                    state = PlayerState.IDLE;
                }
            }

            // Toggle to reset game
            if (gameState == GameState.FINISHED)
            {
                if (state == PlayerState.START)
                {
                    state = PlayerState.RESET;
                }
                else
                {
                    state = PlayerState.START;
                }
            }
        }

        /// <summary>
        /// Registers this player instance at the game manager
        /// </summary>
        [PunRPC]
        private void RegisterPlayerAtGameManager(int playerNumber)
        {
            gameManager.SetPlayer(playerNumber, gameObject);
        }

        #endregion

        #region RPCs

        /// <summary>
        /// Synchronizes the player's points on the network
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                stream.SendNext(headItem);
                stream.SendNext(decalItem);
                stream.SendNext(faceItem);
                stream.SendNext(headItemActivity);
                stream.SendNext(points);
                stream.SendNext(playerNumber);
                stream.SendNext(state);
                stream.SendNext(numberofShield);
                stream.SendNext(colored);
                stream.SendNext(coloredDecal);
                // stream.SendNext(isSpeaking);
            }
            else if (stream.isReading)
            {
                headItem = (int)stream.ReceiveNext();
                decalItem = (int)stream.ReceiveNext();
                faceItem = (int)stream.ReceiveNext();
                headItemActivity = (bool)stream.ReceiveNext();
                points = (int)stream.ReceiveNext();
                playerNumber = (int)stream.ReceiveNext();
                state = (PlayerState)stream.ReceiveNext();
                numberofShield = (int)stream.ReceiveNext();
                colored = (string)stream.ReceiveNext();
                coloredDecal = (string)stream.ReceiveNext();
                // isSpeaking = (bool)stream.ReceiveNext();
            }
        }

        #endregion
    }
}