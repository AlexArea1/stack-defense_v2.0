﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
[RequireComponent(typeof(Camera))]
public class RemovePlayerModelFromCullingMask : MonoBehaviour {
    [SerializeField] private int _playerNumber;

    [SerializeField] private List<SingleUnityLayer> _layers;

    private Camera _camera;

    private IPlayerNumberProvider _playerNumberProvider;

    public void Refresh() {
        _camera = GetComponent<Camera>();
        _playerNumberProvider = GetComponentInParent<IPlayerNumberProvider>();
        if (_playerNumberProvider == null)
            return;

        _playerNumber = _playerNumberProvider.PlayerNumber - 1;
        var layer = _layers.GetPlayerModelLayer(_playerNumber);
        if (layer != null)
            _camera.RemoveLayerFromMask(layer);
    }
}
}