﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    public class DestroyAfterSeconds : Photon.PunBehaviour
    {
        [Tooltip("Time after the gameObject is being destroyed")]
        public float time;

        private float startTime;

        private void Start()
        {
            startTime = Time.time;
        }

        private void Update()
        {
            if(photonView.isMine)
            {
                if (Time.time > startTime + time)
                {
                    PhotonNetwork.Destroy(gameObject);
                }
            }
        }
    }
}