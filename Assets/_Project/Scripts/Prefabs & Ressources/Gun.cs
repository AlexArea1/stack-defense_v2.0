﻿using Com.VisionMe.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using VRTK;
using UnityEngine.XR;

namespace Com.VisionMe.StackDefense {
public class Gun : Photon.PunBehaviour {
    #region Public Variables

    [Tooltip("The maximum amount of bullets the player can have at once")]
    public int maxAmountOfBullets;

    [Tooltip("The time until the player gets another bullet")]
    public float reloadTime;

    [Tooltip("The amount of bullets the player has got at the moment")]
    public int currentBullets;

    [Tooltip("Prefab for the bullets")] public GameObject bulletPrefab;

    [Tooltip("Position where the bullets should spawn")]
    public GameObject spawnPoint;

    [Tooltip("Text displaying the amount of currently available bullets")]
    public Text bulletCounter;

    [Tooltip("GameObject representing the current reload status by displaying a green bar")]
    public GameObject reloadStatus;

    public int playerNumber = 0;

    #endregion

    #region Private Variables

    //The time when the gun has been reloaded the last time
    private float lastReloadTime;

    private Vector3 scaleInitial;
    private Vector3 positionInitial;

    private GameManager gameManager;
    private VR_Input vr_Input;

    private IPlayerNumberProvider _playerNumberProvider;
    #endregion

    #region MonoBehaviour Callbacks

    /// <summary>
    /// Use this for initialization
    /// </summary>
    void Start() {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();

        _playerNumberProvider = GetComponentInParent<IPlayerNumberProvider>();
        playerNumber = _playerNumberProvider.PlayerNumber;

        if (gameManager.playerNumber == playerNumber) {
            vr_Input = FindObjectOfType<VR_Input>();

            EventAndInput eai = new EventAndInput();
            eai.functionEvent = new UnityEngine.Events.UnityEvent();
            eai.functionEvent.AddListener(TryToShoot);
            eai.inputFeatureUsage = CommonUsages.triggerButton;
            eai.leftHand = true;
            eai.getType = VR.GetType.GETDOWN;

            vr_Input.AddEvent("Shoot", eai);
        }

        scaleInitial = reloadStatus.transform.localScale;
        positionInitial = reloadStatus.transform.localPosition;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update() {
        bulletCounter.text = currentBullets.ToString();

        if (gameManager.state != GameState.RUNNING) {
            currentBullets = maxAmountOfBullets;
        }

        float reloadStatusPercentage = (Time.time - lastReloadTime) / reloadTime;


        if (currentBullets < maxAmountOfBullets) {
            reloadStatus.transform.localScale =
                new Vector3(scaleInitial.x, scaleInitial.y * reloadStatusPercentage, scaleInitial.z);
            reloadStatus.transform.localPosition = new Vector3(
                positionInitial.x + (scaleInitial.y / 2) * (1 - reloadStatusPercentage), positionInitial.y,
                positionInitial.z);

            if (Time.time - lastReloadTime > reloadTime) {
                currentBullets++;
                lastReloadTime = Time.time;
            }
        }
        else {
            reloadStatus.transform.localScale = new Vector3(scaleInitial.x, scaleInitial.y, scaleInitial.z);
            reloadStatus.transform.localPosition = new Vector3(positionInitial.x, positionInitial.y, positionInitial.z);
            lastReloadTime = Time.time;
        }
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    /// <summary>
    /// Try to shoot if the player presses the trigger. It will shoot if the game is running and there are bullets available
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void TryToShoot() {
        if (gameManager.playerNumber != playerNumber)
            return;

        if (gameManager.state == GameState.RUNNING && !gameManager.isSpectator) {
            if (currentBullets > 0) {
                Shoot();
                currentBullets -= 1;
            }
        }
    }

    void Shoot() {
        if (PhotonNetwork.connected) {
            PhotonNetwork.Instantiate(bulletPrefab.name, spawnPoint.transform.position, spawnPoint.transform.rotation,
                0);
            SoundManager.instance.PlaySound(0, transform.position);
            //TODO: Add habtic feedback
            //VR_Input.instance.Vibrate(1, 1, 0.05f, OVRInput.Controller.LTouch);
        }
        else {
            Instantiate(bulletPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
        }
    }

    #endregion

    #region RPCs

    #endregion
}
}