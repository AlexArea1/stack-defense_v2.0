﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.VisionMe.VR;

namespace Com.VisionMe.StackDefense
{
    /// <summary>
    /// A stacking object is an object which can be grabbed and placed on the table. It is moving upwards as long as the player hasn't grabbed the stacking object.
    /// As soon as the player has grabbed it once it activates it's physics so it can fall down and be destroyed.
    /// </summary>
    public class StackingObject : Photon.PunBehaviour
    {
        #region Public Variables

        [Tooltip("Speed the object is flying upwards with")]
        public float movementSpeed = 1f;

        [Tooltip("The height when the object is being destroyed")]
        public float maxHeight = 20f;

        [Tooltip("Explosion prefab when someone shoots the stacking object")]
        public GameObject explosionPrefabHit;

        [Tooltip("Explosion prefab when the stacking object falls onto the ground")]
        public GameObject explosionPrefabGround;

        [Tooltip("True if the cube has been grabbed")]
        public bool hasBeenGrabbed = false;

        /// <summary>
        /// Used to avoid execution of OnCollisionEnter twice. This resulted in minus points!
        /// </summary>
        private bool firstCollision = false;

        #endregion

        #region Private Variables
        private GameManager gameManager;
        #endregion

        #region MonoBehaviour Callbacks

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            //if(photonView.isMine || !PhotonNetwork.connected)
            //{
            //    GetComponent<VR_InteractableObject>().onGrab.AddListener(delegate { this.GrabObject(); });
            //}
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            if (photonView.isMine || !PhotonNetwork.connected)
            {

                GetComponent<VR_InteractableObject>().onGrab.AddListener(delegate { this.GrabObject(); });
            }

            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            if (!gameManager)
                return;

            if(photonView.isMine || !PhotonNetwork.connected)
            {
                //Destroy the stacking object when it's flying too high
                if(transform.position.y > maxHeight)
                {
                    //Remove a point from the player when the object has been grabbed to avoid "elevator trick"
                    if(hasBeenGrabbed)
                    {
                        gameManager.playerSetup.GetComponent<Player>().RemovePoint();
                    }

                    Delete();
                }

                // Destroy block when hold in hand but time is over
                if(gameManager.state != GameState.RUNNING)
                {
                    // Synchronization delay can cause a block to be spawned before the new gamestate is synchronized
                    if(gameManager.state != GameState.COUNTDOWN_START)
                    {
                        //TODO: entcommend...
                        //GetComponent<VR_InteractableObject>().IsGrabbable = false;
                    }

                    if(GetComponent<VR_InteractableObject>().IsGrabbed)
                    {
                        Destroy();
                    }
                }

                //Stop moving upwards when the gameObject has been grapped
                if(!hasBeenGrabbed)
                {
                    MoveUpwards();
                }
                /*else
                {
                    GetComponent<Rigidbody>().isKinematic = false;
                }*/
            }
        }

        /// <summary>
        /// Called when the gameObject collides with another collider
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter(Collision collision)
        {
            if(photonView.isMine)
            {
                if (collision.transform.tag == "Ground" && !firstCollision)
                {
                    firstCollision = true;
                    Debug.Log("Collision with ground");
                    PhotonNetwork.Instantiate(explosionPrefabGround.name, transform.position, Quaternion.identity, 0);
                    Destroy();
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Deletes this stacking object in the room WITHOUT removing a point from the player
        /// </summary>
        public void Delete()
        {
            PhotonNetwork.Destroy(gameObject);
        }

        /// <summary>
        /// Deletes this stacking object in the room AND removes a point from the player
        /// </summary>
        public void Destroy()
        {
            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            if (!gameManager)
                return;

            Debug.Log("Destroying: " + gameManager.playerSetup.GetComponent<Player>().points);
            gameManager.playerSetup.GetComponent<Player>().RemovePoint();
            PhotonNetwork.Destroy(gameObject);
            Debug.Log("After: " + gameManager.playerSetup.GetComponent<Player>().points);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Stops the stacking object from moving upwards. It is called as soon as the object is grabbed for the first time.
        /// </summary>
        private void GrabObject()
        {
            //TODO: entcommend...
            //GetComponent<VR_InteractableObject>().SavedKinematic = false;

            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            if (!gameManager)
                return;

            //Add a point to the player
            if (!hasBeenGrabbed)
            {
                gameManager.playerSetup.GetComponent<Player>().AddPoint();
            }

            hasBeenGrabbed = true;
        }

        /// <summary>
        /// Moves the object upwards
        /// </summary>
        private void MoveUpwards()
        {
            transform.position += new Vector3(0, movementSpeed * Time.deltaTime, 0);
        }



        #endregion

        #region RPCs

        //public void SpawnCube()
        //{
        //    if (photonView.isMine)
        //    {
        //        PhotonNetwork.Instantiate(cubes[randomCube].name, transform.position, transform.rotation, 0);
        //    }
        //}

        #endregion
    }
}