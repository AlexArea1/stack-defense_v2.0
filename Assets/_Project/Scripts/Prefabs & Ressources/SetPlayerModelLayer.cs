﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
/// <summary>
/// set the unity layer to the defined layer on start depending on the player number
/// </summary>
public class SetPlayerModelLayer : MonoBehaviour {
    [SerializeField] private List<SingleUnityLayer> _layers;

    private IPlayerNumberProvider _playerNumberProvider;

    private void Awake() {
        _playerNumberProvider = GetComponentInParent<IPlayerNumberProvider>();
    }

    private void Start() {
        var playerNumber = GetPlayerNumber();
        var playerLayer = _layers.GetPlayerModelLayer(playerNumber - 1);

        if (playerLayer != null)
            LayerHelpers.MoveToLayerIncludingChildren(transform, playerLayer.LayerIndex);
    }

    private int GetPlayerNumber() {
        if (_playerNumberProvider == null) {
            Debug.Log($"Did not find player component in parent. Assuming playerNo=0");
            return 0;
        }

        return _playerNumberProvider.PlayerNumber;
    }
}
}