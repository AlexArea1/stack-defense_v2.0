﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LogicUI : MonoBehaviour
{
    public GameObject headCustomPanel;
    public GameObject faceCustomPanel;
    public GameObject handCustomPanel;
    public GameObject decalCustomPanel;

    //headItems
    public List<GameObject> headitems = new List<GameObject>();
    bool headitem = false;

    //decalitems
    public List<GameObject> decalitems = new List<GameObject>();
    bool decalitem = false;
    public GameObject pageOne;
    public GameObject pageTwo;

    //faceitems
    public GameObject robotFace;
    public List<Material> faceMaterials = new List<Material>();
    bool faceItems = false;
    public int counterFace;

    //handItems
    public List<GameObject> handItems = new List<GameObject>();
    bool handitem = false;
    public GameObject flexColorPicker;
    public GameObject mirror;
    public GameObject handModelDummy;
    public static bool defaultColor = false;
    //public Material whiteHandSkin;
    public List<GameObject> defaultHandList = new List<GameObject>();
    public Text hexColor;
    public Text decalHexColor;
    public GameObject flexColorPicker2Hand;

    public Texture defaultSkin;
    public Texture whiteSkin;

    public static bool isWhite = false;
    public static bool isBlue = false;

    // Set next headItem
    private int counterhead;
    private int fixedCounter;

    // Set next decalItem
    private int decalCounter;
    bool next = false;
    bool back = false;

    public GameObject PlayerCostum;

    public void Start()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            counterhead = i + 3;
            fixedCounter = i + 3;
        }
        for (int j = 0; j < faceMaterials.Count; j++)
        {
            counterFace = j + 1;
        }
    }
    public void Update()
    {
        PlayerPrefs.SetString("Colored", hexColor.text.ToString());
        PlayerPrefs.SetString("ColoredDecal", decalHexColor.text.ToString());
        Debug.Log(counterhead);
    }


    public void RightRotate()
    {
        if (Input.GetMouseButton(0))
        {
            PlayerCostum.transform.Rotate(0, -3, 0);
        }
    }

    public void LeftRotate()
    {

        PlayerCostum.transform.Rotate(0, 3, 0);
    }
    public void Apply()
    {
        SceneManager.LoadScene("Launcher_VR");
    }
    public void Revert()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        PlayerPrefs.SetInt("item", 0);
        PlayerPrefs.SetInt("faceItem", 0);
    }
    #region ItemPanels
    public void headCustomItemsOn()
    {
        headCustomPanel.SetActive(true);
        faceCustomPanel.SetActive(false);
        handCustomPanel.SetActive(false);
        decalCustomPanel.SetActive(false);
        headitem = true;
        decalitem = false;
        faceItems = false;
        flexColorPicker.SetActive(false);
        mirror.SetActive(true);
        handModelDummy.SetActive(false);
    }
    public void faceCustomItemsOn()
    {
        headCustomPanel.SetActive(false);
        faceCustomPanel.SetActive(true);
        handCustomPanel.SetActive(false);
        decalCustomPanel.SetActive(false);
        headitem = false;
        decalitem = false;
        faceItems = true;
        flexColorPicker.SetActive(false);
        mirror.SetActive(true);
        handModelDummy.SetActive(false);
    }
    public void handCustomItemsOn()
    {
        headCustomPanel.SetActive(false);
        faceCustomPanel.SetActive(false);
        handCustomPanel.SetActive(true);
        decalCustomPanel.SetActive(false);
        headitem = false;
        faceItems = false;
        decalitem = false;
        flexColorPicker.SetActive(false);
        flexColorPicker2Hand.SetActive(true);
        mirror.SetActive(false);
        handModelDummy.SetActive(true);
    }
    public void decalCustomItemsOn()
    {
        headCustomPanel.SetActive(false);
        faceCustomPanel.SetActive(false);
        handCustomPanel.SetActive(false);
        decalCustomPanel.SetActive(true);
        headitem = false;
        faceItems = false;
        decalitem = true;
        flexColorPicker.SetActive(false);
        mirror.SetActive(true);
        handModelDummy.SetActive(false);
    }
    #endregion ItemPanels
    #region Decals
    public void decalItemNothing()
    {
        for (int i = 0; i < decalitems.Count; i++)
        {
            decalitems[i].SetActive(false);
        }
        PlayerPrefs.SetInt("decalitem", 0);
    }
    public void decalItemBL()
    {
        for (int i = 0; i < decalitems.Count; i++)
        {
            decalitems[i].SetActive(false);
        }
        decalitems[0].SetActive(true);
        PlayerPrefs.SetInt("decalitem", 1);
    }
    public void decalItemBR()
    {
        for (int i = 0; i < decalitems.Count; i++)
        {
            decalitems[i].SetActive(false);
        }
        decalitems[1].SetActive(true);
        PlayerPrefs.SetInt("decalitem", 2);
    }
    public void decalItemFR()
    {
        for (int i = 0; i < decalitems.Count; i++)
        {
            decalitems[i].SetActive(false);
        }
        decalitems[2].SetActive(true);
        PlayerPrefs.SetInt("decalitem", 3);
    }
    public void decalItemFL()
    {
        for (int i = 0; i < decalitems.Count; i++)
        {
            decalitems[i].SetActive(false);
        }
        decalitems[3].SetActive(true);
        PlayerPrefs.SetInt("decalitem", 4);
    }
    #endregion  Decals
    #region Head
    public void headItem1nothing()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        PlayerPrefs.SetInt("headitem", 0);
    }
    public void headItem2AntennaL()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        headitems[0].SetActive(true);
        PlayerPrefs.SetInt("headitem", 1);
    }
    public void headItem3AntennaR()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        headitems[1].SetActive(true);
        PlayerPrefs.SetInt("headitem", 2);
    }
    public void headItem4AntennaLR()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        headitems[0].SetActive(true);
        headitems[1].SetActive(true);
        PlayerPrefs.SetInt("headitem", 3);
    }
    public void headItem5DevilHorns()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        headitems[2].SetActive(true);
        PlayerPrefs.SetInt("headitem", 4);
    }
    public void headItem6Halo()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        headitems[3].SetActive(true);
        PlayerPrefs.SetInt("headitem", 5);
    }
    public void headItem7Top01()
    {
        for (int i = 0; i < headitems.Count; i++)
        {
            headitems[i].SetActive(false);
        }
        headitems[4].SetActive(true);
        PlayerPrefs.SetInt("headitem", 6);
    }
    #endregion Head
    #region face
    public void faceItem1()
    {
        PlayerPrefs.SetInt("faceItem", 0);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[0];
        }

    }
    public void faceItem2()
    {
        PlayerPrefs.SetInt("faceItem", 1);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[1];
        }
    }
    public void faceItem3()
    {
        PlayerPrefs.SetInt("faceItem", 2);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[2];
        }
    }
    public void faceItem4()
    {
        PlayerPrefs.SetInt("faceItem", 3);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[3];
        }
    }
    public void faceItem5()
    {
        PlayerPrefs.SetInt("faceItem", 4);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[4];
        }
    }
    public void faceItem6()
    {
        PlayerPrefs.SetInt("faceItem", 5);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[5];
        }
    }
    public void faceItem7()
    {
        PlayerPrefs.SetInt("faceItem", 6);
        for (int i = 0; i < faceMaterials.Count; i++)
        {
            robotFace.GetComponent<MeshRenderer>().material = faceMaterials[6];
        }
    }

    #endregion face
    #region Hand
    public void HandChangeToDefault()
    {
        //defaultColor = true;
        //string defaultcolor = "#CCCCCC";
        //hexColor.text = "#CCCCCC";
        //Color newgrey;
        isBlue = false;
        isWhite = false;
        flexColorPicker.SetActive(false);
        handModelDummy.SetActive(false);
        mirror.SetActive(true);
        foreach (GameObject item in handItems)
        {
            item.SetActive(false);
        }
        foreach (GameObject defaultHand in defaultHandList)
        {
            defaultHand.SetActive(true);
            defaultHand.GetComponent<Renderer>().material.SetTexture("_MainTex", defaultSkin);
        }

    }
    public void HandChangeToBlue()
    {
        isBlue = true;
        isWhite = false;
        foreach (GameObject item in handItems)
        {
            item.SetActive(true);
            item.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
        }

        foreach (GameObject defaultHand in defaultHandList)
        {
            defaultHand.SetActive(false);
        }
        flexColorPicker.SetActive(true);
        handModelDummy.SetActive(true);
        mirror.SetActive(false);
}
    public void HandChangeToWhite()
    {
        isBlue = false;
        isWhite = true;
        foreach (GameObject defaultHand in defaultHandList)
        {
            defaultHand.SetActive(true);
           // defaultHand.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
            defaultHand.GetComponent<Renderer>().material.SetTexture("_MainTex", whiteSkin);
        }
        foreach (GameObject item in handItems)
        {
            item.SetActive(false);
        }

        handModelDummy.SetActive(false);
        flexColorPicker.SetActive(false);
        mirror.SetActive(true);
    }
    #endregion Hand
    #region LeftRightChoose
    public void CustomNext()
    {
        flexColorPicker.SetActive(false);
        mirror.SetActive(true);
        handModelDummy.SetActive(false);
        //nextButton to Choose the next item from the headItemList
        if (headitem == true)
        {
            next = true;
            if (next == true)
            {
                // counter = fixedCounter;
                counterhead--;
               // next = false;
            }
            switch (counterhead)
            {
                case 6:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead == 6)
                    {
                        headitems[4].SetActive(true);
                    }
                    break;

                case 5:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead == 5)
                    {
                        headitems[3].SetActive(true);
                    }
                    break;

                case 4:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }

                    if (counterhead == 4)
                    {
                        headitems[2].SetActive(true);
                    }
                    break;

                case 3:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }

                    if (counterhead == 3)
                    {
                        headitems[0].SetActive(true);
                        headitems[1].SetActive(true);
                    }
                    break;

                case 2:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }

                    if (counterhead == 2)
                    {
                        headitems[1].SetActive(true);
                    }
                    break;

                case 1:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead == 1)
                    {
                        headitems[0].SetActive(true);
                    }
                    break;
                case 0:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead <= 0 || counterhead >= 6)
                    {
                        counterhead = fixedCounter;
                    }
                    break;
            }
        }

        if (faceItems == true)
        {

            next = true;
            if (next == true)
            {
                counterFace--;
                if (counterFace < 0)
                {
                    counterFace = 6;
                }
            }
            switch (counterFace)
            {
                case 6:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 6);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[6];
                    }
                    break;

                case 5:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 5);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[5];
                    }
                    break;

                case 4:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 4);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[4];
                    }
                    break;

                case 3:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 3);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[3];
                    }
                    break;

                case 2:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 2);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[2];
                    }
                    break;

                case 1:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 1);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[1];
                    }
                    break;
                case 0:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 0);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[0];
                    }
                    if (counterFace < 0 || counterFace >= 6)
                    {
                        counterFace = 6;
                    }
                    break;
            }
        }

    }
    public void CustomBack()
    {
        flexColorPicker.SetActive(false);
        mirror.SetActive(true);
        handModelDummy.SetActive(false);
        //nextButton to Choose the next item from the headItemList // turn arround
        if (headitem == true)
        {
            counterhead++;
            if (counterhead >= 7)
            {
                counterhead = 0;
            }
            switch (counterhead)
            {
                case 0:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    break;

                case 1:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead == 1)
                    {
                        headitems[0].SetActive(true);
                    }
                    break;

                case 2:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }

                    if (counterhead == 2)
                    {
                        headitems[1].SetActive(true);
                    }
                    break;

                case 3:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }

                    if (counterhead == 3)
                    {
                        headitems[0].SetActive(true);
                        headitems[1].SetActive(true);
                    }
                    break;

                case 4:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }

                    if (counterhead == 4)
                    {
                        headitems[2].SetActive(true);
                    }
                    break;

                case 5:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead == 5)
                    {
                        headitems[3].SetActive(true);
                    }
                    break;
                case 6:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    if (counterhead == 6)
                    {
                        headitems[4].SetActive(true);
                    }
                    break;
                case 7:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        headitems[i].SetActive(false);
                    }
                    back = true;
                    if (counterhead >= 7 || counterhead <= 0)
                    {
                        counterhead = 0;
                    }
                    break;
            }
        }

        if (faceItems == true)
        {
            counterFace++;
            if (counterFace >= 7)
            {
                counterFace = 0;
            }
            switch (counterFace)
            {
                case 0:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 0);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[0];
                    }
                    if (counterFace <= 0 || counterFace >= 7)
                    {
                        counterFace = 0;
                    }
                    break;

                case 1:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 1);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[1];
                    }
                    break;

                case 2:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 2);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[2];
                    }
                    break;

                case 3:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 3);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[3];
                    }
                    break;

                case 4:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 4);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[4];
                    }
                    break;

                case 5:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 5);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[5];
                    }
                    break;
                case 6:
                    for (int i = 0; i < faceMaterials.Count; i++)
                    {
                        PlayerPrefs.SetInt("faceItem", 6);
                        robotFace.GetComponent<MeshRenderer>().material = faceMaterials[6];
                    }
                    break;
            }
        }



        if (decalitem == true)
        {
            decalCounter++;
            if (decalCounter >= 7)
            {
                decalCounter = 0;
            }

            switch (decalCounter)
            {
                case 0:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }
                    break;
                case 1:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }
                    if (decalCounter == 1)
                    {
                        decalitems[0].SetActive(true);
                    }
                    break;

                case 2:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }

                    if (decalCounter == 2)
                    {
                        decalitems[1].SetActive(true);
                    }
                    break;

                case 3:
                    for (int i = 0; i < headitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }

                    if (decalCounter == 3)
                    {
                        decalitems[0].SetActive(true);
                        decalitems[1].SetActive(true);
                    }
                    break;

                case 4:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }

                    if (decalCounter == 4)
                    {
                        decalitems[2].SetActive(true);
                    }
                    break;

                case 5:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }
                    if (decalCounter == 5)
                    {
                        decalitems[3].SetActive(true);
                    }
                    break;
                case 6:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }
                    if (decalCounter == 6)
                    {
                        decalitems[4].SetActive(true);
                    }
                    break;
                case 7:
                    for (int i = 0; i < decalitems.Count; i++)
                    {
                        decalitems[i].SetActive(false);
                    }
                    back = true;
                    if (decalCounter >= 7 || decalCounter <= 0)
                    {
                        decalCounter = 0;
                    }
                    break;

            }

        }
    }
    #endregion LeftRightChoose

    public void nextdecalCustom()
    {
        pageOne.SetActive(false);
        pageTwo.SetActive(true);
    }
    public void backdecalCostum()
    {
        pageOne.SetActive(true);
        pageTwo.SetActive(false);
    }
}

