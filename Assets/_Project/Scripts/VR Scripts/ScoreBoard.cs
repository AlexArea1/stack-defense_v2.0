﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.VisionMe.StackDefense;
using System.Text;

public class ScoreBoard : MonoBehaviour
{
    public int playerCount;
    public TextMeshProUGUI score;

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.player.NickName == "")
        {
            PhotonNetwork.player.NickName = "Player#" + Random.Range(0, 9999);
        }

        UpdateScore();
    }



    void UpdateScore()
    {
        var playerNames = new StringBuilder();

        ExitGames.Client.Photon.Hashtable PlayerCustomProps = new ExitGames.Client.Photon.Hashtable();
        PlayerCustomProps["Ping"] = PhotonNetwork.GetPing();
        PhotonNetwork.player.SetCustomProperties(PlayerCustomProps);
        // score.GetComponent<TextMeshProUGUI>().text = PhotonNetwork.player.CustomProperties["Ping"].ToString();


        playerCount = PhotonNetwork.playerList.Length;


        foreach (var player in PhotonNetwork.playerList)
        {
            print("Nickname" + player.NickName);
            playerNames.Append(player.NickName);
        }
        string output =  playerNames.ToString() + " " + PhotonNetwork.player.CustomProperties["Ping"].ToString() + "\n";

        score.text = output;

    }



}
