﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MaxSpec : MonoBehaviour
{
    private TMP_InputField specText;

    private int maxSpec = 4;
    private int minSpec = 0;


    public static int specNumber;

    void Start()
    {
        specText = gameObject.GetComponent<TMP_InputField>();
    }
    void Update()
    {
        int.TryParse(specText.text, out specNumber);

        if (specNumber >= maxSpec)
        {
            specNumber = 4;
            specText.text = maxSpec.ToString();
        }
        if (specNumber <= minSpec)
        {
            specNumber = 0;
            specText.text = minSpec.ToString();
        }


    }
}

