﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomNameAlert : MonoBehaviour
{
    public RoomListing room;
    // Update is called once per frame
    void Update()
    {
        room = this.GetComponent<RoomListing>();
        room.SetRoomNameTextOfObject();
    }
}
