﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangeDecals : MonoBehaviour
{
    public FlexibleColorPicker fcp;
    public List<GameObject> decals = new List<GameObject>();
    
    void Update()
    {
        foreach (GameObject decalItem in decals)
        {
            //decalItem.GetComponent<MeshRenderer>().material.SetColor("Colorfull", fcp.color);
            decalItem.GetComponent<MeshRenderer>().material.color = fcp.color;
        }
    }
}
