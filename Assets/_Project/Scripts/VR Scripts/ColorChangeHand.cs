﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangeHand : MonoBehaviour
{
    public FlexibleColorPicker fcp;
    public List<GameObject> hands = new List<GameObject>();
    
    void Update()
    {
        foreach (GameObject handItem in hands)
        {
            handItem.GetComponent<SkinnedMeshRenderer>().material.color = fcp.color;
        }
    }
}
