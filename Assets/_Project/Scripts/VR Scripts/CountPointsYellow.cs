﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.VisionMe.StackDefense;
using UnityEngine.UI;
using System;

namespace Com.VisionMe.StackDefense
{
    public class CountPointsYellow : MonoBehaviour
    {
        public Text thisText;
        public GameObject playerColorPoints;
        public GameManager gameManager;

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        void Update()
        {
            if (gameManager.player2OnField.playerSetupPlayer2 != null)
            {
                if (gameManager.state == GameState.RUNNING)
                {
                    if (playerColorPoints == null) playerColorPoints = GameObject.Find("YellowpointCounterCollider");

                    thisText.text = playerColorPoints.GetComponent<pointCounter>().points.ToString();
                    gameManager.player2OnField.playerSetupPlayer2.GetComponent<Player>().points = playerColorPoints.GetComponent<pointCounter>().points;
                }

                if (gameManager.player2OnField.playerSetupPlayer2.GetComponent<Player>().state != PlayerState.START)
                {
                    if (playerColorPoints == null) playerColorPoints = null;

                    thisText.text = "";
                }
            }

            if (gameManager.player2OnField.playerSetupPlayer2 == null)
            {
                thisText.text = "";
            }

        }
    }
}