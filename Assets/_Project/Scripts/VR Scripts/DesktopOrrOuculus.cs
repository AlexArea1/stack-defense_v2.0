﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DesktopOrrOuculus : MonoBehaviour
{

    public void OculusVrVersion()
    {
        SceneManager.LoadScene("Launcher_VR");
    }

    public void DesctopVersion()
    {
        SceneManager.LoadScene("Launcher_Desktop");
    }


}
