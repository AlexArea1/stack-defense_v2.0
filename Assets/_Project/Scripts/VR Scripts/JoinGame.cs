﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinGame : MonoBehaviour
{
    public GameObject create;
    public GameObject quit;
    public GameObject Keyboard;

    public List<GameObject> listHide = new List<GameObject>();
    public List<GameObject> listDontHide = new List<GameObject>();




    public void OpenJoinMenu()
    {
        foreach (GameObject item in listHide)
        {
            item.SetActive(true);
        }
        create.SetActive(false);
        quit.SetActive(false);

    }

    public void CloseJoinMenu()
    {
        foreach (GameObject item in listHide)
        {
            item.SetActive(false);
        }
        create.SetActive(true);
        quit.SetActive(true);
}

    public void OpenJoinManual()
    {
        Keyboard.SetActive(true);
    }


}


