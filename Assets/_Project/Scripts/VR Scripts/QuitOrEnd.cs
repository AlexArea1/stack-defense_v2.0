﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitOrEnd : MonoBehaviour
{

    public void EndGame()
    {
        Application.Quit();
    }
}
