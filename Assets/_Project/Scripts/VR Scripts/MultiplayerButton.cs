﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiplayerButton : MonoBehaviour
{
    public void CustomScene()
    {
        SceneManager.LoadScene("CharacterTest");
    }

    public void TutorialScene()
    {
        SceneManager.LoadScene("Tutorial");
    }
}