﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using Com.VisionMe.StackDefense;
using UnityEngine.Networking;
using System;
using System.IO;
using TMPro;

/// <summary>
/// This namespace works as an adapter between StackDefense and Oculus
/// </summary>
namespace Com.VisionMe.VR
{
    public enum GetType
    {
        GET, GETDOWN, GETUP
    }

    [Serializable]
    public class EventAndInput
    {
        public UnityEvent functionEvent;
        public InputFeatureUsage<bool> inputFeatureUsage;
        public bool leftHand;
        public GetType getType;
    }

    public enum OwnInputUsages
    {
        primaryButton,
        primaryTouch,
        secondaryButton,
        secondaryTouch,
        gripButton,
        triggerButton,
        StickClick,
        StickTouch
    }

    public struct XrAndOwnInputUsage
    {
        public OwnInputUsages ownUsage;
        public InputFeatureUsage<bool> xrUsage;
    }

    /// <summary>
    /// This class handles the controller inputs
    /// The events are loaded from a json file and added to the event Dictionary
    /// The event Dictionary contains the name for the event, the button is is activated with and the function of the event
    /// The Objects that contain the functions of the events add these themsefles in their awake function (can't be saves in json file)
    /// Adds an Object a function to an event that does not exist, it creates a new event that automaticly gets saved in the json
    /// </summary>
    public class VR_Input : MonoBehaviour
    {
        public Dictionary<string, EventAndInput> events { get; private set; } = new Dictionary<string, EventAndInput>();

        private Dictionary<InputFeatureUsage<bool>, bool> actualInputFeaturesLeft;
        private Dictionary<InputFeatureUsage<bool>, bool> lastInputFeaturesLeft;
        private Dictionary<InputFeatureUsage<bool>, bool> actualInputFeaturesRight;
        private Dictionary<InputFeatureUsage<bool>, bool> lastInputFeaturesRight;

        private List<InputDevice> leftHandDevices = new List<InputDevice>();
        private List<InputDevice> rightHandDevices = new List<InputDevice>();

        [HideInInspector] public ControlsMenu controlsMenu;

        private VRInputType inputType = VRInputType.UNKNOWN;

        public static List<XrAndOwnInputUsage> possibleControls = null;

        [SerializeField] private string saveName = "ControlsJson";
        private string savePath;


        [SerializeField] private TextMeshProUGUI showNameText;
        void Awake()
        {
            if (showNameText)
                showNameText.text = $"{XRDevice.model}";

            controlsMenu = FindObjectOfType<ControlsMenu>();

            SetAllPossibleControls();

            //savePath = $"{Application.streamingAssetsPath}/{saveName}.json";
            savePath = $"{Application.persistentDataPath}/{saveName}.json";

            //if (Application.platform == RuntimePlatform.Android)
            //{
            //    savePath = "jar:file://" + Application.dataPath + "!/assets";
            //}

            if (!LoadControlFromJson())
                SaveControlsToJson();
        }

        //Add all possible Controls that can be used to a List
        private void SetAllPossibleControls()
        {
            if (possibleControls == null)
            {
                possibleControls = new List<XrAndOwnInputUsage>();
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.primaryButton, xrUsage = CommonUsages.primaryButton });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.primaryTouch, xrUsage = CommonUsages.primaryTouch });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.secondaryButton, xrUsage = CommonUsages.secondaryButton });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.secondaryTouch, xrUsage = CommonUsages.secondaryTouch });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.gripButton, xrUsage = CommonUsages.gripButton });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.triggerButton, xrUsage = CommonUsages.triggerButton });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.StickClick, xrUsage = CommonUsages.primary2DAxisClick });
                possibleControls.Add(new XrAndOwnInputUsage() { ownUsage = OwnInputUsages.StickTouch, xrUsage = CommonUsages.primary2DAxisTouch });
            }

            SetPossibleControls(ref actualInputFeaturesLeft);
            SetPossibleControls(ref actualInputFeaturesRight);
            SetPossibleControls(ref lastInputFeaturesLeft);
            SetPossibleControls(ref lastInputFeaturesRight);
        }

        //Fill the dictionarys that save the different states of the inputs
        private void SetPossibleControls(ref Dictionary<InputFeatureUsage<bool>, bool> _dic)
        {
            _dic = new Dictionary<InputFeatureUsage<bool>, bool>();
            for (int i = 0; i < possibleControls.Count; i++)
            {
                _dic.Add(possibleControls[i].xrUsage, false);
            }
        }

        void Update()
        {
            if (!CheckToContinue())
            {
                Debug.Log("ret");
                return;
            }

            //Get the actual Controler states
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Left, leftHandDevices);
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right, rightHandDevices);

            UpdateControlerValues();

            ExecuteEvents();
        }

        //Chekc if there is a problem
        private bool CheckToContinue()
        {
            //Check if there is an XR Device
            if (!XRDevice.isPresent)
            {
                Debug.Log("No XRDevice found");
                return false;
            }

            //Check if it is an known Device
            if (inputType == VRInputType.UNKNOWN)
            {
                inputType = GetVRInputType();

                if (inputType == VRInputType.UNKNOWN)
                {
                    Debug.Log("Unknown Device");
                    return false;
                }
            }

            return true;
        }

        //Check if the input of an event got pressed and execute it
        private void ExecuteEvents()
        {
            foreach (var e in events.Values)
            {
                //Check if there is a function for the event
                if (e.functionEvent != null)
                {
                    switch (e.getType)
                    {
                        case VR.GetType.GET:
                            if (GetUsage(e.inputFeatureUsage, e.leftHand))
                                e.functionEvent.Invoke();
                            break;
                        case VR.GetType.GETDOWN:
                            if (GetUsageDown(e.inputFeatureUsage, e.leftHand))
                                e.functionEvent.Invoke();
                            break;
                        case VR.GetType.GETUP:
                            if (GetUsageUp(e.inputFeatureUsage, e.leftHand))
                                e.functionEvent.Invoke();
                            break;
                        default:
                            Debug.LogError("Unknown GetType");
                            break;
                    }
                }
            }
        }

        //Add a new event, when the event exists (through save json) just add the function
        public void AddEvent(string _name, EventAndInput _eventAndInput)
        {
            if (events.ContainsKey(_name))
            {
                events[_name].functionEvent = _eventAndInput.functionEvent;
            }
            else
            {
                events.Add(_name, _eventAndInput);
                SaveControlsToJson();
            }

            UpdateControlsMenu();
        }

        //Update the Controls Menu that all events are shown
        private void UpdateControlsMenu()
        {
            if (!controlsMenu)
                controlsMenu = FindObjectOfType<ControlsMenu>();

            if (controlsMenu)
            {
                controlsMenu.UpdateLayout();
            }
            else
                Debug.Log("No ControlsMenu found");
        }

        //Update the dictionarys with the actual und last controller states
        private void UpdateControlerValues()
        {
            bool hasController = true;
            if (leftHandDevices.Count <= 0)
                hasController = false;

            List<InputFeatureUsage<bool>> inputFeatures = new List<InputFeatureUsage<bool>>();
            foreach (var value in actualInputFeaturesLeft.Keys)
            {
                inputFeatures.Add(value);
            }
            for (int i = 0; i < inputFeatures.Count; i++)
            {
                bool save = false;
                if (hasController)
                {
                    //Get the actual quest trigger state
                    if (inputFeatures[i] == CommonUsages.triggerButton)
                    {
                        float triggerValue;
                        leftHandDevices[0].TryGetFeatureValue(CommonUsages.trigger, out triggerValue);
                        save = triggerValue >= 0.8f;
                    }
                    //Get the actual state
                    else
                        leftHandDevices[0].TryGetFeatureValue(inputFeatures[i], out save);
                }

                //Set the last state
                lastInputFeaturesLeft[inputFeatures[i]] = actualInputFeaturesLeft[inputFeatures[i]];
                //Save the actual state
                actualInputFeaturesLeft[inputFeatures[i]] = save;
            }

            hasController = true;
            if (rightHandDevices.Count <= 0)
                hasController = false;

            inputFeatures = new List<InputFeatureUsage<bool>>();
            foreach (var value in actualInputFeaturesRight.Keys)
            {
                inputFeatures.Add(value);
            }
            for (int i = 0; i < inputFeatures.Count; i++)
            {
                bool save = false;
                if (hasController)
                    rightHandDevices[0].TryGetFeatureValue(inputFeatures[i], out save);

                lastInputFeaturesRight[inputFeatures[i]] = actualInputFeaturesRight[inputFeatures[i]];
                actualInputFeaturesRight[inputFeatures[i]] = save;
            }
        }

        #region GetUsageFunctions
        //Return whether the usage/input is pressed at the moment
        private bool GetUsage(InputFeatureUsage<bool> _inputFeatureUsage, bool _left)
        {
            if (_left)
                return actualInputFeaturesLeft[_inputFeatureUsage];
            else
                return actualInputFeaturesRight[_inputFeatureUsage];
        }
        //Return whether the usage/input was pressed last frame
        private bool GetUsageDown(InputFeatureUsage<bool> _inputFeatureUsage, bool _left)
        {
            if (_left)
            {
                //Debug.Log($"Actual Input features: {actualInputFeaturesLeft}");
                //Debug.Log($"last Input features {lastInputFeaturesLeft}");
                //Debug.Log($"Input feature {_inputFeatureUsage}");
                return actualInputFeaturesLeft[_inputFeatureUsage] && !lastInputFeaturesLeft[_inputFeatureUsage];
            }
            else
                return actualInputFeaturesRight[_inputFeatureUsage] && !lastInputFeaturesRight[_inputFeatureUsage];
        }
        //Return whether the usage/input was released last frame
        private bool GetUsageUp(InputFeatureUsage<bool> _inputFeatureUsage, bool _left)
        {
            if (_left)
                return !actualInputFeaturesLeft[_inputFeatureUsage] && lastInputFeaturesLeft[_inputFeatureUsage];
            else
                return !actualInputFeaturesRight[_inputFeatureUsage] && lastInputFeaturesRight[_inputFeatureUsage];
        }
        #endregion

        /// <summary>
        /// Starts vibration with the given parameters
        /// </summary>
        /// <param name="frequency">Frequency of the vibration</param>
        /// <param name="amplitude">Amplitude of the vibration</param>
        /// <param name="duration">Duration of the vibration</param>
        /// <param name="controllerMask">Controller the vibration should be applied to</param>
        public void Vibrate(float frequency, float amplitude, float duration/*, OVRInput.Controller controllerMask*/)
        {
            //TODO: fix the vibration
            //HapticCapabilities hapticCapabilities;
            //leftHandDevices[0].TryGetHapticCapabilities(out hapticCapabilities);
            //
            //if (hapticCapabilities.supportsImpulse)
            //{
            //    leftHandDevices[0].SendHapticImpulse(0, amplitude, duration);
            //}

            //StartCoroutine(Vibration(frequency, amplitude, duration, controllerMask));
        }

        //Check if the VR Devis is supported
        private VRInputType GetVRInputType()
        {
            if (XRDevice.model.Contains("Vive"))
            {
                return VRInputType.VIVE;
            }
            else if (XRDevice.model.Contains("Oculus"))
            {
                return VRInputType.OCULUS;
            }

            return VRInputType.UNKNOWN;
        }

        #region SaveControlsFunctions
        //Load the control json file and add the events
        private bool LoadControlFromJson()
        {
            if (!File.Exists(savePath))
            {
                Debug.Log("No Controls File found");
                return false;
            }

            string load = System.IO.File.ReadAllText(savePath);
            ControlsData data = new ControlsData();
            JsonUtility.FromJsonOverwrite(load, data);
            Debug.Log("Loading Controls");

            //Convert the data into the events
            events = new Dictionary<string, EventAndInput>();
            for (int i = 0; i < data.keys.Count; i++)
            {
                EventAndInput eai = new EventAndInput();
                eai.leftHand = data.leftHands[i];
                eai.getType = data.getTypes[i];

                for (int j = 0; j < possibleControls.Count; j++)
                {
                    if (data.usages[i] == possibleControls[j].ownUsage)
                    {
                        eai.inputFeatureUsage = possibleControls[j].xrUsage;
                        break;
                    }
                }

                events.Add(data.keys[i], eai);
            }

            UpdateControlsMenu();

            return true;
        }

        //Save the events and inputs into the control json file
        public void SaveControlsToJson()
        {
            ControlsData data = new ControlsData();

            //Convert events into the data
            foreach (var item in events)
            {
                data.keys.Add(item.Key);
                data.leftHands.Add(item.Value.leftHand);
                data.getTypes.Add(item.Value.getType);

                for (int i = 0; i < possibleControls.Count; i++)
                {
                    if (item.Value.inputFeatureUsage == possibleControls[i].xrUsage)
                    {
                        data.usages.Add(possibleControls[i].ownUsage);
                        break;
                    }
                }
            }

            string save = JsonUtility.ToJson(data);
            System.IO.File.WriteAllText(Application.persistentDataPath + "/ControlsJson.json", save);

            //Check if a ControlJson File exists, when not create one
            if (!System.IO.File.Exists(savePath))
                System.IO.File.Create(savePath);

            System.IO.File.WriteAllText(savePath, save);
        }
        #endregion
    }
}