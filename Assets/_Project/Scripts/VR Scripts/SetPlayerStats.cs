﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Com.VisionMe.StackDefense;

public class SetPlayerStats : MonoBehaviour
{

    public GameObject pingText;
    public GameObject playerText;
    public GameObject playerPoints;
    public GameObject muteToggle;

    Player1Exist player1;
    Player2Exist player2;
    Player3Exist player3;
    Player4Exist player4;

    bool variablesSynced = false;


    void Start()
    {
        playerText.GetComponent<TextMeshProUGUI>().text = PhotonNetwork.player.NickName;

    }
    void Update()
    {
        if (muteToggle.GetComponent<Toggle>().enabled == true)
        {
            Player.playerVoiceVolume = 0f;
        }
        else
        {
            Player.playerVoiceVolume = 1f;
        }
        playerPoints.GetComponent<TextMeshProUGUI>().text = Player.playerPoints.ToString();

        ExitGames.Client.Photon.Hashtable PlayerCustomProps = new ExitGames.Client.Photon.Hashtable();
        PlayerCustomProps["Ping"] = PhotonNetwork.GetPing();
        PhotonNetwork.player.SetCustomProperties(PlayerCustomProps);
        pingText.GetComponent<TextMeshProUGUI>().text = PhotonNetwork.player.CustomProperties["Ping"].ToString();

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(pingText.GetComponent<TextMeshProUGUI>().text);
            stream.SendNext(playerPoints.GetComponent<TextMeshProUGUI>().text);
            stream.SendNext(playerText.GetComponent<TextMeshProUGUI>().text);

        }
        else if (stream.isReading)
        {
            pingText.GetComponent<TextMeshProUGUI>().text = (string)stream.ReceiveNext();
            playerPoints.GetComponent<TextMeshProUGUI>().text = (string)stream.ReceiveNext();
            playerText.GetComponent<TextMeshProUGUI>().text = (string)stream.ReceiveNext();
        }
    }


}
