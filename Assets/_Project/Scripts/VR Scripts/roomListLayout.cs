﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class roomListLayout : MonoBehaviour
{

    public Button server;
    GameObject serverListingObj;

    public int serverNumbers;

    public List<Button> servers = new List<Button>();

    private void Awake()
    {
        GetRoomList();
        GetRoomList();
    }
    public void Start()
    {
        StartCoroutine(refresh1());
        StartCoroutine(refresh());
        //for (int i = 0; i < servers.Count; i++)
        //{
        //    Destroy(servers[i].gameObject);
        //}
        Transform panelTransform = this.transform;
        foreach (Transform child in panelTransform)
        {
            Destroy(child.gameObject);
        }

        //servers.Clear();
        //Destroy(serverListingObj);
        RoomInfo[] infoRooms = PhotonNetwork.GetRoomList();
        foreach (RoomInfo room in infoRooms)
        {
            Debug.Log("Created server");
            serverListingObj = Instantiate(server.gameObject);
            serverListingObj.transform.SetParent(transform, false);
            //server.name = room.Name + " | " + room.PlayerCount + " / " + room.MaxPlayers;
            server.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = room.Name;
            server.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = room.PlayerCount.ToString() + "/" + room.MaxPlayers;
            servers.Add(server);
        }
    }


    public void GetRoomList()
    {
        //for (int i = 0; i < servers.Count; i++)
        //{
        //    Destroy(servers[i].gameObject);
        //}
        Transform panelTransform = this.transform;
        foreach (Transform child in panelTransform)
        {
            Destroy(child.gameObject);
        }

        //servers.Clear();
        //Destroy(serverListingObj);
        RoomInfo[] infoRooms = PhotonNetwork.GetRoomList();
        foreach (RoomInfo room in infoRooms)
        {
            Debug.Log("Created server");
            serverListingObj = Instantiate(server.gameObject);
            serverListingObj.transform.SetParent(transform, false);
            //server.name = room.Name + " | " + room.PlayerCount + " / " + room.MaxPlayers;
            server.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = room.Name;
            server.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = room.PlayerCount.ToString() + "/" + room.MaxPlayers;
            servers.Add(server);
        }
    }
    IEnumerator refresh()
    {
        yield return new WaitForSeconds(5);
        GetRoomList();
        GetRoomList();
        GetRoomList();
        StartCoroutine(refresh());
        yield break;
    }

    IEnumerator refresh1()
    {
        yield return new WaitForSeconds(1);
        GetRoomList();
        GetRoomList();
        GetRoomList();
        yield break;
    }
}




