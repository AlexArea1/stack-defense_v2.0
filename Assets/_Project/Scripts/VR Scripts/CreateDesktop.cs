﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreateDesktop : MonoBehaviour
{
    public GameObject serverSetting;
    public GameObject joinButton;
    public GameObject Quit;
    public GameObject CreateGame;

    public void HideThis()
    {

        serverSetting.SetActive(true);
        CreateGame.SetActive(false);
        joinButton.SetActive(false);
        Quit.SetActive(false);
    }
    public void DontHidethis()
    {
 
        serverSetting.SetActive(false);
        CreateGame.SetActive(true);
        joinButton.SetActive(true);
        Quit.SetActive(true);
    }

}
