﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.VisionMe.StackDefense;
using UnityEngine.UI;
using System;

namespace Com.VisionMe.StackDefense
{
    public class CountPointsBlue : MonoBehaviour
    {
        public Text thisText;
        public GameObject playerColorPoints;
        public GameManager gameManager;


        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        void Update()
        {
            if (gameManager.player1OnField.playerSetupPlayer1 != null)
            {
                if (gameManager.state == GameState.RUNNING)
                {
                    playerColorPoints = GameObject.Find("BluepointCounterCollider");

                    thisText.text = playerColorPoints.GetComponent<pointCounter>().points.ToString();
                    gameManager.player1OnField.playerSetupPlayer1.GetComponent<Player>().points = playerColorPoints.GetComponent<pointCounter>().points;
                }

                if (gameManager.player1OnField.playerSetupPlayer1.GetComponent<Player>().state != PlayerState.START)
                {
                    if (playerColorPoints == null) playerColorPoints = null;

                    thisText.text = "";
                }
            }

            if (gameManager.player1OnField.playerSetupPlayer1 == null)
            {
                thisText.text = "";
            }
        }
    }
}