﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.VisionMe.StackDefense;
using UnityEngine.UI;
using System;

namespace Com.VisionMe.StackDefense
{
    public class CountPoints : MonoBehaviour
    {
        public List<Text> pointsFromPlayerList = new List<Text>();

        private Text thisText;

        private void Awake()
        {
            thisText = GetComponent<Text>();
        }

        void Update()
        {
            for (int i = 0; i < pointsFromPlayerList.Count; i++)
            {
                if (pointsFromPlayerList[i].gameObject.activeInHierarchy)
                {
                    thisText.text = pointsFromPlayerList[i].text;
                    break;
                }
            }
        }
    }
}