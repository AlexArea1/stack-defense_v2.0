﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxPanel : MonoBehaviour
{
    // Choose your Skybox code;
    public string ChooseSkyBox;
    public GameObject ServerSetting;
    public GameObject OpenMoreSkyboxesChoose;

    public Material cloudSun;
    public Material blueSun;
    public Material fantasy1;
    public Material fantasy2;
    public Material anotherPlanet;
    public Material nebula;
    public Material skyOnFire;
    public Material space;
    public Material goldenSun;
    public Material gloriousPink;

    public void SpaceMap()
    {
        ChooseSkyBox = "Space";
        ServerSetting.SetActive(true);
       // OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = space;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }
    public void GloriousPink()
    {
        ChooseSkyBox = "GloriousPink";
        ServerSetting.SetActive(true);
       // OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = gloriousPink;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }
    public void CloudSun1()
    {
        ChooseSkyBox = "CloudSun";
        ServerSetting.SetActive(true);
     //   OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = cloudSun;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }

    public void BlueSun2()
    {
        ChooseSkyBox = "BlueSun";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = blueSun;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }

    public void GoldenSun3()
    {
        ChooseSkyBox = "GoldenSun";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = goldenSun;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }

    public void Fantasy14()
    {
        ChooseSkyBox = "Fantasy1";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = fantasy1;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }

    public void Fantasy25()
    {
        ChooseSkyBox = "Fantasy2";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = fantasy2;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }

    public void AnotherPlanet6()
    {
        ChooseSkyBox = "AnotherPlanet";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = anotherPlanet;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }
    public void Nebula()
    {
        ChooseSkyBox = "Nebula";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = nebula;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }
    public void SkyOnFire8()
    {
        ChooseSkyBox = "SkyOnFire";
        ServerSetting.SetActive(true);
      //  OpenMoreSkyboxesChoose.SetActive(false);
        RenderSettings.skybox = skyOnFire;
        PlayerPrefs.SetString("SkyBoxChoose", ChooseSkyBox);
    }

    public void Back()
    {
        ServerSetting.SetActive(true);
        OpenMoreSkyboxesChoose.SetActive(false);
    }

    public void OpenMoreSkyboxes()
    {
        OpenMoreSkyboxesChoose.SetActive(true);
    }
}

