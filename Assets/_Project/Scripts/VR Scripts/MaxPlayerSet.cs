﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class MaxPlayerSet : MonoBehaviour
{
    private TMP_InputField playernumberText;

    private int maxPlayer = 4;
    private int minPlayer = 2;

    public int PlayerNumber;

    void Start()
    {
        playernumberText = gameObject.GetComponent<TMP_InputField>();
    }
    void Update()
    {
        int.TryParse(playernumberText.text, out PlayerNumber);

        if (PlayerNumber <= minPlayer)
        {
            PlayerNumber = 0;
            playernumberText.text = minPlayer.ToString();

        }

        else if (PlayerNumber >= maxPlayer)
        {
            PlayerNumber = 4;
            playernumberText.text = maxPlayer.ToString();
        }
     
    }
}
