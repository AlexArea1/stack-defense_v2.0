﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class HideAnotherObjects
{
    public GameObject targetgameObjects;
}
public class Create : MonoBehaviour
{
    GameObject targetGameObjects;
    public GameObject serverSetting;
    public GameObject Quit;
    public GameObject CreateGame;

    public List<HideAnotherObjects> HideThisObjectsList = new List<HideAnotherObjects>();
    public List<GameObject> visibleMultiplayerList = new List<GameObject>();

    public GameObject Keyboard;

    public void HideThis()
    {
        foreach (HideAnotherObjects item in HideThisObjectsList)
        {
            item.targetgameObjects.SetActive(false);
        }
        serverSetting.SetActive(true);
        //CreateGame.SetActive(false);
        Quit.SetActive(false);
    }
    public void DontHidethis()
    {
        foreach (HideAnotherObjects item in HideThisObjectsList)
        {
            item.targetgameObjects.SetActive(true);
        }
        serverSetting.SetActive(false);
        CreateGame.SetActive(true);
        Quit.SetActive(true);
    }


    public void multiplayerVisible()
    {
        foreach (GameObject item in visibleMultiplayerList)
        {
            item.SetActive(true);
        }
    }

    public void OculusQuest()
    {
        CreateGame.SetActive(true);
        Quit.SetActive(true);

    }

    public void SwitchKeyboardPositionToRoomName()
    {
        Keyboard.transform.position = new Vector3(0,0.7f,2.45f);
        Keyboard.transform.eulerAngles = new Vector3(46.36f, 0, 0);
    }

    public void SwitchKeyboardPositionToPlayerName()
    {
        Keyboard.transform.position = new Vector3(-0.63f, 2.43f, 3.25f);
        Keyboard.transform.eulerAngles = new Vector3(0, 0, 0);
    }

    public void KeyboardActivity()
    {
        Keyboard.SetActive(false);
    }

    public void DesktopVrUser()
    {
        SceneManager.LoadScene("Launcher_Desktop");
    }
}
