﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class connectToServer : MonoBehaviour
{
    LobbyManager lobbyManager;
    Button serverButton;


    public TextMeshProUGUI ping;
    public TextMeshProUGUI roomName;
    public TextMeshProUGUI maxPlayers;
    public GameObject nameTxt;

    void Start()
    {
        serverButton = this.GetComponent<Button>();
    }

    void Update()
    {
        if (!lobbyManager) FindObjectOfType<LobbyManager>();
        ExitGames.Client.Photon.Hashtable PlayerCustomProps = new ExitGames.Client.Photon.Hashtable();
        PlayerCustomProps["Ping"] = PhotonNetwork.GetPing();
        PhotonNetwork.player.SetCustomProperties(PlayerCustomProps);
        ping.text = PhotonNetwork.player.CustomProperties["Ping"].ToString();
        if(nameTxt == null)nameTxt = GameObject.Find("NameText");
    }


    public void JoinRoom()
    {
        PlayerPrefs.SetString("PlayerName", nameTxt.GetComponent<TextMeshProUGUI>().text);
        TextMeshProUGUI EntryText = roomName;
        PhotonNetwork.player.NickName = PlayerPrefs.GetString("PlayerName");
        PhotonNetwork.JoinRoom(EntryText.text);

    }

    
}
