﻿using Com.VisionMe.StackDefense;
using Com.VisionMe.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class SpectatorMovement : MonoBehaviour
{
    private GameManager gamemManager;
    private VR_Input vrInput;
    private XRController leftController;

    [SerializeField] private float midSpeed = 0.3f;
    [SerializeField] private float circularSpeed = 1;
    [SerializeField] private float minMidDistance = 3;
    [SerializeField] private float maxMidDistance = 7;
    [SerializeField] private float height = 0;

    private float circularPosition = 0;
    private float midDistance = 5;

    private List<InputDevice> leftHandDevices = new List<InputDevice>();

    private VRInputType inputType = VRInputType.UNKNOWN;

    private void Awake()
    {
        gamemManager = FindObjectOfType<GameManager>();
        vrInput = FindObjectOfType<VR_Input>();

        foreach (var controller in GetComponentsInChildren<XRController>())
        {
            if(controller.controllerNode == XRNode.LeftHand)
            {
                leftController = controller;
                break;
            }
        }
        if (!leftController)
            Debug.LogError("No left Controller found");
    }

    private void Update()
    {
        if (!CheckToExecuteUpdate())
            return;

        InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Left, leftHandDevices);

        if(leftHandDevices.Count > 0)
        {
            bool returnBool = false;

            if(inputType == VRInputType.OCULUS || inputType == VRInputType.QUEST)
                leftHandDevices[0].TryGetFeatureValue(CommonUsages.primary2DAxisTouch, out returnBool);
            else if(inputType == VRInputType.VIVE)
                leftHandDevices[0].TryGetFeatureValue(CommonUsages.primary2DAxisClick, out returnBool);
            if (returnBool)
            {
                Vector2 touchPos = new Vector2();
                leftHandDevices[0].TryGetFeatureValue(CommonUsages.primary2DAxis, out touchPos);

                float angle = Mathf.Atan2(touchPos.x, touchPos.y);
                angle *= Mathf.Rad2Deg;

                if (angle < 0)
                    angle += 360;

                angle -= 45;

                //RIGHT
                if(angle >= 0 && angle < 90)
                {
                    circularPosition -= circularSpeed * Time.deltaTime;
                }
                //DOWN
                else if(angle >= 90 && angle < 180)
                {
                    if (midDistance < maxMidDistance)
                        midDistance += midSpeed * Time.deltaTime;
                }
                //LEFT
                else if(angle >= 180 && angle < 270)
                {
                    circularPosition += circularSpeed * Time.deltaTime;
                }
                //TOP
                else if(angle >= 270 && angle < 360)
                {
                    if (midDistance > minMidDistance)
                        midDistance -= midSpeed * Time.deltaTime;
                }

                ////Vectort from Controller
                //Vector3 moveDirection = leftController.transform.forward * touchPos.y + leftController.transform.right * touchPos.x;
                //
                ////Move CameraRig
                //transform.position += moveDirection * moveSpeed * Time.deltaTime;
            }

            Vector3 pos = new Vector3(midDistance, height, 0);
            pos = Quaternion.Euler(0, circularPosition, 0) * pos;

            transform.position = pos;
            transform.forward = -pos;
        }
    }

    private bool CheckToExecuteUpdate()
    {
        if (!gamemManager.isSpectator)//(PlayerPrefs.GetInt("isSpectator") == 0)
            return false;

        if (!XRDevice.isPresent)
        {
            Debug.Log("No XRDevice found");
            return false;
        }

        if (inputType == VRInputType.UNKNOWN)
        {
            inputType = GetVRInputType();

            if (inputType == VRInputType.UNKNOWN)
            {
                Debug.Log("Unknown Device");
                return false;
            }
        }

        return true;
    }

    private VRInputType GetVRInputType()
    {
        if (XRDevice.model.Contains("Vive"))
        {
            return VRInputType.VIVE;
        }
        else if (XRDevice.model.Contains("Oculus"))
        {
            return VRInputType.OCULUS;
        }

        return VRInputType.UNKNOWN;
    }
}
