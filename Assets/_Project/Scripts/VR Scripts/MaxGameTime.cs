﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MaxGameTime : MonoBehaviour
{
    private TMP_InputField gameTimeText;

    private int maxGT = 600;
    private int minGT = 120;


    public static int gameTimeNumber = 120;

    public Slider GameTimeSlider;

    void Start()
    {
        gameTimeText = gameObject.GetComponent<TMP_InputField>();
       

    }
    void Update()
    {
        int.TryParse(gameTimeText.text, out gameTimeNumber);

        //if (gameTimeNumber >= maxGT)
        //{
        //    gameTimeNumber = maxGT;
        //    gameTimeText.text = maxGT.ToString();
        //}

        //if (gameTimeNumber < minGT)
        //{
        //    gameTimeNumber = minGT;
        //    gameTimeText.text = minGT.ToString();
        //}


        gameTimeText.text = Mathf.RoundToInt(GameTimeSlider.value).ToString();
    }
}
