﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Com.VisionMe.StackDefense;

public class pointCounter : MonoBehaviour
{
    pointCounter instance;
    //public GameObject pointCounterText;
    public Text middlePoints;

    public int points = 0;
    bool pointsup = false;

    public List<GameObject> countedBlocks = new List<GameObject>();
    public Text playerPointsOverThePlattform;
 

    private void Awake()
    {
        //countedBlocks = new List<GameObject>();

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void Update()
    {
        //if (pointCounterText == null)
        //{
        //    if (middlePoints.color == Color.blue)
        //    {
        //        pointCounterText = GameObject.Find("blueCounter");
        //    }
        //    if (middlePoints.color == Color.red)
        //    {
        //        pointCounterText = GameObject.Find("redCounter");
        //    }
        //    if (middlePoints.color == Color.green)
        //    {
        //        pointCounterText = GameObject.Find("greenCounter");
        //    }
        //    if (middlePoints.color == Color.yellow)
        //    {
        //        pointCounterText = GameObject.Find("yellowCounter");
        //    }
        //}
        middlePoints.text = points.ToString();
        playerPointsOverThePlattform.text = points.ToString();
        //pointCounterText.GetComponent<Text>().text = points.ToString();
        CheckPoints();
    }

    private void OnTriggerEnter(Collider other)
    {
        //pointsup = true;
        if (other.gameObject.tag == "StackingObject")
        {
            pointsup = true;
            if (pointsup == true)
            {
                if (!countedBlocks.Contains(other.gameObject))
                {
                    countedBlocks.Add(other.gameObject);
                    points = points + 1;
                }
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (countedBlocks.Contains(other.gameObject))
        {
            countedBlocks.Remove(other.gameObject);
            pointsup = false;
            points = points - 1;
        }
        if (other.gameObject == null)
        {
            points = points - 1;
        }
    }

    public void CheckPoints()
    {
        for (int i = countedBlocks.Count - 1; i >= 0; i--)
        {
            if (countedBlocks[i] != null)
            {
                Debug.Log("Exist +1 point");
            }
            else
            {
                countedBlocks.RemoveAt(i);
                points = points - 1;
            }

        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(points);
        }
        else if (stream.isReading)
        {
            points = (int)stream.ReceiveNext();
        }
    }
}

