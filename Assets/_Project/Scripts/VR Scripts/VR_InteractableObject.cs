﻿using ExitGames.Client.Photon.Voice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//using OVR;
//using Com.VisionMe.OVRE;

/// <summary>
/// This namespace works as an adapter between StackDefense and Oculus
/// </summary>
namespace Com.VisionMe.VR
{
    /// <summary>
    /// An object that can be grabbed and interacted with
    /// </summary>
    public class VR_InteractableObject : MonoBehaviour
    {
        [Tooltip("Emitted when the object is grabbed")]
        public UnityEvent onGrab;

        // True if the object is currently grabbed
        private bool isGrabbed;

        private Rigidbody rigidBody;

        private Transform copyTransform;

        // Use this for initialization
        void Awake() => rigidBody = GetComponent<Rigidbody>();

        // Update is called once per frame
        void Update()
        {
            if (!isGrabbed)
                return;

            transform.position = copyTransform.position;
            transform.rotation = copyTransform.rotation;
        }

        /// <summary>
        /// Gets whether the object is currently grabbed
        /// </summary>
        public bool IsGrabbed => isGrabbed;

        public void Grab(Transform _handTransform)
        {
            rigidBody.isKinematic = true;

            copyTransform = new GameObject("GrabbedAnchorPoint").transform;
            copyTransform.position = transform.position;
            copyTransform.rotation = transform.rotation;
            copyTransform.parent = _handTransform;

            isGrabbed = true;
            onGrab.Invoke();
        }

        public void Release()
        {
            isGrabbed = false;

            rigidBody.isKinematic = false;

            Destroy(copyTransform.gameObject);
            copyTransform = null;
        }
    }
}