﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.VisionMe.StackDefense;
using UnityEngine.UI;
using System;

namespace Com.VisionMe.StackDefense
{
    public class CountPointsRed : MonoBehaviour
    {
        public Text thisText;
        public GameObject playerColorPoints;
        public GameManager gameManager;

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        void Update()
        {
            if (gameManager.player4OnField.playerSetupPlayer4 != null)
            {
                if (gameManager.state == GameState.RUNNING)
                {
                    if (playerColorPoints == null) playerColorPoints = GameObject.Find("RedCounterCollider");

                    thisText.text = playerColorPoints.GetComponent<pointCounter>().points.ToString();
                    gameManager.player4OnField.playerSetupPlayer4.GetComponent<Player>().points = playerColorPoints.GetComponent<pointCounter>().points;
                }

                if (gameManager.player4OnField.playerSetupPlayer4.GetComponent<Player>().state != PlayerState.START)
                {
                    if (playerColorPoints == null) playerColorPoints = null;

                    thisText.text = "";
                }
            }

            if (gameManager.player4OnField.playerSetupPlayer4 == null)
            {
                thisText.text = "";
            }
        }
    }
}