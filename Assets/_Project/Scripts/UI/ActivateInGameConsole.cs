﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IngameDebugConsole;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    public class ActivateInGameConsole : MonoBehaviour
    {
        private Canvas _inGameConsoleCanvas;

        private void Awake()
        {
            var debugLogManager = Resources.FindObjectsOfTypeAll<DebugLogManager>().FirstOrDefault();
            if (debugLogManager == null)
            {
                Debug.LogWarning($"could not find debugLogManager");
                return;
            }

            _inGameConsoleCanvas = debugLogManager.GetComponent<Canvas>();
        }

        public void Switch()
        {
            if (_inGameConsoleCanvas == null)
                return;
            _inGameConsoleCanvas.enabled = !_inGameConsoleCanvas.enabled;
        }
    }
}