﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Com.VisionMe.StackDefense
{
    public class AttachCanvasToMainCam : MonoBehaviour
    {
        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void Start()
        {
            ResetCam();
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            ResetCam();
        }

        private void ResetCam()
        {
            var cam = Camera.main;
            var canvas = GetComponent<Canvas>();
            canvas.worldCamera = cam;
            canvas.planeDistance = 1f;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}