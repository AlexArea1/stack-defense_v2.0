﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    [RequireComponent(typeof(TMP_Text))]
    public class DisplayVR_Hardware : MonoBehaviour
    {
        private TMP_Text _tmpText;

        private VRInputType _vrInputType;

        private void Awake()
        {
            _tmpText = GetComponent<TMP_Text>();
        }

        private void Start()
        {
            var localTemporarySavedData = FindObjectOfType<LocalTemporarySavedData>();
            if (localTemporarySavedData == null)
            {
                Debug.LogWarning($"no LocalTemporarySavedData instance found");
                _vrInputType = VRInputType.UNKNOWN;
            }
            else
            {
                _vrInputType = localTemporarySavedData.vrHeadsetType;
            }

            _tmpText.text = _vrInputType.ToString();
        }
    }
}