﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Displays one tower of one player
/// </summary>
public class DisplayPointsTower : MonoBehaviour
{
    [Header("Settings")]

    [Tooltip("PlayerNumber of the player that should be tracked")]
    public int playerNumber;

    [Tooltip("The height offset a new block should spawn with")]
    public float offsetSpawnY;

    [Tooltip("Random offset in x direction")]
    public float offsetSpawnX;

    [Tooltip("Current amount of points of this tower")]
    public int points = 0;

    [Tooltip("UI text displaying the points")]
    public GameObject text;

    [Tooltip("Parent object for all stacking objects")]
    public GameObject stackingObjectsParent;

    [Tooltip("Prefab of a UI stacking object")]
    public GameObject[] prefabsStackingObject;

    private RectTransform rt;

    private GameManager gameManager;

    public TextMeshProUGUI texttest;


    public void Awake()
    {
        texttest = GameObject.Find("textObj").GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        rt = GetComponent<RectTransform>();
    }

    public void Update()
    {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();

        // Check if points have been changed
        if (gameManager.points[playerNumber - 1] != points)
        {
            if(gameManager.points[playerNumber - 1] < points)
            {
                RemovePoint();
            }
            else
            {
                AddPoint();
            }
        }

        if(text != null)
        {
            text.GetComponent<Text>().text = points.ToString("D0");
        }

        texttest.text = points.ToString("D0");
    }

    /// <summary>
    /// Adds a point to the stack
    /// </summary>
    private void AddPoint()
    {        
        points += 1;

        // Calculate height with a offset to the highest block
        GameObject block = Instantiate(prefabsStackingObject[Random.Range(0, prefabsStackingObject.Length - 1)], Vector3.zero, Quaternion.identity);

        RectTransform blockRT = block.GetComponent<RectTransform>();
        Canvas canvas = GetComponentInParent<Canvas>();

        Vector3 pos = new Vector3(rt.position.x + Random.Range(-offsetSpawnX, offsetSpawnX), rt.position.y + offsetSpawnY, 0);
        block.GetComponent<RectTransform>().anchoredPosition = pos;

        float scaleX = Screen.width / 1600.0f;
        float scaleY = Screen.height / 900.0f;

        block.GetComponent<RectTransform>().localScale *= canvas.scaleFactor;

        blockRT.SetParent(stackingObjectsParent.transform, true);
        //block.transform.position = pos;
        block.GetComponent<Image>().enabled = true;
    }

    /// <summary>
    /// Removes a points from the stack
    /// </summary>
    private void RemovePoint()
    {
        points -= 1;

        Destroy(stackingObjectsParent.transform.GetChild(Random.Range(0, stackingObjectsParent.transform.childCount - 1)).gameObject);
    }
}
