﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.VisionMe.StackDefense
{
    /// <summary>
    /// Synchronizes the table's point displays with each player's points
    /// </summary>
    public class Display : Photon.PunBehaviour
    {
        #region Public Variables

        [Tooltip("True if the current timer should be displayed. Otherwise a player's points will be displayed")]
        public bool showTimer;

        [Tooltip("True if the status should be shown")]
        public bool showStatus = true;

        [Tooltip("The playerNumber of the player whos points should be displayed")]
        public int playerNumber;

        #endregion

        #region Private Variables
        private GameManager gameManager;
        #endregion

        #region MonoBehaviour Callbacks

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {

        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            if (!gameManager)
                gameManager = FindObjectOfType<GameManager>();

            if (!gameManager)
                return;

            // Display the timer
            if(showTimer)
            {
                if(gameManager.state == GameState.COUNTDOWN_END || gameManager.state == GameState.FINISHED)
                {
                    transform.GetChild(0).GetComponent<Text>().text = "0";
                }
                else
                {
                    transform.GetChild(0).GetComponent<Text>().text = gameManager.currentTime.ToString("F0");
                }
            }
            else
            {
                int[] points = gameManager.points;

                transform.GetChild(0).GetComponent<Text>().text = points[playerNumber - 1].ToString();
            }

            // Display the status
            if(showStatus)
            {
                if (gameManager.state == GameState.IDLE || gameManager.state == GameState.FINISHED)
                {
                    if(gameManager.players[playerNumber - 1] != null)
                    {
                        if (gameManager.state == GameState.IDLE && gameManager.players[playerNumber - 1].GetComponent<Player>().state == PlayerState.START)
                        {
                            // Display ready to start game
                            SetIsReady(true);
                        }
                        else if (gameManager.state == GameState.FINISHED && gameManager.players[playerNumber - 1].GetComponent<Player>().state == PlayerState.RESET)
                        {
                            // Display ready to reset game
                            SetIsReady(true);
                        }
                        else
                        {
                            SetIsReady(false);
                        }
                    }
                    else
                    {
                        SetIsReady(false);
                    }
                    
                }
                else
                {
                    DeactivateStatus();
                }
            }
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Disables the status display
        /// </summary>
        private void DeactivateStatus()
        {
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.SetActive(false);
        }

        /// <summary>
        /// Sets the status display
        /// </summary>
        /// <param name="value"></param>
        private void SetIsReady(bool value)
        {
            transform.GetChild(1).gameObject.SetActive(value);
            transform.GetChild(2).gameObject.SetActive(!value);
        }

        #endregion

        #region RPCs

        #endregion
    }
}