﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    [RequireComponent(typeof(TMP_Text))]
    public class DisplayApplicationVersion : MonoBehaviour
    {
        private TMP_Text _tmpText;

        private void Awake()
        {
            _tmpText = GetComponent<TMP_Text>();
        }

        private void Start()
        {
            _tmpText.text = "v" + Application.version;
        }
    }
}