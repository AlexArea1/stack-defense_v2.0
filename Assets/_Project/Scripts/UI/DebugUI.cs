﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour
{
    public Text[] hashtable;

    public Text[] localtable;

    public Text localplayer;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateHashtable", 1.0f, 1.0f);
        InvokeRepeating("UpdateLocaltable", 1.0f, 1.0f);
        InvokeRepeating("UpdateLocalplayer", 1.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();
    }

    void UpdateLocaltable ()
    {
        // Update local table
        for (int i = 0; i < 4; i++)
        {
            if(gameManager.players[i] != null)
            {
                localtable[i].text = "Player " + gameManager.players[i].GetComponent<Player>().playerNumber;
            }
            else
            {
                localtable[i].text = "-";
            }
            
        }
    }

    void UpdateHashtable()
    {
        // Update hash table
        ExitGames.Client.Photon.Hashtable players = PhotonNetwork.room.CustomProperties;

        //Debug.Log(players.ToStringFull());

        for (int i = 1; i < 5; i++)
        {
            string key = "Player_" + i;

            if (players.ContainsKey(key))
            {
                int value = (int)players[key];
                hashtable[i - 1].text = "Player: " + i + " | ID: " + value;
            }
            else
            {
                //Debug.Log(key + " is not in table");
                hashtable[i - 1].text = "Player: " + i + " | ID: -";
            }
        }
    }

    void UpdateLocalplayer()
    {
        string host = "no";

        if(PhotonNetwork.isMasterClient)
        {
            host = "yes";
        }

        localplayer.text = "ID: " + PhotonNetwork.player.ID + "      |      Number: " + gameManager.playerNumber + "      |      Host: " + host;
    }
}
