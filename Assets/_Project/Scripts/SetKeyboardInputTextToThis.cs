﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRKeyboard.Utils;
using TMPro;

public class SetKeyboardInputTextToThis : MonoBehaviour
{
    [SerializeField] private KeyboardManager keyboardManager = null;
    private TMP_InputField thisInputField;

    void Awake()
    {
        thisInputField = GetComponent<TMP_InputField>();
    }

    public void SetInputText()
    {
        if (!GetKeyboard())
            return;

        keyboardManager.inputText = thisInputField;
    }

    public void ActivateKeyboard()
    {
        if (!GetKeyboard())
            return;

        keyboardManager.gameObject.SetActive(true);
    }

    private bool GetKeyboard()
    {
        if (!keyboardManager)
            keyboardManager = FindObjectOfType<KeyboardManager>();

        if (!keyboardManager)
        {
            Debug.Log("No VR Keyboard found");
            return false;
        }

        return true;
    }
}
