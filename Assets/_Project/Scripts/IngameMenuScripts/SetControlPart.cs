﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.XR;
using Com.VisionMe.VR;
using System;

public class SetControlPart : MonoBehaviour
{
    private VR_Input vrInput;

    [SerializeField] private TextMeshProUGUI mechanicNameTxt;
    [SerializeField] private TMP_Dropdown dropdownBox;

    private Dictionary<string, int> inputNamesNumDic;
    private Dictionary<int, string> inputNumNamesDic;

    public void InitailiseSetControlPart(string _functionKey, InputFeatureUsage<bool> _inputFeature)
    {
        vrInput = FindObjectOfType<VR_Input>();

        dropdownBox = GetComponentInChildren<TMP_Dropdown>();
        dropdownBox.ClearOptions();

        inputNamesNumDic = new Dictionary<string, int>();
        inputNumNamesDic = new Dictionary<int, string>();
        for (int i = 0; i < VR_Input.possibleControls.Count; i++)
        {
            inputNamesNumDic.Add(VR_Input.possibleControls[i].ownUsage.ToString(), i);
            inputNumNamesDic.Add(i, VR_Input.possibleControls[i].ownUsage.ToString());
        }

        List<string> inputNamesList = new List<string>(inputNamesNumDic.Keys);
        dropdownBox.AddOptions(inputNamesList);


        mechanicNameTxt.text = _functionKey;

        for (int i = 0; i < VR_Input.possibleControls.Count; i++)
        {
            if(_inputFeature == VR_Input.possibleControls[i].xrUsage)
            {
                dropdownBox.value = inputNamesNumDic[VR_Input.possibleControls[i].ownUsage.ToString()];
            }
        }
    }

    public void DoropdownValueChanged()
    {
        for (int i = 0; i < VR_Input.possibleControls.Count; i++)
        {
            if(inputNumNamesDic[dropdownBox.value] == VR_Input.possibleControls[i].ownUsage.ToString())
            {
                vrInput.events[mechanicNameTxt.text].inputFeatureUsage = VR_Input.possibleControls[i].xrUsage;
            }
        }
    }
}
