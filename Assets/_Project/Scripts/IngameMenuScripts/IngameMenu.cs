﻿using Com.VisionMe.StackDefense;
using Com.VisionMe.VR;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class IngameMenu : MonoBehaviour
{
    private Canvas canvas;
    private VR_Input vr_Input;
    private AudioSource audioSource;
    private GameManager gameManager;

    [Header("Panels")]
    [SerializeField] private GameObject closeBtn;
    [SerializeField] private GameObject ingameMenuPanel;
    [SerializeField] private GameObject controlSettingsPanel;

    [Header("More Objects")]
    [SerializeField] private Slider musicSlider;

    [Header("Variables")]
    [SerializeField] private float distanceToFace = 5;

    private bool isMenuOpen = false;
    public string actualPanel { get; private set; } = "";

    private void Awake()
    {
        canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;

        vr_Input = FindObjectOfType<VR_Input>();
        audioSource = GameObject.Find("Audio Source").GetComponent<AudioSource>();
        gameManager = FindObjectOfType<GameManager>();

        GoToPanel("Controls");
    }

    private void Start()
    {
        EventAndInput eai = new EventAndInput();
        eai.functionEvent = new UnityEngine.Events.UnityEvent();
        eai.functionEvent.AddListener(ToggleMenu);
        eai.inputFeatureUsage = CommonUsages.gripButton;
        eai.leftHand = true;
        eai.getType = Com.VisionMe.VR.GetType.GETDOWN;

        vr_Input.AddEvent("ToggleIngameMenu", eai);

        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            float volume = PlayerPrefs.GetFloat("MusicVolume");
            musicSlider.value = volume;
            audioSource.volume = volume;
        }
        else
            musicSlider.value = audioSource.volume;

        if (isMenuOpen)
            GoToPanel("Main");
        else
            GoToPanel("None");
    }

    private void Update()
    {
        Vector3 dir = transform.position - Camera.main.transform.position;
        if(dir.magnitude > 0)
            transform.forward = dir;
    }

    //Open and close the menu
    public void ToggleMenu()
    {
        isMenuOpen = !isMenuOpen;

        if (isMenuOpen)
        {
            GoToPanel("Main");
        }
        else
            GoToPanel("None");
    }

    public void ToggleMusic()
    {
        audioSource.volume = musicSlider.value;

        PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);
    }

    //Go to the lobby (player/spectator buttons)
    public void ToLobby()
    {
        gameManager.ConnectToLobby();
    }

    //Disconnect from a game
    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Launcher_VR");
    }

    //Open a panel
    public void GoToPanel(string _panel)
    {
        bool change = true;
        switch (_panel)
        {
            //Close the ingame menu
            case "None":
                closeBtn.SetActive(false);
                ingameMenuPanel.SetActive(false);
                controlSettingsPanel.SetActive(false);
                break;
            //Open the Main Panel
            case "Main":
                closeBtn.SetActive(true);
                ingameMenuPanel.SetActive(true);
                controlSettingsPanel.SetActive(false);
                break;
            //Open the controls Panel
            case "Controls":
                closeBtn.SetActive(true);
                ingameMenuPanel.SetActive(false);
                controlSettingsPanel.SetActive(true);
                break;
            //Unknown Panel name
            default:
                change = false;
                Debug.LogError("Unknown Panel name");
                break;
        }

        //Save the controls when the controls panel is closed
        if (change)
        {
            if (actualPanel == "Controls")
                vr_Input.SaveControlsToJson();

            actualPanel = _panel;
        }
    }
}
