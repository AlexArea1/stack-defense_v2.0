﻿using Com.VisionMe.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ControlsMenu : MonoBehaviour
{
    private VR_Input vrInput;
    private IngameMenu ingameMenu;

    [SerializeField] private GameObject controlsPanel;
    [SerializeField] private GameObject scrollContainer;
    [SerializeField] private GameObject setControlPartPrefab;

    private List<SetControlPart> setControlParts;

    private bool awoke = false;

    private void Awake()
    {
        awoke = true;

        vrInput = FindObjectOfType<VR_Input>();
        ingameMenu = GetComponent<IngameMenu>();

        setControlParts = new List<SetControlPart>();
    }

    public void UpdateLayout()
    {
        Awake();
        //string lastPanel = ingameMenu.actualPanel;
        //ingameMenu.GoToPanel("Controls");

        while(setControlParts.Count > 0)
        {
            Destroy(setControlParts[0].gameObject);
            setControlParts.RemoveAt(0);
        }

        foreach (var item in vrInput.events)
        {
            SetControlPart nPart = Instantiate(setControlPartPrefab, scrollContainer.transform).GetComponent<SetControlPart>();
            nPart.InitailiseSetControlPart(item.Key, item.Value.inputFeatureUsage);
            setControlParts.Add(nPart);

            RectTransform rectTransform = nPart.GetComponent<RectTransform>();
            
            Vector3 pos = rectTransform.localPosition;
            pos.y = (setControlParts.Count - 1) * -15 - 2;
            rectTransform.localPosition = pos;
        }

        //ingameMenu.GoToPanel(lastPanel);
    }
}
