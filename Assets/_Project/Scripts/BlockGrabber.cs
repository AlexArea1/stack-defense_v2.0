﻿using Com.VisionMe.StackDefense;
using Com.VisionMe.VR;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.XR;

public class BlockGrabber : MonoBehaviour
{
    private VR_Input vr_Input;

    private List<VR_InteractableObject> stackingObjectsInReach;
    private VR_InteractableObject grabbedObject = null;
    GameManager gameManager;

    private IPlayerNumberProvider _playerNumberProvider;
    public int playerNumberPlayer = 0;

    private void Awake()
    {
        _playerNumberProvider = GetComponentInParent<IPlayerNumberProvider>();
        playerNumberPlayer = _playerNumberProvider.PlayerNumber;
       // gameManager.playerNumber != playerNumberPlayer;
        gameManager = FindObjectOfType<GameManager>();

        if (gameManager.playerNumber == playerNumberPlayer)
        { 
            vr_Input = FindObjectOfType<VR_Input>();
            stackingObjectsInReach = new List<VR_InteractableObject>();
            //Adding the GrabBlock Event to the Input
            EventAndInput eai = new EventAndInput();
            eai.functionEvent = new UnityEngine.Events.UnityEvent();
            eai.functionEvent.AddListener(GrabBlock);
            eai.inputFeatureUsage = CommonUsages.triggerButton;
            eai.leftHand = false;
            eai.getType = Com.VisionMe.VR.GetType.GETDOWN;

            vr_Input.AddEvent("GrabBlock", eai);

            //Adding the RealeaseBlock Event to the Input
            eai = new EventAndInput();
            eai.functionEvent = new UnityEngine.Events.UnityEvent();
            eai.functionEvent.AddListener(ReleaseBlock);
            eai.inputFeatureUsage = CommonUsages.triggerButton;
            eai.leftHand = false;
            eai.getType = Com.VisionMe.VR.GetType.GETUP;

            vr_Input.AddEvent("ReleaseBlock", eai);
        }

    }

    public void GrabBlock()
    {
        if (grabbedObject)
            return;
        if (gameManager.state != GameState.RUNNING)
            return;

        float smallestDistance = 1000;
        int smallestIndex = 0;

        for (int i = stackingObjectsInReach.Count - 1; i >= 0; i--)
        {
            if(stackingObjectsInReach[i] == null)
            {
                stackingObjectsInReach.RemoveAt(i);
                
                if (smallestIndex == i)
                    break;
                if (smallestIndex > i)
                    smallestIndex--;

                continue;
            }

            float nDistance = (stackingObjectsInReach[i].transform.position - transform.position).magnitude;
            if (nDistance < smallestDistance)
            {
                smallestDistance = nDistance;
                smallestIndex = i;
            }
        }

        if (smallestDistance < 1000)
        {
            grabbedObject = stackingObjectsInReach[smallestIndex];
            grabbedObject.Grab(transform);
        }
    }

    public void ReleaseBlock()
    {
        if (!grabbedObject)
            return;

        grabbedObject.Release();
        grabbedObject = null;
    }

    private void OnTriggerEnter(Collider _other)
    {
        VR_InteractableObject obj = _other.GetComponent<VR_InteractableObject>();

        if (obj)
        {
            if (!stackingObjectsInReach.Contains(obj))
            {
                stackingObjectsInReach.Add(obj);
            }
        }
    }

    private void OnTriggerExit(Collider _other)
    {
        VR_InteractableObject obj = _other.GetComponent<VR_InteractableObject>();

        if (obj)
        {
            if (stackingObjectsInReach.Contains(obj))
            {
                stackingObjectsInReach.Remove(obj);
            }
        }
    }

}
