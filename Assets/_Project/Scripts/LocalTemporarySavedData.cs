﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class LocalTemporarySavedData : MonoBehaviour
{
    public VRInputType vrHeadsetType = VRInputType.UNKNOWN;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        UpdateVRHeadset();
    }

    private void Update()
    {
        if (vrHeadsetType == VRInputType.UNKNOWN)
        {
            UpdateVRHeadset();
        }
    }

    private void UpdateVRHeadset()
    {
        if (XRDevice.model.Contains("Vive"))
        {
            vrHeadsetType = VRInputType.VIVE;
        }
        else if (XRDevice.model.Contains("Oculus"))
        {
            vrHeadsetType = VRInputType.OCULUS;
        }
        else
        {
            vrHeadsetType = VRInputType.UNKNOWN;
        }
    }
}