﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
// The current state of the game
public enum GameState {
    IDLE,
    COUNTDOWN_START,
    RUNNING,
    COUNTDOWN_END,
    FINISHED
}

public enum SpectatorCameraMode {
    FREE,
    FOCUSED
}

// The current state of each player
// START for starting game, RESET for resetting game, IDLE for everything else
public enum PlayerState {
    IDLE,
    START,
    RESET
}

public enum SoundEffect {
    SHOT,
    REFLECTION,
    HIT,
    READY,
    SET,
    GO,
    TEN,
    NINE,
    EIGHT,
    SEVEN,
    SIX,
    FIVE,
    FOUR,
    THREE,
    TWO,
    ONE,
    TIMEOVER,
    ENDTHREE,
    ENDTWO,
    ENDONE,
    ENDZERO,
    GAMEOVER,
    WIN,
    TIE
}

public enum SoundEffectSpeaker {
    FEMALE1 = 0,
    MALE1 = 1
}


public enum VRInputType {
    UNKNOWN,
    VIVE,
    OCULUS,
    QUEST
}
}