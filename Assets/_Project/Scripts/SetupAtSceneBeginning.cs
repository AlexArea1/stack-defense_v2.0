﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupAtSceneBeginning : MonoBehaviour
{
    LocalTemporarySavedData savedData = null;
    

    private void Start()
    {
        savedData = FindObjectOfType<LocalTemporarySavedData>();

        if (savedData)
        {
            if (savedData.vrHeadsetType == Com.VisionMe.StackDefense.VRInputType.OCULUS)
            {
                GameObject xrCameraOffset = GameObject.Find("Camera Offset");
                if (xrCameraOffset)
                {
                    xrCameraOffset.transform.position += new Vector3(0, 2, 0);
                }
            }
        }
    }
}
