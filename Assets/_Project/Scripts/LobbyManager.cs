﻿using Com.VisionMe.StackDefense;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Com.VisionMe.StackDefense
{
    public class LobbyManager : Photon.PunBehaviour
    {
        public GameObject[] JoinRoomButtons;

        public GameObject[] RoomNameTexts;

        public TMP_InputField RoomName;

        private string[] _textString;

        public string sceneToLoad = "Game";

        string _gameVersion = "1";

        public int serverNumbers;

        public int maxPlayersPerRoom;
        public int maxSpectator;

        public byte amountOfPlayers;

        [Header("Launcher Menu Objects")]
        public TMP_InputField playerNameTmp;
        [SerializeField] private TextMeshProUGUI maxPlayersTmp;
        [SerializeField] private TextMeshProUGUI maxSpectatorsTmp;
        [SerializeField] private TMP_InputField gameTimeTmp;
        [SerializeField] private Toggle shieldToggle;

        public string skyBox;



        private bool isShieldOn;
        private void Awake()
        {
            LoadLauncherSettingsFromPlayerPrefs();

        }

        void Start()
        {
            if (!PhotonNetwork.connected)
            {
                PhotonNetwork.autoJoinLobby = false;
                PhotonNetwork.automaticallySyncScene = true;

                PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
            var temp = PhotonVoiceNetwork.Client;


           // RoomName.text = "RoomName" + UnityEngine.Random.Range(1,9999).ToString();
        }

        public void checkFieldIfIsEmpty()
        {
            if (RoomName.text == "")
            {
                RoomName.text = "RoomName" + UnityEngine.Random.Range(1, 9999).ToString();
            }
        }

        public void CheckIfPlayerNameIsEmpty()
        {
            if (playerNameTmp.text == "")
            {
                playerNameTmp.text = "Player" + UnityEngine.Random.Range(1, 9999).ToString();
            }
        }

        private void Update()
        {
            //PlayerPrefs.SetString("RoomName", RoomName.text);
            //PlayerPrefs.SetString("PlayerName", playerNameTmp.text);
        }
        public override void OnConnectedToMaster()
        {
            print("Connected to master");
            PhotonNetwork.JoinLobby();

        }

        public override void OnJoinedLobby()
        {
            print("Connected to Lobby");
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Joined Room");

            if (PhotonNetwork.room.PlayerCount == 1)
            {
                Debug.Log("Loading scene: " + sceneToLoad);
                PhotonNetwork.LoadLevel(sceneToLoad);
            }
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            // Set PlayerName
            //TODO: Set and get one after another?
            PlayerPrefs.SetString("PlayerName", playerNameTmp.text);
            newPlayer.NickName = PlayerPrefs.GetString("PlayerName");

            print(newPlayer.NickName + "has joined");
            if (PhotonNetwork.isMasterClient && PhotonNetwork.room.PlayerCount >= 2)
            {
                PhotonNetwork.LoadLevel("Game");
            }
        }

        public void CreateRoom()
        {

            SaveLauncherSettingsInPlayerPrefs();

            PhotonNetwork.player.NickName = PlayerPrefs.GetString("PlayerName");
            maxPlayersPerRoom = Convert.ToInt32(PlayerPrefs.GetString("MaxPlayers"));
            maxSpectator = Convert.ToInt32(PlayerPrefs.GetString("MaxSpectators"));
            amountOfPlayers = Convert.ToByte(maxPlayersPerRoom + maxSpectator);

            RoomOptions stackDefRoomOptions = new RoomOptions();
            stackDefRoomOptions.IsVisible = true;
            stackDefRoomOptions.IsOpen = true;
            stackDefRoomOptions.MaxPlayers = amountOfPlayers;

            // RoomOption Stuffs

            // Shield Setting
            stackDefRoomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable(); 
            int checkSettingbool = PlayerPrefs.GetInt("ShielsIsOn");
            if (checkSettingbool == 1)
            {
                isShieldOn = true;
            }
            if (checkSettingbool == 0)
            {
                isShieldOn = false;
            }
            stackDefRoomOptions.CustomRoomProperties.Add("Shield", isShieldOn);

            // SkyBox Setting
            skyBox = PlayerPrefs.GetString("SkyBoxChoose");
            stackDefRoomOptions.CustomRoomProperties.Add("SkyBoxes", skyBox);

            PhotonNetwork.CreateRoom(RoomName.text, stackDefRoomOptions, TypedLobby.Default);

            print("Creating and Joining Room");
        }

        public void GetRoomList()
        {
            RoomInfo[] RoomList = PhotonNetwork.GetRoomList();

            if (RoomList.Length > 0)
            {
                for (int index = 0; index < RoomList.Length;
                    ++index)
                {
                    serverNumbers = index;
                    RoomInfo Room = RoomList[index];
                    GameObject Entry = JoinRoomButtons[index];

                    TextMeshProUGUI JoinText = Entry.GetComponentInChildren<TextMeshProUGUI>();

                    JoinText.text = Room.Name;// Random.Range(0, 1000).ToString();//RoomName.text; //"Server Created";//RoomName.text;

                    print(JoinText.text);

                }
                string pinging;
                pinging = PhotonNetwork.GetPing().ToString();

                for (int index = 0; index < RoomList.Length;
                    ++index)
                {
                    RoomInfo Room = RoomList[index];
                    GameObject Entry = RoomNameTexts[index];

                    TextMeshProUGUI MaxText = Entry.GetComponentInChildren<TextMeshProUGUI>();

                    MaxText.text = Room.PlayerCount.ToString() + "/" + Room.MaxPlayers;// Random.Range(0, 1000).ToString();//RoomName.text; //"Server Created";//RoomName.text;

                    print(MaxText.text);
                }
            }
        }
        public void Connect(TextMeshProUGUI EntryText)
        {
            // Set PlayerName
            PlayerPrefs.SetString("PlayerName", playerNameTmp.text);
            PlayerPrefs.Save();
            PhotonNetwork.player.NickName = PlayerPrefs.GetString("PlayerName");
            PhotonNetwork.JoinRoom(EntryText.text);
        }

        public void Exitbutton()
        {
            Application.Quit();
        }


        public void SaveLauncherSettingsInPlayerPrefs()
        {
            PlayerPrefs.SetString("PlayerName", playerNameTmp.text);
            PlayerPrefs.SetString("MaxPlayers", maxPlayersTmp.text);
            PlayerPrefs.SetString("MaxSpectators", maxSpectatorsTmp.text);
            PlayerPrefs.SetString("GameTime", gameTimeTmp.text);
            PlayerPrefs.SetInt("ShielsIsOn", (shieldToggle.isOn) ? 1 : 0);
        }

        public void LoadLauncherSettingsFromPlayerPrefs()
        {
            if (PlayerPrefs.HasKey("PlayerName"))
                playerNameTmp.text = PlayerPrefs.GetString("PlayerName");
            if (PlayerPrefs.HasKey("MaxPlayers"))
                maxPlayersTmp.text = PlayerPrefs.GetString("MaxPlayers");
            if (PlayerPrefs.HasKey("MaxSpectators"))
                maxSpectatorsTmp.text = PlayerPrefs.GetString("MaxSpectators");
            if (PlayerPrefs.HasKey("GameTime"))
                gameTimeTmp.text = PlayerPrefs.GetString("GameTime");
            if (PlayerPrefs.HasKey("ShieldIsOn"))
                shieldToggle.isOn = PlayerPrefs.GetInt("ShieldIsOn") == 1;
        }
    }

}
