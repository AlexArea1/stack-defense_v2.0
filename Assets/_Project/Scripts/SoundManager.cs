﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
public class SoundManager : Photon.PunBehaviour {
    [Tooltip("Sounds for shot, reflect, hitting, countdown, victory")]
    public AudioClip[] sounds;

    [SerializeField] private List<SoundEffectDefinition> alternativeSounds;

    [HideInInspector] public static SoundManager instance;

    private GameManager gameManager;

    void Awake() {
        instance = this;
    }

    /// <summary>
    /// Plays a sound on the network
    /// </summary>
    /// <param name="sound">Sound to be played</param>
    /// <param name="pos">If the sound should be played in stereo add it's position here</param>
    public void PlaySound(SoundEffect sound, Vector3 pos = default(Vector3)) {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();

        if (pos == Vector3.zero) {
            if (gameManager) {
                photonView.RPC("PlaySoundOnNetwork", PhotonTargets.All, sound);
                gameManager.lastSound = sound;
            }
        }
        else {
            photonView.RPC("PlayStereoSoundOnNetwork", PhotonTargets.All, sound, pos);
        }
    }

    /// <summary>
    /// Plays a sound for the given players
    /// </summary>
    /// <param name="sound">Sound to be played</param>
    /// <param name="players">PlayerNumbers that sound should be played for</param>
    /// <param name="pos">If the sound should be played in stereo add it's position here</param>
    public void PlaySoundForPlayers(SoundEffect sound, int[] players) {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();

        if (!gameManager)
            return;

        photonView.RPC("PlaySoundForPlayersOnNetwork", PhotonTargets.All, sound, players);
        gameManager.lastSound = sound;
    }

    /// <summary>
    /// Plays a sound
    /// </summary>
    /// <param name="sound"></param>
    [PunRPC]
    private void PlaySoundOnNetwork(SoundEffect sound) {
        GetComponent<AudioSource>().PlayOneShot(GetClip(sound));
    }

    /// <summary>
    /// Plays a sound at a position
    /// </summary>
    /// <param name="sound"></param>
    /// <param name="pos"></param>
    [PunRPC]
    private void PlayStereoSoundOnNetwork(SoundEffect sound, Vector3 pos) {
        AudioSource.PlayClipAtPoint(GetClip(sound), pos);
    }

    /// <summary>
    /// Plays a sound for some players
    /// </summary>
    /// <param name="sound"></param>
    [PunRPC]
    private void PlaySoundForPlayersOnNetwork(SoundEffect sound, int[] players) {
        if (!gameManager)
            gameManager = FindObjectOfType<GameManager>();

        if (!gameManager)
            return;

        if (players.Contains(gameManager.playerNumber)) {
            GetComponent<AudioSource>().PlayOneShot(GetClip(sound));
        }
    }

    private AudioClip GetClip(SoundEffect soundEffect) {
        var alternativeClip = alternativeSounds
            .FirstOrDefault(s => s.SoundEffect == soundEffect && s.Speaker == SoundEffectSpeaker)?.AudioClip;
        if (alternativeClip != null)
            return alternativeClip;

        return sounds[(int) soundEffect];
    }

    private static SoundEffectSpeaker? _soundEffectSpeaker = null;

    public static SoundEffectSpeaker SoundEffectSpeaker {
        get {
            if (_soundEffectSpeaker == null)
                _soundEffectSpeaker = (SoundEffectSpeaker) PlayerPrefs.GetInt(SOUND_EFFECT_SPEAKER_PLAYERPREFS_KEY, 0);
            return (SoundEffectSpeaker) _soundEffectSpeaker;
        }
        set {
            _soundEffectSpeaker = value;
            PlayerPrefs.SetInt(SOUND_EFFECT_SPEAKER_PLAYERPREFS_KEY, (int) value);
        }
    }

    private static string SOUND_EFFECT_SPEAKER_PLAYERPREFS_KEY = "SOUND_SETTING";
}
}