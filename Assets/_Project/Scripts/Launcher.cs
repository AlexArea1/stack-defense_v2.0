﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Com.VisionMe.StackDefense
{
    /// <summary>
    /// The launcher is used to launch a new game or to connect to an existing one
    /// </summary>
    public class Launcher : Photon.PunBehaviour
    {
        #region Public Variables

        [Tooltip("Connect automatically when starting the game")]
        public bool connectOnStart;

        [Tooltip("The detail of PUN logs")]
        public PhotonLogLevel logLevel = PhotonLogLevel.Informational;

        [Tooltip("The maximal amount of players per room")]
        public byte maxPlayersPerRoom = 4;

        [Tooltip("The name of the scene that should be loaded when joining a room")]
        public string sceneToLoad = "Game";

        [Tooltip("The UI Panel for login")]
        public GameObject controlPanel;

        [Tooltip("The UI Panel for displaying the connection progress")]
        public GameObject progressLabel;

        [Tooltip("UI for the Roomname")]
        public TMP_InputField RoomName;

        public TMP_InputField ipadress;

        public TMP_InputField portadress;

        #endregion

        #region Private Variables

        //The client's version number
        string _gameVersion = "1";

        //True if the user has started to connection progress
        bool isConnecting;

        private bool isCreating = false;

        #endregion

        #region MonoBehaviour CallBacks

        private void Awake()
        {
            PhotonNetwork.autoJoinLobby = false;
            PhotonNetwork.automaticallySyncScene = true;
        }

        private void Start()
        {

        }


        #endregion

        #region Public Methods

        /// <summary>
        /// Start the connection process.
        ///  - If already connected, we attempt joining a random room
        ///  - If not yet connected, connect this application instance
        /// </summary>
        /// 
        public void Connect()
        {
            isConnecting = true;

            progressLabel.SetActive(true);
            controlPanel.SetActive(false);

            if (PhotonNetwork.connected)
            {
                //PhotonNetwork.ConnectToMaster(ipadress.text, int.Parse(portadress.text), "cded48f4-2ea3-47d3-be6d-4d7c3ebfeabf", PhotonNetwork.gameVersion);
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
        }

        public void JoinRandomConnect()
        {
            isConnecting = true;

            progressLabel.SetActive(true);
            controlPanel.SetActive(false);

            if (PhotonNetwork.connected)
            {
                PhotonNetwork.JoinRandomRoom();
                
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
        }


        public void CreateConnect()
        {
            isConnecting = true;

            isCreating = true;

            progressLabel.SetActive(true);
            controlPanel.SetActive(false);

            PhotonNetwork.ConnectUsingSettings(_gameVersion);

            if (!PhotonNetwork.connected)
            {
                //PhotonNetwork.ConnectToMaster(ipadress.text, int.Parse(portadress.text), "cded48f4-2ea3-47d3-be6d-4d7c3ebfeabf", PhotonNetwork.gameVersion);
                //PhotonNetwork.CreateRoom(RoomName.text);
                //PhotonNetwork.JoinRoom(RoomName.text);
            }
            else
            {
                //PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
        }

        #endregion

        #region Photon.PunBehaviour CallBacks

        public override void OnConnectedToMaster()
        {
            Debug.Log("Tutorial/Launcher: OnConnectedToMaster() was called by PUN");

            //if(isConnecting)
            //{
            //    PhotonNetwork.JoinRandomRoom();
            //}

            if (isCreating)
            {
                PhotonNetwork.CreateRoom(RoomName.text);
                //PhotonNetwork.JoinRoom(RoomName.text);
            }
            else
            {
                PhotonNetwork.JoinRoom(ipadress.text);
            }
        }

        public override void OnDisconnectedFromPhoton()
        {
            Debug.Log("Tutorial/Launcher: OnDisconnectedFromPhoton() was called by PUN");

            progressLabel.SetActive(false);
            controlPanel.SetActive(true);
        }

        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
            Debug.Log("Tutorial/Launcher: OnPhotonRandomJoinFailed() was called by PUN.");

            return;

            bool check = PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = maxPlayersPerRoom }, null); //LAUNCHER FÜR STING ROOMNAME!!
            Debug.Log("The server is " + check /*PhotonNetwork.CreateRoom(RoomName.text.ToString(), new RoomOptions() { MaxPlayers = maxPlayersPerRoom }, null)*/);
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Tutorial/Launcher: OnJoinedRoom() called by PUN.");

            if(PhotonNetwork.room.PlayerCount == 1)
            {
                Debug.Log("Loading scene: " + sceneToLoad);
                PhotonNetwork.LoadLevel(sceneToLoad);
            }
        }

        #endregion
    }
}