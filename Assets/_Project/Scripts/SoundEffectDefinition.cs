﻿using System;
using UnityEngine;

namespace Com.VisionMe.StackDefense {
[Serializable]
public class SoundEffectDefinition {
    public SoundEffectSpeaker Speaker;
    public SoundEffect SoundEffect;
    public AudioClip AudioClip;
}
}