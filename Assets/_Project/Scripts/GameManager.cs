﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Com.VisionMe.VR;
using ExitGames.Client.Photon;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace Com.VisionMe.StackDefense
{
    /// <summary>
    /// This class manages the general game flow:
    ///     - it keeps track of the current game phases
    ///     - it keeps track of the current players connected to the game by organizing the hashtable which pairs each playerNumber to a playerID
    ///     - it stores some important variables like the lokal player or other connected players and makes them globally accessable using GameManager.instance
    /// </summary>
    public class GameManager : Photon.PunBehaviour, IPunObservable
    {
        #region Public Variables

        [Header("Game settings")]

        [Tooltip("The length of the game in seconds")]
        public int gameTime;

        [Tooltip("The length of the countdown in seconds")]
        public int countdownTime = 3;

        [Tooltip("The amount of time in seconds before the game starts automatically")]
        public int startAutomaticallyTime = 120;

        [Tooltip("A game can start automatically after a given time limit without all players being ready")]
        public bool startsAutomatically = false;

        [Tooltip("Time it takes to spawn another cube for all players")]
        public float spawnRate;

        [Tooltip("Shield set On or Off")]
        public bool ShieldOnOff;

        [Space(10)]
        [Header("Game information")]

        [Tooltip("Indicates the current state of the game")]
        public GameState state = GameState.IDLE;

        [Tooltip("Amount of currently connected players")]
        public int amountOfPlayers;

        public int maxSpec;

        [Tooltip("All currently connected player setups")]
        public GameObject[] players;

        [Tooltip("The points of each player")]
        public int[] points;


        [Header("Local player information")]

        [Tooltip("The number of this player")]
        public int playerNumber = 0;

        [Tooltip("True if this client is a spectator. Otherwise he is a player")]
        public bool isSpectator = true;

        [Tooltip("True if the client is connected with a desktop version. Otherwise it's a VR version")]
        public bool isDesktopClient = false;

        [Tooltip("This player's setup")]
        public GameObject playerSetup;


        [Header("Debug settings")]
        [Space(10)]

        [Tooltip("Used for debugging the problem with the bullets lag when transfering ownership")]
        public bool settingTransferOwnershipNewOwnerInterpolation = false;

        [Tooltip("Used for debugging the problem with the bullets lag when transfering ownership")]
        public bool settingTransferOwnershipOldOwnerInterpolation = false;

        [Tooltip("Used for debugging the problem with the bullets lag when transfering ownership")]
        public float settingTransOwnershipNewOwnerMultiplier = 1.0f;


        [Header("Prefabs")]
        [Space(10)]

        [Tooltip("The prefab to use for representing the player")]
        public GameObject prefabPlayerSetup;

        [Tooltip("The prefab to instantiate for the winner of the game")]
        public GameObject[] prefabPrizes;

        [Tooltip("The prefab to instantiate when the client connects with a desktop")]
        public GameObject prefabSpectator;


        [Header("Arenas")]
        [Space(10)]

        [Tooltip("The different arenas (disposal of positions for table, spawner...) for every amount of players")]
        public GameObject[] arenas;

        /// <summary>
        /// The current timer is used to time the countdowns and game time
        /// </summary>
        [HideInInspector]
        public float currentTime = 0;

        /// <summary>
        /// Instance of this game manager
        /// </summary>
        //static public GameManager instance;



        /// <summary>
        /// All prize objects that have been instantiated so they can be deleted again when resetting the game
        /// </summary>
        private List<GameObject> prizes;

        public List<GameObject> leadingStatuses;

        /// <summary>
        /// True when vaiables and gameObjects have been synced
        /// </summary>
        [HideInInspector]
        public bool isConnected = false;

        /// <summary>
        /// True when the variables have been synced
        /// </summary>
        [HideInInspector]
        public bool variablesSynced = false;

        /// <summary>
        /// True when the gameObjects have been synced
        /// </summary>
        [HideInInspector]
        public bool gameObjectsSynced = false;

        #endregion

        #region Private Variables

        /// <summary>
        /// VR camera rig
        /// </summary>
        private GameObject cameraRig;

        /// <summary>
        /// True if the camera rig has been moved already
        /// </summary>
        private bool cameraRigHasBeenMoved = false;

        /// <summary>
        /// Time the state of all player's has been checked the last time
        /// </summary>
        private float lastCheckedAllPlayersState;

        /// <summary>
        /// Time when cubes spawned the last time
        /// </summary>
        private float lastSpawnTime;

        /// <summary>
        /// Sound that has been played last. This is used to avoid sound looping
        /// </summary>
        public SoundEffect lastSound = 0;

        private SoundManager soundManager;

        private Vector3 xrRigBeginPos;


        /// <summary>
        /// To change the Camera and the position
        /// IsOculusQuest to check if you play as OculusQuest
        /// </summary>
        public GameObject CamUi;
        public GameObject EventSystem;
        public GameObject UICanvMouse;
        public GameObject LocalAvatar;
        public bool IsOculusQuest;

        public GameObject VrChoosePlayer;

        public GameObject VrOculusRoomCamDesktop;
        public GameObject CanvasDesktop;
        bool isplayer = false;

        public GameObject Shield;


        public List<GameObject> ListDesktopMenu = new List<GameObject>();

        public int counters = 1;


        [Header("Skyboxes")]
        // To Choose The Skyboxses 
        public string chooseSkyBox;
        public Material cloudSun;
        public Material blueSun;
        public Material fantasy1;
        public Material fantasy2;
        public Material anotherPlanet;
        public Material nebula;
        public Material skyOnFire;
        public Material space;
        public Material goldenSun;
        public Material gloriousPink;

        [Header("PlayerStuff")]
        public TextMeshProUGUI player1;
        public TextMeshProUGUI player2;
        public TextMeshProUGUI player3;
        public TextMeshProUGUI player4;

        public GameObject spec1;
        public GameObject spec2;
        public GameObject spec3;
        public GameObject spec4;


        public Text pointsPlayer1;
        public Text pointsPlayer2;
        public Text pointsPlayer3;
        public Text pointsPlayer4;


        [Header("Ping")]
        public GameObject pingTextPlayer1;
        public GameObject pingTextPlayer2;
        public GameObject pingTextPlayer3;
        public GameObject pingTextPlayer4;

        public TextMeshProUGUI pingTextspec1;
        public TextMeshProUGUI pingTextspec2;
        public TextMeshProUGUI pingTextspec3;
        public TextMeshProUGUI pingTextspec4;


        public string[] namesPlayer;
        //public string[] namesSpectator;
        public int[] ping;

        [Header("Messages")]
        [SerializeField] private GameObject gameIsRunningMessageObject;

        [Header("CheckIfPlayerExist")]
        public Player1Exist player1OnField;
        public Player2Exist player2OnField;
        public Player3Exist player3OnField;
        public Player4Exist player4OnField;


        [Header("CheckIfPlayerOrSpec")]
        public bool player1connected;
        public bool player2connected;
        public bool player3connected;
        public bool player4connected;



        [SerializeField] private VR_Input vr_Input;

        public event Action<int> onPlayerNumberUpdate;
        [SerializeField] private ScriptableGameStats scriptableGameStats;


        #endregion

        #region MonoBehaviour Callbacks

        public void Awake()
        {

            amountOfPlayers = Convert.ToInt32(PlayerPrefs.GetString("MaxPlayers"));
            foreach (GameObject item in ListDesktopMenu)
            {
                item.SetActive(false);
            }
            VrOculusRoomCamDesktop.SetActive(false);
            VrChoosePlayer.SetActive(true);
            CanvasDesktop.SetActive(false);
            xrRigBeginPos = LocalAvatar.transform.position;


        }

        public void Start()
        {
            PlayerPing();
            if (!vr_Input)
                vr_Input = GetComponent<VR_Input>();

            if (!vr_Input)
                Debug.LogError("Could not find vr_Input");

            EventAndInput eai = new EventAndInput();
            eai.functionEvent = new UnityEngine.Events.UnityEvent();
            eai.functionEvent.AddListener(SetPlayerReady);
            eai.inputFeatureUsage = CommonUsages.primaryButton;
            eai.leftHand = false;
            eai.getType = Com.VisionMe.VR.GetType.GETDOWN;

            vr_Input.AddEvent("SetPlayerReady", eai);
        }

        public void Connect()
        {
            SetSettingsFromLauncher();
            soundManager = SoundManager.instance;
            prizes = new List<GameObject>();
            leadingStatuses = new List<GameObject>();

            // Recieve the setting whether the client is player or spectator from the launcher scene
            if (PlayerPrefs.HasKey("isPlayer"))
            {
                if (PlayerPrefs.GetInt("isPlayer") == 1)//"isSpectator"
                {
                    isSpectator = false;
                }
                else
                {
                    isSpectator = true;
                }
            }

            if (PlayerPrefs.HasKey("isSpectator"))
            {
                if (PlayerPrefs.GetInt("isSpectator") == 1)//"isSpectator"
                {
                    isSpectator = true;
                }
                else
                {
                    isSpectator = false;
                }
            }

            //if (PlayerPrefs.HasKey("isDesktopClient"))
            //{
            //    if (PlayerPrefs.GetInt("isDesktopClient") == 1) //"isDesktopClient"
            //    {
            //        isDesktopClient = true;
            //    }
            //    else
            //    {
            //        isDesktopClient = false;
            //    }
            //}

            // cameraRig = GameObject.Find("XR Rig");  //("LocalAvatarWithGrab");

            currentTime = startAutomaticallyTime;
        }

        /// <summary>
        /// Connect as a player
        /// </summary>
        public void ConnectAsPlayer()
        {

            if (state != GameState.IDLE)
            {
                StartCoroutine(ShowMessageForTime(gameIsRunningMessageObject, 3));
                return;
            }

            MoveCameraRig();
            // CamUi.SetActive(false);

            EventSystem.SetActive(false);
            UICanvMouse.SetActive(false);
            // VrChoosePlayer.SetActive(false);
            LocalAvatar.SetActive(true);
            PlayerPrefs.SetInt("isPlayer", 1);
            PlayerPrefs.SetInt("isSpectator", 0);

            foreach (var line in FindObjectsOfType<XRInteractorLineVisual>())
            {
                line.lineLength = 0;
            }

            Connect();
            JoinAsPlayer();

            for (int i = 0; i < namesPlayer.Length; i++)
            {
                if (!string.IsNullOrEmpty(namesPlayer[0]))
                {
                    namesPlayer[0] = player1OnField.playerSetupPlayer1.GetPhotonView().owner.NickName;
                }
                if (!string.IsNullOrEmpty(namesPlayer[1]))
                {
                    namesPlayer[1] = player2OnField.playerSetupPlayer2.GetPhotonView().owner.NickName;
                }
                if (!string.IsNullOrEmpty(namesPlayer[2]))
                {
                    namesPlayer[2] = player3OnField.playerSetupPlayer3.GetPhotonView().owner.NickName;
                }
                if (!string.IsNullOrEmpty(namesPlayer[3]))
                {
                    namesPlayer[3] = player4OnField.playerSetupPlayer4.GetPhotonView().owner.ToString();
                }
            }

        }



        /// <summary>
        /// Connect as a spectator
        /// </summary>
        public void ConnectAsSpectator()
        {
            SetSettingsFromLauncher();
            //  CamUi.SetActive(false);
            EventSystem.SetActive(false);
            UICanvMouse.SetActive(false);
            //VrChoosePlayer.SetActive(false);
            LocalAvatar.SetActive(true);
            PlayerPrefs.SetInt("isPlayer", 0);
            PlayerPrefs.SetInt("isSpectator", 1);
            //Connect();
            JoinAsSpectator();
        }

        public void ConnectToLobby()
        {
            PlayerPrefs.SetInt("isPlayer", 0);
            PlayerPrefs.SetInt("isSpectator", 0);

            LocalAvatar.transform.position = xrRigBeginPos;
            LocalAvatar.transform.eulerAngles = new Vector3(0, 0, 0);

            if (playerSetup)
            {
                PhotonNetwork.Destroy(playerSetup.GetPhotonView());
                DeletePlayer(PhotonNetwork.player);
            }
        }

        public void SetSettingsFromLauncher()
        {
            maxSpec = Convert.ToInt32(PlayerPrefs.GetString("MaxSpectators"));
            gameTime = Convert.ToInt32(PlayerPrefs.GetString("GameTime"));

            chooseSkyBox = (string)PhotonNetwork.room.CustomProperties["SkyBoxes"];

            if (chooseSkyBox == "CloudSun")
            {
                RenderSettings.skybox = cloudSun;
            }
            else if (chooseSkyBox == "BlueSun")
            {
                RenderSettings.skybox = blueSun;
            }
            else if (chooseSkyBox == "Space")
            {
                RenderSettings.skybox = space;
            }
            else if (chooseSkyBox == "GoldenSun")
            {
                RenderSettings.skybox = goldenSun;
            }
            else if (chooseSkyBox == "Fantasy1")
            {
                RenderSettings.skybox = fantasy1;
            }
            else if (chooseSkyBox == "Fantasy2")
            {
                RenderSettings.skybox = fantasy2;
            }
            else if (chooseSkyBox == "AnotherPlanet")
            {
                RenderSettings.skybox = anotherPlanet;
            }
            else if (chooseSkyBox == "Nebula")
            {
                RenderSettings.skybox = nebula;
            }
            else if (chooseSkyBox == "SkyOnFire")
            {
                RenderSettings.skybox = skyOnFire;
            }
            else if (chooseSkyBox == "GloriousPink")
            {
                RenderSettings.skybox = gloriousPink;
            }
        }


        /// <summary>
        /// OnConnected is called when the client has finished joining the room (connecting, loading the scene, loading all instantiated objects und synchronizing all variables) 
        /// </summary>
        /// 
        public void OnConnected()
        {
            Debug.Log("Connected!");
            isConnected = true;
            //Set the spectator to false, when ther is an XR device
            if (XRDevice.isPresent)
                isSpectator = false;

            if (PhotonNetwork.connected)
            {
                Debug.Log("Start: " + state.ToString());

                if (PhotonNetwork.isMasterClient)
                {
                    DeactivateLeadingStatuses();
                    currentTime = gameTime;
                }

                if (isDesktopClient)
                {
                    JoinAsDesktopSpectator();
                }
                else if (!isSpectator && state == GameState.IDLE)
                {
                    //  JoinAsPlayer();
                }
                else
                {
                    JoinAsSpectator();
                }
            }
            else
            {
                playerNumber = 1;
            }
        }

        /// <summary>
        /// Keeping track of the room's status
        /// </summary>
        public void Update()
        {
            // Find the local avatar
            if (cameraRig == null)
            {
                cameraRig = GameObject.Find("XR Rig");//("LocalAvatarWithGrab");

                if (cameraRig)
                    Debug.Log("Found rig");
                else
                    Debug.Log("Didnt found rig");
            }

            SetUserInAGameList();
            //   inRoomPlayer2.text = PhotonNetwork.playerList[1].name;

            // Check if the client is completly connected
            if (!isConnected)
            {
                CheckIfClientIsFullyConnected();
            }

            // Move camera rig to it's position
            if (!isSpectator && !cameraRigHasBeenMoved)
            {
                //MoveCameraRig();
                //Debug.Log("Move rig from update");
            }

            // The master client keeps track of the current game phases
            if (PhotonNetwork.isMasterClient)
            {
                if (state == GameState.IDLE)
                {
                    currentTime = gameTime;

                    // Set the arena to player size = 4 if it isn't but another player joins
                    //if(GetAmountOfPlayers() != GetArenaPlayerSize() && GetArenaPlayerSize() != 4)
                    //{
                    //    Debug.Log(GetAmountOfPlayers() + " vs " + GetArenaPlayerSize());
                    //    photonView.RPC("SetArenaToPlayerSize", PhotonTargets.AllBuffered, 4);
                    //}
                    // Start the game automatically if there are at least two players and the countdown has finished
                    if (startsAutomatically)
                    {
                        if (GetAmountOfPlayers() > 1)
                        {
                            currentTime -= Time.deltaTime;

                            if (currentTime <= 0 || CheckAllPlayersState())
                            {
                                Debug.Log("Starting the game automatically");
                                StartGame();
                            }
                        }
                    }
                    else
                    {
                        // Start the game as soon as all connected players are ready
                        if (CheckAllPlayersState() && GetAmountOfPlayers() >= 1)
                        {
                            Debug.Log("Starting the game because all players are ready");
                            StartGame();
                        }
                    }
                }
                else if (state == GameState.COUNTDOWN_START)
                {
                    // Start countdown running
                    currentTime -= Time.deltaTime;

                    if (currentTime < 2 && (int)lastSound < (int)SoundEffect.READY)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.READY);
                    }
                    else if (currentTime < 1 && (int)lastSound < (int)SoundEffect.SET)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.SET);
                    }

                    if (currentTime <= 0)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.GO);
                        LoadLeadingStatuses();
                        state = GameState.RUNNING;
                        currentTime = gameTime;
                    }
                }
                else if (state == GameState.RUNNING)
                {
                    // The game is currently running
                    currentTime -= Time.deltaTime;

                    SpawnCubes();
                    UpdateLeadingStatuses();

                    if (currentTime < 15 && (int)lastSound < (int)SoundEffect.TEN)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.TEN);
                    }
                    if (currentTime < 9 && (int)lastSound < (int)SoundEffect.NINE)
                    {
                        //SoundManager.instance.PlaySound(SoundEffect.NINE);
                    }
                    if (currentTime < 8 && (int)lastSound < (int)SoundEffect.EIGHT)
                    {
                        //SoundManager.instance.PlaySound(SoundEffect.EIGHT);
                    }
                    if (currentTime < 7 && (int)lastSound < (int)SoundEffect.SEVEN)
                    {
                        //SoundManager.instance.PlaySound(SoundEffect.SEVEN);
                    }
                    if (currentTime < 6 && (int)lastSound < (int)SoundEffect.SIX)
                    {
                        //SoundManager.instance.PlaySound(SoundEffect.SIX);
                    }
                    if (currentTime < 5 && (int)lastSound < (int)SoundEffect.FIVE)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.FIVE);
                    }
                    if (currentTime < 4 && (int)lastSound < (int)SoundEffect.FOUR)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.FOUR);
                    }
                    if (currentTime < 3 && (int)lastSound < (int)SoundEffect.THREE)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.THREE);
                    }
                    if (currentTime < 2 && (int)lastSound < (int)SoundEffect.TWO)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.TWO);
                    }
                    if (currentTime < 1 && (int)lastSound < (int)SoundEffect.ONE)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.ONE);
                    }


                    if (currentTime <= 0)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.TIMEOVER);
                        state = GameState.COUNTDOWN_END;
                        currentTime = countdownTime;
                    }

                    //End the game when there are no players left
                    if (GetAmountOfPlayers() == 0)
                    {
                        DeactivateLeadingStatuses();
                        photonView.RPC("ResetPlayer", PhotonTargets.All, PlayerState.IDLE);
                        currentTime = gameTime;
                        lastSound = 0;

                        ConnectToLobby();
                        state = GameState.IDLE;
                    }
                }
                else if (state == GameState.COUNTDOWN_END)
                {
                    // End countdown running
                    currentTime -= Time.deltaTime;

                    if (currentTime < 3 && (int)lastSound < (int)SoundEffect.ENDTHREE)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.ENDTHREE);
                    }
                    else if (currentTime < 2 && (int)lastSound < (int)SoundEffect.ENDTWO)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.ENDTWO);
                    }
                    else if (currentTime < 1 && (int)lastSound < (int)SoundEffect.ENDONE)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.ENDONE);
                    }
                    else if (currentTime <= 0 && (int)lastSound < (int)SoundEffect.ENDZERO)
                    {
                        SoundManager.instance.PlaySound(SoundEffect.ENDZERO);
                    }

                    if (currentTime <= 0)
                    {
                        currentTime = 0;
                        DeactivateLeadingStatuses();

                        // Create prizes
                        foreach (int winner in CalculateLeadingPlayers())
                        {
                            prizes.Add(PhotonNetwork.Instantiate(prefabPrizes[winner - 1].name, GameObject.Find("Winner Player_" + winner).transform.position, GameObject.Find("Winner Player_" + winner).transform.rotation, 0));
                        }

                        state = GameState.FINISHED;
                        currentTime = 5;
                    }
                }
                else if (state == GameState.FINISHED)
                {
                    currentTime -= Time.deltaTime;

                    if (currentTime < 0)
                    {
                        currentTime = 0;
                    }

                    if (currentTime < 4.5 && (int)lastSound < (int)SoundEffect.GAMEOVER)
                    {
                        //SoundManager.instance.PlaySound(SoundEffect.GAMEOVER);
                    }
                    if (currentTime < 4 && (int)lastSound < (int)SoundEffect.WIN)
                    {
                        List<int> winners = CalculateLeadingPlayers();
                        int[] winnersArray = winners.ToArray();

                        if (winnersArray.Length == 1)
                        {
                            soundManager.PlaySoundForPlayers(SoundEffect.WIN, winnersArray);
                        }
                        else
                        {
                            soundManager.PlaySoundForPlayers(SoundEffect.TIE, winnersArray);
                        }
                    }

                    // Game has finished
                    //Check if all Players are set to ready and reset the arena
                    if (CheckAllPlayersState())
                    {
                        DeactivateLeadingStatuses();
                        photonView.RPC("ResetPlayer", PhotonTargets.All, PlayerState.IDLE);
                        currentTime = gameTime;
                        lastSound = 0;

                        //Reset the plattform count to 4 after the game has ended
                        photonView.RPC("SetArenaToPlayerSize", PhotonTargets.AllBuffered, 4);

                        // Destroy prizes
                        foreach (GameObject prize in prizes)
                        {
                            PhotonNetwork.Destroy(prize);
                        }

                        state = GameState.IDLE;
                    }

                }

            }


            for (int i = 0; i < namesPlayer.Length; i++)
            {
                namesPlayer[0] = player1OnField?.player1Name;
                namesPlayer[1] = player2OnField?.player2Name;
                namesPlayer[2] = player3OnField?.player3Name;
                namesPlayer[3] = player4OnField?.player4Name;
            }

            if (player1OnField != null)
            {
                player1connected = player1OnField.isPlayer1Joined;

            }
            if (player2OnField != null)
            {
                player2connected = player2OnField.player2joined;
            }
            if (player3OnField != null)
            {
                player3connected = player3OnField.player3joined;
            }
            if (player4OnField != null)
            {
                player4connected = player4OnField.player4joined;
            }


            pingTextPlayer1.SetActive(player1connected);
            pingTextPlayer2.SetActive(player2connected);
            pingTextPlayer3.SetActive(player3connected);
            pingTextPlayer4.SetActive(player4connected);


            if (state == GameState.RUNNING)
            {
                player1OnField = FindObjectOfType<Player1Exist>();
                player2OnField = FindObjectOfType<Player2Exist>();
                player3OnField = FindObjectOfType<Player3Exist>();
                player4OnField = FindObjectOfType<Player4Exist>();
            }

        }
        #endregion

        #region Photon Messages


        public void SetUserInAGameList()
        {
            // PlayersName 
            if (PhotonNetwork.playerList.Length > 0) player1.text = namesPlayer[0].ToString();
            if (PhotonNetwork.playerList.Length > 1) player2.text = namesPlayer[1].ToString();
            if (PhotonNetwork.playerList.Length > 2) player3.text = namesPlayer[2].ToString();
            if (PhotonNetwork.playerList.Length > 3) player4.text = namesPlayer[3].ToString();

            if (player1connected == false)
            {
                player1.text = "";
            }
            if (player2connected == false)
            {
                player2.text = "";
            }
            if (player3connected == false)
            {
                player3.text = "";
            }
            if (player4connected == false)
            {
                player4.text = "";
            }



            //if (PhotonNetwork.playerList.Length > 4) spec1.GetComponent<TextMeshProUGUI>().text = namesPlayer[4].ToString();
            //if (PhotonNetwork.playerList.Length > 5) spec2.GetComponent<TextMeshProUGUI>().text = namesPlayer[5].ToString();
            //if (PhotonNetwork.playerList.Length > 6) spec3.GetComponent<TextMeshProUGUI>().text = namesPlayer[6].ToString();
            //if (PhotonNetwork.playerList.Length > 7) spec4.GetComponent<TextMeshProUGUI>().text = namesPlayer[7].ToString();
        }

        public void PlayerPing()
        {
            // Playerping with Coroutine
            if (players[0] != null)
            {
                if (player1OnField.playerSetupPlayer1 != null)
                {
                    player1connected = true;
                    pingTextPlayer1.SetActive(player1connected);
                    pointsPlayer1.gameObject.SetActive(player1connected);
                    ping[0] = scriptableGameStats.playerInfos[0].ping;
                    pingTextPlayer1.GetComponent<TextMeshProUGUI>().text = ping[0].ToString();

                    if (player1OnField.playerSetupPlayer1 == null)
                    {
                        player1connected = false;
                    }
                }
            }

            if (players[1] != null)
            {
                if (player2OnField.playerSetupPlayer2 != null)
                {
                    player2connected = true;
                    pingTextPlayer2.SetActive(player2connected);
                    pointsPlayer2.gameObject.SetActive(player2connected);
                    ping[1] = scriptableGameStats.playerInfos[1].ping;
                    pingTextPlayer2.GetComponent<TextMeshProUGUI>().text = ping[1].ToString();

                    if (player2OnField.playerSetupPlayer2 == null)
                    {
                        player2connected = false;
                    }
                }
            }

            if (players[2] != null)
            {
                if (player3OnField.playerSetupPlayer3 != null)
                {
                    player3connected = true;
                    pingTextPlayer3.SetActive(player3connected);
                    pointsPlayer3.gameObject.SetActive(player3connected);
                    ping[2] = scriptableGameStats.playerInfos[2].ping;
                    pingTextPlayer3.GetComponent<TextMeshProUGUI>().text = ping[2].ToString();

                    if (player3OnField.playerSetupPlayer3 == null)
                    {
                        player3connected = false;

                    }
                }
            }

            if (players[3] != null)
            {
                if (player4OnField.playerSetupPlayer4 != null)
                {
                    player4connected = true;
                    pingTextPlayer4.SetActive(player4connected);
                    pointsPlayer4.gameObject.SetActive(player4connected);
                    ping[3] = scriptableGameStats.playerInfos[3].ping;
                    pingTextPlayer4.GetComponent<TextMeshProUGUI>().text = ping[3].ToString();

                    if (player4OnField.playerSetupPlayer4 == null)
                    {
                        player4connected = false;
                    }
                }
            }
            StartCoroutine(pingPlayer());
        }

        public IEnumerator pingPlayer()
        {
            yield return new WaitForSeconds(3);
            PlayerPing();
        }

        /// <summary>
        /// Load the menu scene when the player leaves the game
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        /// <summary>
        /// Called when a new player joins the room
        /// </summary>
        /// <param name="player"></param>
        public override void OnPhotonPlayerConnected(PhotonPlayer player)
        {
            Debug.Log("[GameManager] New player connected with ID: " + player.ID);

            //Start the countdown if the room is set to start the game automatically
            if (PhotonNetwork.isMasterClient)
            {
                if (PhotonNetwork.room.PlayerCount == 2 && state == GameState.IDLE)
                {
                    currentTime = startAutomaticallyTime;
                }
            }

        }

        /// <summary>
        /// Deletes the player from the hashtable and locally when disconnecting to release it's position so a new player can connect and take his place
        /// </summary>
        /// <param name="player"></param>
        public override void OnPhotonPlayerDisconnected(PhotonPlayer player)
        {
            Debug.Log("[GameManager] Player disconnected with ID " + player.ID);

            if (PhotonNetwork.isMasterClient)
            {
                DeletePlayer(player);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Starts the game
        /// </summary>
        private void StartGame()
        {
            ReassignPlayerNumbers();
            DeactivateLeadingStatuses();
            photonView.RPC("SetArenaToPlayerSize", PhotonTargets.AllBuffered, GetAmountOfPlayers());
            photonView.RPC("ResetPlayer", PhotonTargets.All, PlayerState.START);

            // Set the arena to player size = 4 if it isn't but another player joins
            if (GetAmountOfPlayers() != GetArenaPlayerSize() && GetArenaPlayerSize() != 4)
            {
                Debug.Log(GetAmountOfPlayers() + " vs " + GetArenaPlayerSize());
                photonView.RPC("SetArenaToPlayerSize", PhotonTargets.AllBuffered, 4);
            }

            currentTime = countdownTime;
            state = GameState.COUNTDOWN_START;
        }

        /// <summary>
        /// Join the game as player
        /// </summary>
        private void JoinAsPlayer()
        {
            isplayer = true;
            // Register the player
            playerNumber = CalculatePlayerNumber();
            photonView.RPC("AddPlayerToHashtable", PhotonTargets.All, PhotonNetwork.player.ID, playerNumber);

            // Create the player
            playerSetup = PhotonNetwork.Instantiate(this.prefabPlayerSetup.name, GameObject.Find("CameraRig Player_" + playerNumber).transform.position, GameObject.Find("CameraRig Player_" + playerNumber).transform.rotation, 0);

            Shield = GameObject.Find("Shield");
            Gun playerGun = FindObjectOfType<Gun>();

            playerGun.playerNumber = playerNumber;


            playerSetup.transform.localPosition += new Vector3(0, 0, 0);
            //playerSetup.GetComponent<Player>().SetPlayerNumber(playerNumber);

            isSpectator = false;
            cameraRigHasBeenMoved = false;

            MoveCameraRig();

            onPlayerNumberUpdate?.Invoke(playerNumber);
        }

        /// <summary>
        /// Join the game as spectator
        /// </summary>
        private void JoinAsSpectator()
        {
            // Delete the player if he was playing before
            if (playerSetup != null)
            {
                PhotonNetwork.Destroy(playerSetup.GetPhotonView());
                DeletePlayer(PhotonNetwork.player);
            }

            // Position the player as spectator
            GameObject spectatorPosition = GameObject.Find("SpectatorPosition");
            cameraRig.transform.parent = null;
            cameraRig.transform.position = spectatorPosition.transform.position;
            cameraRig.transform.rotation = spectatorPosition.transform.rotation;

            playerSetup = null;
            isSpectator = true;

            UpdatePlayers();
        }

        /// <summary>
        /// Joins the game as spectator with a desktop client
        /// </summary>
        private void JoinAsDesktopSpectator()
        {
            Debug.Log("Joined as desktop spectator");

            cameraRig.SetActive(false);

            GameObject spectatorSetup = Instantiate(prefabSpectator, Vector3.zero, Quaternion.identity);
            spectatorSetup.transform.position = new Vector3(0, 4, -5);

            playerSetup = null;
            isSpectator = true;

            UpdatePlayers();
        }

        /// <summary>
        /// Deletes a player in the hashtable and updates it for all other players
        /// </summary>
        private void DeletePlayer(PhotonPlayer player)
        {
            ExitGames.Client.Photon.Hashtable players = PhotonNetwork.room.CustomProperties;

            // Iterate through all properties to find the player's playNumber who just disconnected
            for (int i = 1; i < 5; i++)
            {
                string key = "Player_" + i;

                // Check whether hashtable contains the player
                if (players.ContainsKey(key))
                {
                    int value = (int)players[key];

                    if (value == player.ID)
                    {
                        // Delete the player in the hash table
                        ExitGames.Client.Photon.Hashtable deletePlayer = new ExitGames.Client.Photon.Hashtable();
                        deletePlayer.Add(key, null);
                        PhotonNetwork.room.SetCustomProperties(deletePlayer);

                        // Delete the player for everyone locally
                        photonView.RPC("UpdatePlayers", PhotonTargets.All);

                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the given player locally by updating the playerClone and saving it in the game manager
        /// </summary>
        /// <param name="id">ID of the player that should be updated</param>
        /// <param name="playerNumber">New playerNumber of the player</param>
        private void UpdatePlayerLocally(int id, int playerNumber)
        {
            Debug.Log("Updating PlayerClone | ID: " + id + " | number: " + playerNumber);
            Player[] players = FindObjectsOfType<Player>();
            GameObject[] players2 = GameObject.FindGameObjectsWithTag("Player");


            Debug.Log(players.Length + " players found");
            Debug.Log(players2.Length + " players found");

            foreach (Player player in players)
            {
                if (player.photonView.ownerId == id)
                {
                    // Update playerClone
                    player.SetPlayerNumber(playerNumber);

                    // Update gameManager
                    this.players[playerNumber - 1] = player.gameObject;

                    Debug.Log(" -> Updated PlayerClone | ID: " + id + " | number: " + playerNumber);
                }
                else
                {
                    Debug.Log("OwnerID " + player.photonView.ownerId + " != " + id);
                }
            }
        }

        /// <summary>
        /// Changes the position of a player in the hashtable
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void ChangePlayerInHashtable(int from, int to)
        {
            ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.room.CustomProperties;
            ExitGames.Client.Photon.Hashtable changePlayer = new ExitGames.Client.Photon.Hashtable();

            string key = "Player_" + from;
            int id = (int)hashtable[key];

            changePlayer.Add(key, null);

            key = "Player_" + to;
            changePlayer.Add(key, id);

            PhotonNetwork.room.SetCustomProperties(changePlayer);
            Debug.Log("[GameManager] Changed player in hashtable with ID " + id + " from " + from + " to " + to);
        }

        /// <summary>
        /// Reassigns all player's playerNumbers locally
        /// </summary>
        private void ReassignPlayerNumbers()
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] == null)
                {
                    for (int x = i + 1; x < players.Length; x++)
                    {
                        //Change a player's position
                        if (players[x] != null)
                        {
                            players[i] = players[x];
                            players[i].GetComponent<Player>().SetPlayerNumber(i + 1);
                            players[x] = null;

                            if (PhotonNetwork.isMasterClient)
                            {
                                ChangePlayerInHashtable(x + 1, i + 1);
                            }

                            x = players.Length + 1;
                        }

                        if (x == players.Length)
                        {
                            return;
                        }
                    }
                }
            }

            photonView.RPC("UpdatePlayers", PhotonTargets.All);
        }

        /// <summary>
        /// Rotates the camera rig accordingly to the player's number to fit the player with his table
        /// </summary>
        private void MoveCameraRig()
        {
            Debug.Log("Moving Rig");

            // Rotate only if the playerNumber has already been calculated
            if (playerNumber != 0)
            {
                float PlayerCamPosition = 0.004f;

                Debug.Log("Moving camera rig to position: " + playerNumber);

                if (!cameraRig)
                    cameraRig = GameObject.Find("XR Rig(Clone)");
                float PlayerCamPos = cameraRig.transform.position.y - PlayerCamPosition;

                GameObject cameraRigPlayer = GameObject.Find("CameraRig Player_" + playerNumber);

                cameraRig.transform.parent = cameraRigPlayer.transform;
                cameraRig.transform.position = cameraRigPlayer.transform.position;
                cameraRig.transform.rotation = cameraRigPlayer.transform.rotation;

                cameraRig.transform.position = new Vector3(cameraRigPlayer.transform.position.x, PlayerCamPos, cameraRigPlayer.transform.position.z);

                cameraRigHasBeenMoved = true;

                //remove the player model from culling mask
                var remover = cameraRig.GetComponentInChildren<RemovePlayerModelFromCullingMask>();
                if (remover != null)
                    remover.Refresh();

                Debug.Log("Moved Rig");
            }

        }

        /// <summary>
        /// Moves this player's spawner to it's spawn position
        /// </summary>
        private void MoveSpawner()
        {
            if (playerSetup != null)
            {
                playerSetup.transform.GetChild(1).GetComponent<Spawner>().MoveToSpawnerPosition();
            }
        }

        /// <summary>
        /// Calculates the playerNumber of this player using the hashtable
        /// </summary>
        private int CalculatePlayerNumber()
        {
            Debug.Log("Calculating Player Number");
            ExitGames.Client.Photon.Hashtable players = PhotonNetwork.room.CustomProperties;

            for (int i = 1; i < 5; i++)
            {
                string key = "Player_" + i;

                if (!players.ContainsKey(key))
                {
                    Debug.Log("[CalculatePlayerNumber] Player set to Player_" + i);
                    return i;
                }
            }

            return 0;
        }

        /// <summary>
        /// Calculate the leading players and returns their playerNumbers
        /// </summary>
        /// <returns>The playerNumbers of the leading players</returns>
        private List<int> CalculateLeadingPlayers()
        {
            List<int> winners = new List<int>();
            int winnerPoints = 0;

            for (int i = 0; i < points.Length; i++)
            {
                Debug.Log($"Points at {i}: {points[i]}");

                if (players[i] != null)
                {
                    if (points[i] > winnerPoints)
                    {
                        // Player has more points then others
                        winners.Clear();
                        winners.Add(i + 1);
                        winnerPoints = points[i];
                    }
                    else if (points[i] == winnerPoints)
                    {
                        // Player has as much points as others
                        winners.Add(i + 1);

                    }
                }
            }

            return winners;
        }

        /// <summary>
        /// Calculates the current amount of players and returns it
        /// </summary>
        /// <returns>The current amount of players</returns>
        private int GetAmountOfPlayers()
        {
            int amount = 0;

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] != null)
                {
                    amount++;
                }
            }

            return amount;
        }

        /// <summary>
        /// Calculates the current amount of players in the hashtable and returns it
        /// </summary>
        /// <returns></returns>
        private int GetAmountOfPlayersInHashtable()
        {
            int amount = 0;
            // Update list of player again
            ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.room.CustomProperties;

            for (int i = 1; i < 5; i++)
            {
                string key = "Player_" + i;

                if (hashtable.ContainsKey(key))
                {
                    amount += 1;
                }
            }

            return amount;
        }

        /// <summary>
        /// Checks whether all players are ready to start the game
        /// </summary>
        /// <returns>True if all players are ready</returns>
        private bool CheckAllPlayersState()
        {
            lastCheckedAllPlayersState = Time.time;

            foreach (GameObject player in GetConnectedPlayers())
            {
                if (state == GameState.IDLE)
                {
                    if (player.GetComponent<Player>().state != PlayerState.START)
                    {
                        return false;
                    }
                }
                else if (state == GameState.FINISHED)
                {
                    if (player.GetComponent<Player>().state != PlayerState.RESET)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether this client is completly connected and calls OnConnected if true
        /// </summary>
        private void CheckIfClientIsFullyConnected()
        {
            // Connect if you are the first client in the room
            if (PhotonNetwork.isMasterClient && cameraRig != null)
            {
                OnConnected();
            }
            // Are vaiables synced and complete connection is not yet established
            else if (variablesSynced && cameraRig != null)
            {
                // Are players connected?
                if (GetAmountOfPlayersInHashtable() >= 1)
                {
                    if (gameObjectsSynced)
                    {
                        OnConnected();
                    }
                }
                else
                {
                    // Connect if there are spectators only
                    OnConnected();
                }

            }
        }

        /// <summary>
        /// Gets all currently conncted players and returns them
        /// </summary>
        /// <returns>All currently connected players</returns>
        private GameObject[] GetConnectedPlayers()
        {
            List<GameObject> connectedPlayers = new List<GameObject>();

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] != null)
                {
                    connectedPlayers.Add(players[i]);
                }
            }

            return connectedPlayers.ToArray();
        }

        /// <summary>
        /// Gets the player size of the currently active arena and returns it
        /// </summary>
        /// <returns>The player size of the current arena</returns>
        private int GetArenaPlayerSize()
        {
            for (int i = 0; i < arenas.Length; i++)
            {
                if (arenas[i].active)
                {
                    return i + 1;
                }
            }

            return 0;
        }

        /// <summary>
        /// Spawn cubes every x seconds. For fair gameplay the spawned cubes are equal for all players
        /// </summary>
        private void SpawnCubes()
        {
            if (lastSpawnTime + spawnRate < Time.time)
            {
                if (players[0] == null)
                    return;

                int randomCube = UnityEngine.Random.Range(0, players[0].GetComponent<Player>().spawner.cubes.Length);
                Debug.Log("Cubeplayer2Spawned");

                for (int i = 0; i < 4; i++)
                {
                    if (players[i] != null)
                    {
                        players[i].GetComponent<Player>().spawner.photonView.RPC("SpawnCube", PhotonTargets.All, randomCube);

                    }
                }

                lastSpawnTime = Time.time;
            }
        }

        /// <summary>
        /// Finds all leading statuses that are currently active
        /// </summary>
        private void LoadLeadingStatuses()
        {
            leadingStatuses.Clear();

            foreach (LeadingStatus leading in FindObjectsOfType<LeadingStatus>())
            {
                leadingStatuses.Add(leading.gameObject);
            }
        }

        /// <summary>
        /// Updates the leading player indicators
        /// </summary>
        private void UpdateLeadingStatuses()
        {
            // Deactivate all leading statuses
            foreach (GameObject leading in leadingStatuses)
            {
                leading.GetComponent<LeadingStatus>().SetIsLeading(false);
            }

            // Activate leading statuses of all leading players
            foreach (int playerNumber in CalculateLeadingPlayers())
            {
                foreach (GameObject leading in leadingStatuses)
                {
                    if (leading.transform.name == "LeadingStatus Player_" + playerNumber)
                    {
                        leading.GetComponent<LeadingStatus>().SetIsLeading(true);
                    }
                }
            }
        }

        /// <summary>
        /// Deactivates all leading statuses
        /// </summary>
        private void DeactivateLeadingStatuses()
        {
            Debug.Log("Deactivating");
            LoadLeadingStatuses();

            // Deactivate all leading statuses
            foreach (GameObject leading in leadingStatuses)
            {
                Debug.Log("Deactivated");
                leading.GetComponent<LeadingStatus>().SetIsLeading(false);
            }
        }

        private IEnumerator ShowMessageForTime(GameObject _messageObject, float _seconds)
        {
            _messageObject.SetActive(true);

            yield return new WaitForSeconds(_seconds);

            _messageObject.SetActive(false);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Toggles between player and spectator
        /// </summary>
        public void TogglePlayerSpectator()
        {
            if (state == GameState.IDLE)
            {
                Debug.Log("Toggle between player and spectator");
                if (isSpectator)
                {
                    JoinAsPlayer();
                }
                else
                {
                    JoinAsSpectator();
                }
            }
        }

        /// <summary>
        /// Sets the gametime
        /// </summary>
        /// <param name="time">Time one game should last</param>
        public void SetGameTime(int time)
        {
            if (PhotonNetwork.isMasterClient || !PhotonNetwork.connected)
            {
                gameTime = time;
            }
        }

        /// <summary>
        /// Leave the room
        /// </summary>
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        /// <summary>
        /// Gets the ID of the player with the given playerNumber and returns it
        /// </summary>
        /// <param name="playerNumber"></param>
        /// <returns>The ID of the given player</returns>
        public int GetPlayerIdWithPlayerNumber(int playerNumber)
        {
            ExitGames.Client.Photon.Hashtable players = PhotonNetwork.room.CustomProperties;

            string key = "Player_" + playerNumber;

            if (players.ContainsKey(key))
            {
                Debug.Log("[GameManager] Player_" + playerNumber + " has got ID " + (int)players[key]);
                return (int)players[key];
            }

            return 0;
        }

        /// <summary>
        /// Sets the point for a player
        /// </summary>
        /// <param name="playerNumber">The player number of the player</param>
        /// <param name="points">The amount of points</param>
        public void SetPointsForPlayer(int playerNumber, int points)
        {
            if (playerNumber >= 1 && playerNumber <= 4)
            {
                this.points[playerNumber - 1] = points;
            }
        }

        /// <summary>
        /// Sets the instance of a player
        /// </summary>
        /// <param name="playerNumber">The player number of the player</param>
        /// <param name="playerSetup">The player's instance</param>
        public void SetPlayer(int playerNumber, GameObject playerSetup)
        {
            Debug.Log("SetPlayer() - Length: " + players.Length + ", playerNumber: " + playerNumber);
            players[playerNumber - 1] = playerSetup;
        }

        /// <summary>
        /// Gets the playerNumbers of all connected players and returns them
        /// </summary>
        /// <returns>The playerNumbers of all connected players</returns>
        public int[] GetPlayerNumbersOfConnectedPlayers()
        {
            int[] playerNumbers = new int[GetAmountOfPlayers()];
            int playerCounter = 0;

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] != null)
                {
                    playerNumbers[playerCounter] = players[i].GetComponent<Player>().playerNumber;
                    playerCounter++;
                }
            }

            return playerNumbers;
        }

        public void SetPlayerReady()
        {
            playerSetup.GetComponent<Player>().ToggleReady();
        }

        #endregion

        #region RFCs

        /// <summary>
        /// Synchronizes the local players with the hashtable
        /// This method is called whenever something in the hashtable changes
        /// </summary>
        [PunRPC]
        private void UpdatePlayers()
        {
            // Avoid trying to update before the client is fully connected
            if (isConnected)
            {
                Debug.Log("Updating players");

                // Empty list of players
                for (int i = 0; i < players.Length; i++)
                {
                    players[i] = null;
                }

                // Update list of player again
                ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.room.CustomProperties;

                for (int i = 1; i < 5; i++)
                {
                    string key = "Player_" + i;

                    if (hashtable.ContainsKey(key))
                    {
                        Debug.Log("Player " + i + " has been found in the hashtable as " + (int)hashtable[key] + " and will be updated locally");
                        UpdatePlayerLocally((int)hashtable[key], i);
                    }
                }
            }
        }

        /// <summary>
        /// Reset the player by setting it's isReady value to 'false' and deleting all of it's stacking objects
        /// </summary>
        /// <param name="value">Value the player's state should be set to</param>
        [PunRPC]
        private void ResetPlayer(PlayerState value)
        {
            if (!isSpectator)
            {
                // Reset playerState and points
                playerSetup.GetComponent<Player>().SetState(value);
                playerSetup.GetComponent<Player>().ResetPoints();

                // Delete all of the player's stacking objects
                foreach (GameObject stackingObject in GameObject.FindGameObjectsWithTag("StackingObject"))
                {
                    if (stackingObject.GetComponent<PhotonView>().isMine)
                    {
                        stackingObject.GetComponent<StackingObject>().Delete();
                    }
                }
            }

        }

        /// <summary>
        /// Activates the arena for the given player size and deactivates all others
        /// </summary>
        /// <param name="playerSize">The player size the arena should be set to</param>
        [PunRPC]
        private void SetArenaToPlayerSize(int playerSize)
        {
            Debug.Log("[GameManager] Set arena to player size: " + playerSize);

            for (int i = 0; i < arenas.Length; i++)
            {
                if (playerSize == i + 1)
                {
                    arenas[i].SetActive(true);
                }
                else
                {
                    arenas[i].SetActive(false);
                }
            }

            if (!isSpectator)
            {
                MoveCameraRig();
                MoveSpawner();
            }
        }

        /// <summary>
        /// Adds a player to the game
        /// </summary>
        /// <param name="player">The player setup</param>
        /// <param name="playerNumber">The player number</param>
        [PunRPC]
        private void AddPlayerToHashtable(int playerID, int playerNumber)
        {
            if (PhotonNetwork.isMasterClient || !PhotonNetwork.connected)
            {
                ExitGames.Client.Photon.Hashtable newPlayer = new ExitGames.Client.Photon.Hashtable();

                string key = "Player_" + playerNumber;

                newPlayer.Add(key, playerID);
                PhotonNetwork.room.SetCustomProperties(newPlayer);

                Debug.Log("[GameManager] New Player added to hashtable: " + playerNumber);
                photonView.RPC("UpdatePlayers", PhotonTargets.All);
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                stream.SendNext(amountOfPlayers);
                stream.SendNext(chooseSkyBox);
                stream.SendNext(state);
                stream.SendNext(currentTime);
                stream.SendNext(settingTransferOwnershipNewOwnerInterpolation);
                stream.SendNext(settingTransferOwnershipOldOwnerInterpolation);
                stream.SendNext(settingTransOwnershipNewOwnerMultiplier);
                stream.SendNext(namesPlayer);
                stream.SendNext(ping);
            }
            else if (stream.isReading)
            {
                amountOfPlayers = (int)stream.ReceiveNext();
                chooseSkyBox = (string)stream.ReceiveNext();
                state = (GameState)stream.ReceiveNext();
                currentTime = (float)stream.ReceiveNext();
                settingTransferOwnershipNewOwnerInterpolation = (bool)stream.ReceiveNext();
                settingTransferOwnershipOldOwnerInterpolation = (bool)stream.ReceiveNext();
                settingTransOwnershipNewOwnerMultiplier = (float)stream.ReceiveNext();
                namesPlayer = (string[])stream.ReceiveNext();
                ping = (int[])stream.ReceiveNext();
                // Connect as soon as the variable state is synchronized for the first time
                if (!variablesSynced)
                {
                    Debug.Log("Variables recieved");

                    variablesSynced = true;
                }
            }
        }
    }

    #endregion
}
