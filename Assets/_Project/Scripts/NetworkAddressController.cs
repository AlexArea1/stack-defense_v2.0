﻿using Com.VisionMe.StackDefense;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using TMPro;
using UnityEngine;

public class NetworkAddressController : MonoBehaviour
{
    public string ipAddress = "95.90.206.65";
    public string port = "5055";
    public string ipAddressSaveName = "IpAddress";
    public string portSaveName = "Port";
    public bool setLocalIpAddress = false;
    public TMP_InputField ipAddressText;
    public TMP_InputField portText;

    public void SetupServerSettings()
    {
        SetIpAddress();
        SetPort();
        PhotonNetwork.PhotonServerSettings.ServerAddress = ipAddress;
        PhotonNetwork.PhotonServerSettings.ServerPort = int.Parse(port);
    }

    public void SetIpAddress()
    {
        ipAddress = ipAddressText.text;
        PlayerPrefs.SetString(ipAddressSaveName, ipAddress);
    }

    public void SetPort()
    {
        portText.text = port;
        PlayerPrefs.SetString(portSaveName, port);
    }

    public string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                //hintText.text = ip.ToString();
                return ip.ToString();
            }
        }
        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }


    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.PhotonServerSettings.HostType = ServerSettings.HostingOption.PhotonCloud;

        Debug.Log("GetLocalIPAddress: " + GetLocalIPAddress());
        if (setLocalIpAddress) ipAddress = GetLocalIPAddress();
        ipAddressText.text = PlayerPrefs.GetString(ipAddressSaveName, ipAddress);
        portText.text = PlayerPrefs.GetString(portSaveName, port);

        // ConnectToMaster() has two more parameters: "appID" and "gameVersion". Both are only relevant for the Photon Cloud and can be set to any value when you host Photon yourself. 
        //PhotonNetwork.ConnectToMaster(ipAddress, port,"", PhotonNetwork.gameVersion);
        //PhotonNetwork.ConnectUsingSettings("");
        //PhotonServerSettings
        //PhotonPlayer.ipAddress = ipAddress;
        //and PhotonPlayer.port
    }

}
