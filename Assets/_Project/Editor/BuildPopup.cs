﻿#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;
using UnityEditor.Build.Reporting;


namespace Com.VisionMe.StackDefense.Editor
{
    [InitializeOnLoad]
    public class BuildPopup : EditorWindow
    {
        private string previousVersion;

        private string buildVersion;

        private string outputPath;

        private string rootPath;

        private BuildPlayerOptions _options;

        private const string PathKey = "BUILD_DEFAULTPATH";

        static BuildPopup()
        {
            BuildPlayerWindow.RegisterBuildPlayerHandler(OnBuild);
        }

        private static void OnBuild(BuildPlayerOptions options)
        {
            Display(options);
        }

        public static void Display(BuildPlayerOptions options)
        {
            BuildPopup window = CreateInstance<BuildPopup>();
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 550, 450);
            window.titleContent = new GUIContent("Build");
            window._options = options;

            //get the path from previous
            window.rootPath = getRootPath();
            window.previousVersion = window.buildVersion = PlayerSettings.bundleVersion;
            window.ShowModalUtility();
        }

        void OnGUI()
        {
            GUILayout.Space(10);
            EditorGUILayout.LabelField("Previous Version:", previousVersion, EditorStyles.wordWrappedLabel);
            GUILayout.Space(10);
            buildVersion = EditorGUILayout.TextField("Version: ", buildVersion);
            GUILayout.Space(60);
            // outputPath = $"{rootPath}{Path.DirectorySeparatorChar}v{buildVersion}";
            // EditorGUILayout.LabelField("Output Path:", outputPath, EditorStyles.wordWrappedLabel);
            // if (GUILayout.Button("Change output root"))
            // {
            //     string newRoot = EditorUtility.OpenFolderPanel("Select Build Output Root Path", rootPath, "");
            //     newRoot = unifyDirectorySeparatorChars(newRoot);
            //     if (!string.IsNullOrEmpty(newRoot))
            //         rootPath = newRoot;
            // }
            //
            // GUILayout.Space(100);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Cancel"))
            {
                Close();
            }

            if (GUILayout.Button($"BUILD {EditorUserBuildSettings.activeBuildTarget}"))
            {
                //write version, date and time into the version prefab
                if (buildVersion != "")
                    PlayerSettings.bundleVersion = buildVersion;

                //save the root path for next time
                setRootPath(rootPath);

                PerformBuild();

                Close();
                GUIUtility.ExitGUI();
            }

            EditorGUILayout.EndHorizontal();
        }

        private static string unifyDirectorySeparatorChars(string path)
        {
            path = path.Replace('\\', Path.DirectorySeparatorChar);
            path = path.Replace('/', Path.DirectorySeparatorChar);
            return path;
        }

        private void PerformBuild()
        {
            BuildPipeline.BuildPlayer(_options);
        }

        private static string getRootPath()
        {
            if (!PlayerPrefs.HasKey(PathKey))
                return "";
            return unifyDirectorySeparatorChars(PlayerPrefs.GetString(PathKey));
        }

        private static void setRootPath(string newRootPath)
        {
            PlayerPrefs.SetString(PathKey, unifyDirectorySeparatorChars(newRootPath));
        }
    }
}
#endif