﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.VisionMe.StackDefense
{
    public class PlayerPing : MonoBehaviour
    {
        public int ping { get; private set; }

        [SerializeField] private float seconds = 3f;
        [SerializeField] private ScriptableGameStats scriptableGameStats;
        [SerializeField] private PlayerStatsPhoton playerStatsPhoton;

        void Start()
        {
            StartCoroutine(Co_GetPingEvery(seconds));
        }

        IEnumerator Co_GetPingEvery(float seconds)
        {
            while (true)
            {
                yield return new WaitForSeconds(seconds);
                SetOwnPlayerPing();
                GetOtherPlayerPing();
            }
        }

        private void SetOwnPlayerPing()
        {
            ping = PhotonNetwork.GetPing();
            playerStatsPhoton?.SetStat("Ping", ping);

            var player = PhotonNetwork.player;

            if (!player.CustomProperties.ContainsKey("PlayerNumber"))
            {
                Debug.Log($"SetOwnPlayerPing() { ping } { player } { player.ID }: playerNumber not found ");
                return;
            }

            var playerNumber = PhotonNetwork.player.CustomProperties["PlayerNumber"];
            if (playerNumber is int)
            {
                scriptableGameStats?.SetPing((int)playerNumber, ping);
            }

        }

        private void GetOtherPlayerPing()
        {
            foreach (var player in PhotonNetwork.otherPlayers)
            {
                var playerNumber = player.CustomProperties["PlayerNumber"];
                if (playerNumber == null) continue;

                int playerPing = (int)player.CustomProperties["Ping"];
                Debug.Log($"{player.NickName} [{ (int)playerNumber }] : {player.CustomProperties["Ping"]}ms");

                scriptableGameStats?.SetPing((int)playerNumber, playerPing);

            }
        }
    }
}