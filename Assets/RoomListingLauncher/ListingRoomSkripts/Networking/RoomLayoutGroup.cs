﻿using System.Collections.Generic;
using UnityEngine;

public class RoomLayoutGroup : MonoBehaviour
{
    [SerializeField]
    private GameObject _roomListingPrefab;
    private GameObject RoomListingPrefab
    {
        get { return _roomListingPrefab; }
    }
    public string RoomName { get; private set; }


    private List<RoomListing> _roomListingButtons = new List<RoomListing>();
    private List<RoomListing> RoomlistingButtons
    {
        get { return _roomListingButtons; }
    }


    private void OnReceivedRoomListUpdate()
    {
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();

        foreach (RoomInfo room in rooms)
        {
            RoomReceived(room);
        }
        RemoveOldRooms();
    }

    private void RoomReceived(RoomInfo room)
    {
        int index = RoomlistingButtons.FindIndex(x => x.RoomName == room.Name);

        if (index == -1)
        {
            if (room.IsVisible && room.PlayerCount < room.MaxPlayers)
            {
                GameObject roomListingObj = Instantiate(RoomListingPrefab);
                roomListingObj.transform.SetParent(transform, false);

                RoomListing roomListing = roomListingObj.GetComponent<RoomListing>();
                RoomlistingButtons.Add(roomListing);

                index = (RoomlistingButtons.Count - 1);
            }
        }


        if (index != -1)
        {
            RoomListing roomListing = RoomlistingButtons[index];
            roomListing.SetRoomNameText(room.Name);
            roomListing.Update = true;
        }
    }

    private void RemoveOldRooms()
    {
        List<RoomListing> removeRooms = new List<RoomListing>();

        foreach (RoomListing roomListing in RoomlistingButtons)
        {
            if (!roomListing.Update)
                removeRooms.Add(roomListing);
            else 
                roomListing.Update = false;
        }
        foreach (RoomListing roomListing in removeRooms)
        {
            GameObject roomListingObj = roomListing.gameObject;
            RoomlistingButtons.Remove(roomListing);
            Destroy(roomListingObj);
        }
    }

}
