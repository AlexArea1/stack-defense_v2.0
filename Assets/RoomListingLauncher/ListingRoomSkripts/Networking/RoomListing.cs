﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RoomListing : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField _roomNameText;

    private TMP_InputField RoomNameText 
    {
        get { return _roomNameText; }
    }

    public string RoomName { get; private set; }
    public bool Update { get ; set; }

    private void Start()
    {
        GameObject lobbyCanvasObj = MainCanvasManager.Instance.LobbyCanvas.gameObject;
        if (lobbyCanvasObj == null) return;

        LobbyCanvas lobbyCanvas = lobbyCanvasObj.GetComponent<LobbyCanvas>();

        Button button = GetComponent<Button>();
        button.onClick.AddListener(() => lobbyCanvas.OnClickJoinRoom(RoomNameText.text));
    }

    private void OnDestroy()
    {
        Button button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
    }

    public void SetRoomNameText(string text)
    {
        RoomName = text;
        RoomNameText.text = RoomName;
    }

    public void SetRoomNameTextOfObject()
    {
      
        GameObject RoomText;
        RoomText = GameObject.Find("RoomName");
        _roomNameText = RoomText.GetComponent<TMP_InputField>();
    }
    
}
