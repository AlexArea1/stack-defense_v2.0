﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CreateRoom : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField _roomName;
    private TMP_InputField RoomName
    {
        get { return _roomName; }
    }
    // Update is called once per frame
    public void OnClick_CreateRoom()
    {

        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 4 };


        if (PhotonNetwork.CreateRoom(RoomName.text, roomOptions, TypedLobby.Default))
        {
            print("sucsess");
        }
        else
        {
            print("faild");
        }


    }
    private void OnPhotonCreateRoomFailed(object[] codeAndMessage)
    {
        print("");
    }

    public void OnCreatedRoom()
    {

    }
}
