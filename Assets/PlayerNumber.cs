﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Com.VisionMe.StackDefense
{
    public class PlayerNumber : MonoBehaviour
    {
        private GameManager gameManager;
        [SerializeField] private PlayerStatsPhoton playerStatsPhoton;
        private void OnEnable()
        {
            gameManager = FindObjectOfType<GameManager>();
            gameManager.onPlayerNumberUpdate += HandlePlayerNumberUpdate;
        }

        private void HandlePlayerNumberUpdate(int playerNumber)
        {
            Debug.Log($"HandlePlayerNumberUpdate { playerNumber }" );
            playerStatsPhoton.SetStat("PlayerNumber", playerNumber);
        }

        private void OnDisable()
        {
            gameManager.onPlayerNumberUpdate -= HandlePlayerNumberUpdate;
        }

    }
}