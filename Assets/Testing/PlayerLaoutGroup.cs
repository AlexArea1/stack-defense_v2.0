﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLaoutGroup : MonoBehaviour
{
    bool variablesSynced = false;

    [SerializeField]
    private GameObject _playerListingPrefab;
    private GameObject PlayerListingPrefab
    {
        get { return _playerListingPrefab; }
    }

    private List<PlayerListing> _playerListings = new List<PlayerListing>();
    private List<PlayerListing> PlayerListings
    {
        get { return _playerListings; }
    }

    Player1Exist player1;
    Player2Exist player2;
    Player3Exist player3;
    Player4Exist player4;
    PhotonPlayer player;

   // PhotonPlayer player;

    public void Start()
    {
        //PhotonPlayer[] photonPlayers = PhotonNetwork.playerList;
        //for (int i = 0; i < photonPlayers.Length; i++)
        //{
        //    PlayerJoinedRoom(photonPlayers[i]);
        //}
    }
    public void Update()
    {

    }

    // photon when a player leaves a room
    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        PlayerLeftRoom(player);
    }

    public void PlayerJoinedRoom()
    {
        //if (player == null)
        //    return;
        Debug.Log("Geschafft");
        //PlayerLeftRoom(player);

        GameObject playerListingObj = Instantiate(PlayerListingPrefab);
        playerListingObj.transform.SetParent(transform, false);

        PlayerListing playerListing = playerListingObj.GetComponent<PlayerListing>();
        //PhotonPlayer player;
        //playerListing.ApplyPhotonPlayer(PhotonPlayer player);
        PlayerListings.Add(playerListing);
    }

    private void PlayerLeftRoom(PhotonPlayer player)
    {
        int index = PlayerListings.FindIndex(x => x.PhotonPlayer == player);
        if (index != -1)
        {
            Destroy(PlayerListings[index].gameObject);
            PlayerListings.RemoveAt(index);
        }
    }
}
