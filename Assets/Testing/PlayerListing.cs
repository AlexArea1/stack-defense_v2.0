﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerListing : MonoBehaviour
{
    public PhotonPlayer PhotonPlayer { get; private set; }

    [SerializeField]
    public TextMeshProUGUI pingtext;

    [SerializeField]
    public TextMeshProUGUI playerText;

    public void ApplyPhotonPlayer(PhotonPlayer player)
    {
        playerText.text = player.NickName;
    }
}
