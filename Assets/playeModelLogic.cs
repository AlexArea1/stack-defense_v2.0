﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playeModelLogic : MonoBehaviour
{
    public float rotatespeed = 3;

    public float normalRotateSpeed = 0.3f;

    public Slider rotateSpeed;


    public void Start()
    {
        rotateSpeed.value = normalRotateSpeed;
    }

    void Update()
    {
        this.transform.Rotate(0, rotateSpeed.value, 0);
    }
}
