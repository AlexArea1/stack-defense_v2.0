﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoomTextHandler : MonoBehaviour
{
    public TextMeshProUGUI roomNameText;
    public GameObject keyBoard;

    public void changedpos()
    {
        //if (empty == false)
        //{
        //    roomNameText.text = "";
        //    empty = true;
        //}

        if (keyBoard.activeInHierarchy == true)
        {
            keyBoard.transform.position = new Vector3(0.4f, 1.4f, 3.2f);
            keyBoard.transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

}
