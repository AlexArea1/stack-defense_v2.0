﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class SwitchDecalPos : MonoBehaviour
{

    public GameObject decal1;

    public int countNext = 2;

    void Start()
    {
    }
    public void SwitchPosOfDecal()
    {
        countNext--;
        if (countNext < 0)
        {
            countNext = 2;
        }
        switch (countNext)
        {
            case 0:
                decal1.transform.position = new Vector3(-0.314f, 0.0457777f, -0.4112f);
                decal1.transform.eulerAngles = new Vector3(-90.0001f, 0, 0);
                break;
            case 1:
                decal1.transform.position = new Vector3(-0.315f, 0.247f, 0.13f);
                decal1.transform.eulerAngles = new Vector3(-139.791f, -10.68399f, 95.126f);
                break;
        }
    }
}
